# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

from ordermanage.models import *

# Create your models here.

class AmazonLineItem(models.Model):
    order_id = models.ForeignKey(Order, on_delete=models.CASCADE)
    lineitem_id = models.CharField(max_length=100, primary_key=True)
    company_name = models.CharField(max_length=15, default="Amazon")
    item_quantity = models.IntegerField(default = 1,null = False, blank = False)
    tracking_id = models.CharField(max_length=50, null=True, default='')
    feedId = models.CharField(max_length=20, null=True, default='')

class AmazonIsPrime(models.Model):
    order_id = models.OneToOneField(Order, on_delete=models.CASCADE)
    isPrime = models.BooleanField(default=False)

class AmazonAccessToken(models.Model):
    user_id = models.OneToOneField(User, primary_key=True,on_delete=models.CASCADE)
    access_token = models.CharField(max_length=100)
    seller_id = models.CharField(max_length=25)

