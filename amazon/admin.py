# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import *

# Register your models here.

class AmazonLineItemAdmin(admin.ModelAdmin):
    list_display = ["order_id","lineitem_id","item_quantity","company_name","tracking_id","feedId"]

admin.site.register(AmazonLineItem, AmazonLineItemAdmin)

class AmazonIsPrimeAdmin(admin.ModelAdmin):
    list_display = ["order_id","isPrime"]

admin.site.register(AmazonIsPrime, AmazonIsPrimeAdmin)

class AmazonAccessTokenAdmin(admin.ModelAdmin):
    list_display = ["user_id","access_token","seller_id"]

admin.site.register(AmazonAccessToken, AmazonAccessTokenAdmin)
