# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import *
from django.contrib.auth.models import User

import urllib, requests, json, base64, hmac, hashlib, datetime
from time import gmtime, strftime

from xml.dom import minidom

from ordermanage.models import *
from shop.models import *

# Create your views here.

SECRET_KEY = 'jUtGdIR+TdaRdbKp4D4jZ/ZFGEic/S07oVTllESa' #Do not fulfill or add Products with this key
AWS_ACCESS_KEY = 'AKIAJ3ZVWTD2WYM76HJA'

def get_timestamp():
    """Return correctly formatted timestamp"""
    return strftime("%Y-%m-%dT%H:%M:%SZ", gmtime())

def calc_signature(method, domain, URI, request_description, key):
    """Calculate signature to send with request"""
    sig_data = method + '\n' + \
        domain.lower() + '\n' + \
        URI + '\n' + \
        request_description

    hmac_obj = hmac.new(str(key), str(sig_data), hashlib.sha256)
    digest = hmac_obj.digest()

    return  urllib.quote(base64.b64encode(digest), safe='-_+=/.~')

def get_contentMD5_base64_value(content):
    """Return base64 encoded MD5 hash of the feed content.
       Amazon MWS uses this value to determine if the feed data has been corrupted or tampered with during transit."""
    ContentMD5Value = hashlib.md5(content) # Python 2.x
    # ContentMD5Value = hashlib.md5(feedContent.encode('utf-8')).hexdigest() # Python 3.x
    return ContentMD5Value.digest().encode('base64').strip()

# For Converting XML to JSON
def parse_element(element):
    dict_data = dict()
    if element.nodeType == element.TEXT_NODE:
        dict_data['data'] = element.data
    if element.nodeType not in [element.TEXT_NODE, element.DOCUMENT_NODE, 
                                element.DOCUMENT_TYPE_NODE]:
        for item in element.attributes.items():
            dict_data[item[0]] = item[1]
    if element.nodeType not in [element.TEXT_NODE, element.DOCUMENT_TYPE_NODE]:
        for child in element.childNodes:
            child_name, child_dict = parse_element(child)
            if child_name in dict_data:
                try:
                    dict_data[child_name].append(child_dict)
                except AttributeError:
                    dict_data[child_name] = [dict_data[child_name], child_dict]
            else:
                dict_data[child_name] = child_dict 
    return element.nodeName, dict_data

def getOrderItemsAmazon(request, orderId):
    am = AmazonAccessToken.objects.get(pk=request.user)
    SELLER_ID = am.seller_id #'A2875UKYFKYCX5'
    MWSAuthToken = am.access_token #'amzn.mws.58ae5f81-5528-6d07-3494-1241aa3de1fd'

    AmazonOrderId = orderId

    domain = 'mws.amazonservices.com'
    Action = 'ListOrderItems'
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2013-09-01'
    URI = '/Orders/2013-09-01'
    proto = 'https://'
    method = 'POST'
    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken': MWSAuthToken,
        'SellerId': SELLER_ID,
        'AmazonOrderId': AmazonOrderId,
        'SignatureMethod': SignatureMethod,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        'Version': Version,
    }
    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])
    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)
    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))
    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }
    response = requests.request(method, url, headers=headers)
    if response.status_code == 200:
        return response.content
    return "ERROR"

def getOrdersAmazon(request): 
    am = AmazonAccessToken.objects.get(pk=request.user)
    SELLER_ID = am.seller_id #'A2875UKYFKYCX5'
    MWSAuthToken = am.access_token #'amzn.mws.58ae5f81-5528-6d07-3494-1241aa3de1fd'

    MARKETPLACE_ID = 'ATVPDKIKX0DER'

    domain = 'mws.amazonservices.com'
    Action = 'ListOrders'
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2013-09-01'

    CreatedAfter = (datetime.datetime.utcnow() - datetime.timedelta(days=10)).strftime("%Y-%m-%dT%H:%M:%SZ")
    CreatedBefore = (datetime.datetime.utcnow() - datetime.timedelta(minutes=5)).strftime("%Y-%m-%dT%H:%M:%SZ")

    URI = '/Orders/2013-09-01'
    proto = 'https://'
    method = 'POST'
    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken': MWSAuthToken,
        'SellerId': SELLER_ID,
        'SignatureMethod': SignatureMethod,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        'Version': Version,
        'CreatedAfter': CreatedAfter,
        'CreatedBefore': CreatedBefore,
        'MarketplaceId.Id.1': MARKETPLACE_ID
    }
    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])
    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)
    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))
    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }
    response = requests.request(method, url, headers=headers)
    if response.status_code == 200:
        parsedXml = minidom.parseString(response.content)
        jsonData = json.dumps(parse_element(parsedXml), sort_keys=True, indent=4)
        orders = json.loads(jsonData)
        if 'Order' in orders[1]['ListOrdersResponse']['ListOrdersResult']['Orders']:
            tempOrderDict = orders[1]['ListOrdersResponse']['ListOrdersResult']['Orders']['Order']
            orderList = ['']
            if type(tempOrderDict) is dict:
                orderList[0] = tempOrderDict # If only one order is given in response then it is just a dictionary
            else:
                orderList = tempOrderDict # List of Order Dictionary
            for order in orderList:
                orderCount = Order.objects.filter(po=str(order['AmazonOrderId']['#text']['data'])).count()
                if orderCount < 1:
                    dateOrd = str(order['PurchaseDate']['#text']['data']).split("T")
                    orderdate = datetime.datetime.strptime(dateOrd[0], '%Y-%m-%d')
                    # sku = "Ebay001" # change with sku of each line item
                    company = (Company.objects.filter(company_name=CompanyName.objects.get(companyname="Amazon").companyname_id))[0]
                    # company = Company.objects.get(company_sku=sku)
                    courier = CourierMethod.objects.get(courier_method="UPS Ground")
                    isAFN = "AFN" if order['FulfillmentChannel']['#text']['data'] == "AFN" else ""
                    prime = True if order['IsPrime']['#text']['data'] == "true" else False
                    orderItems = getOrderItemsAmazon(request, str(order['AmazonOrderId']['#text']['data']))
                    if orderItems == "ERROR":
                        return HttpResponse("ERROR")
                    else:
                        itemParsedXml = minidom.parseString(orderItems)
                        itemJsonData = json.dumps(parse_element(itemParsedXml), sort_keys=True, indent=4)
                        orderItemsList = json.loads(itemJsonData)
                        orderItemList = orderItemsList[1]['ListOrderItemsResponse']['ListOrderItemsResult']['OrderItems']['OrderItem']
                        orderObj = Order(po=order['AmazonOrderId']['#text']['data'], user=request.user, company_name=company, order_date=orderdate.date(), courier_method=courier, traking_number=isAFN)
                        orderObj.save()
                        primeObj = AmazonIsPrime(order_id=orderObj,isPrime=prime)
                        primeObj.save()
                        if 'ShippingAddress' in order:
                            name = order['ShippingAddress']['Name']['#text']['data']
                            address = order['ShippingAddress']['AddressLine1']['#text']['data'] if 'AddressLine1' in order['ShippingAddress'] else ""
                            city = order['ShippingAddress']['City']['#text']['data'] if 'City' in order['ShippingAddress'] else ""
                            state = order['ShippingAddress']['StateOrRegion']['#text']['data'] if 'StateOrRegion' in order['ShippingAddress'] else ""
                            postalcode = order['ShippingAddress']['PostalCode']['#text']['data'] if 'PostalCode' in order['ShippingAddress'] else ""
                            phone = order['ShippingAddress']['Phone']['#text']['data'] if 'Phone' in order['ShippingAddress'] else ""
                            addressObj = Address(po_number=orderObj, customer_name=name, address=address, city=city, state=state, zip=postalcode, phone_number=phone)
                            addressObj.save()
                        if type(orderItemList) is dict:
                            lineItemObj = AmazonLineItem(lineitem_id=orderItemList['OrderItemId']['#text']['data'],order_id=orderObj,company_name="Amazon",item_quantity=orderItemList['QuantityOrdered']['#text']['data'],tracking_id=isAFN)
                            lineItemObj.save()
                            amz_order_qty = LineTable
                            amz_lineItem =amz_order_qty(purchase_order =orderObj, vendor_SKU = orderItemList['OrderItemId']['#text']['data'] ,order_quantity =orderItemList['QuantityOrdered']['#text']['data']  )
                            amz_lineItem.save()
                        else:
                            for lineitem in orderItemList:
                                lineItemObj = AmazonLineItem(lineitem_id=lineitem['OrderItemId']['#text']['data'],order_id=orderObj,company_name="Amazon",item_quantity=lineitem['QuantityOrdered']['#text']['data'],tracking_id=isAFN)
                                lineItemObj.save()
                                amz_order_qty = LineTable
                                amz_lineItem =amz_order_qty(purchase_order =orderObj, vendor_SKU = lineitem['OrderItemId']['#text']['data'],order_quantity =lineitem['QuantityOrdered']['#text']['data']  )
                                amz_lineItem.save()
                else:
                    addressCount = Address.objects.filter(po_number=str(order['AmazonOrderId']['#text']['data'])).count()
                    if (addressCount < 1) and ('ShippingAddress' in order):
                        name = order['ShippingAddress']['Name']['#text']['data']
                        address = order['ShippingAddress']['AddressLine1']['#text']['data'] if 'AddressLine1' in order['ShippingAddress'] else ""
                        city = order['ShippingAddress']['City']['#text']['data'] if 'City' in order['ShippingAddress'] else ""
                        state = order['ShippingAddress']['StateOrRegion']['#text']['data'] if 'StateOrRegion' in order['ShippingAddress'] else ""
                        postalcode = order['ShippingAddress']['PostalCode']['#text']['data'] if 'PostalCode' in order['ShippingAddress'] else ""
                        phone = order['ShippingAddress']['Phone']['#text']['data'] if 'Phone' in order['ShippingAddress'] else ""
                        orderObj = Order.objects.get(po=str(order['AmazonOrderId']['#text']['data']))
                        addressObj = Address(po_number=orderObj, customer_name=name, address=address, city=city, state=state, zip=postalcode, phone_number=phone)
                        addressObj.save()
                    else:
                        pass
        else:
            pass
    else:
        return HttpResponse("ERROR")
    return HttpResponse("Success")

def getFeedStatusAmazon(request):
    am = AmazonAccessToken.objects.get(pk=request.user)
    SELLER_ID = am.seller_id #'A2875UKYFKYCX5'
    MWSAuthToken = am.access_token #'amzn.mws.58ae5f81-5528-6d07-3494-1241aa3de1fd'

    MARKETPLACE_ID = 'ATVPDKIKX0DER'

    FeedSubmissionId = "77883017830" # get feed id from AmazonLineItem for the item which was fulfilled to check its fulfillment status

    domain = 'mws.amazonservices.com'
    Action = 'GetFeedSubmissionList'
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2009-01-01'

    URI = '/Feeds/2009-01-01'
    proto = 'https://'
    method = 'POST'
    payload = {
            'AWSAccessKeyId': AWS_ACCESS_KEY,
            'Action': Action,
            'MWSAuthToken': MWSAuthToken,
            'SellerId': SELLER_ID,
            'SignatureMethod': SignatureMethod,
            'SignatureVersion': SignatureVersion,
            'Timestamp': Timestamp,
            'Version': Version,
            'FeedSubmissionIdList.Id.1': FeedSubmissionId,
            'MarketplaceIdList.Id.1': MARKETPLACE_ID
        }
    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])
    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)
    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))
    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }
    response = requests.request(method, url, headers=headers)
    if response.status_code == 200:
        parsedXml = minidom.parseString(response.content)
        jsonData = json.dumps(parse_element(parsedXml), sort_keys=True, indent=4)
        feedStatusResponse = json.loads(jsonData)
        feedStatus = feedStatusResponse[1]['GetFeedSubmissionListResponse']['GetFeedSubmissionListResult']['FeedSubmissionInfo']['FeedProcessingStatus']['#text']['data']
        if feedStatus == "_DONE_":
            feedResult = getFeedResultAmazon(request,FeedSubmissionId)
            if feedResult == "ERROR":
                return HttpResponse("Some Error Occurred In Feed Result API")
            else:
                parsedXml = minidom.parseString(feedResult)
                jsonData = json.dumps(parse_element(parsedXml), sort_keys=True, indent=4)
        # return HttpResponse(jsonData)
        return render(request, "json.json" , {'dict':jsonData}, content_type='application/json')
    return HttpResponse("Some Error Occurred Feed Status API")

def getFeedResultAmazon(request,FeedSubmissionId):
    am = AmazonAccessToken.objects.get(pk=request.user)
    SELLER_ID = 'A2875UKYFKYCX5' #am.seller_id #'A2875UKYFKYCX5'
    MWSAuthToken = 'amzn.mws.58ae5f81-5528-6d07-3494-1241aa3de1fd' #am.access_token #'amzn.mws.58ae5f81-5528-6d07-3494-1241aa3de1fd'

    MARKETPLACE_ID = 'ATVPDKIKX0DER'

    domain = 'mws.amazonservices.com'
    Action = 'GetFeedSubmissionResult'
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2009-01-01'

    URI = '/Feeds/2009-01-01'
    proto = 'https://'
    method = 'POST'
    payload = {
            'AWSAccessKeyId': AWS_ACCESS_KEY,
            'Action': Action,
            'MWSAuthToken': MWSAuthToken,
            'SellerId': SELLER_ID,
            'SignatureMethod': SignatureMethod,
            'SignatureVersion': SignatureVersion,
            'Timestamp': Timestamp,
            'Version': Version,
            'FeedSubmissionId': FeedSubmissionId,
            'MarketplaceIdList.Id.1': MARKETPLACE_ID
        }
    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])
    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)
    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))
    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }
    response = requests.request(method, url, headers=headers)
    if response.status_code == 200:
        # parsedXml = minidom.parseString(response.content)
        # jsonData = json.dumps(parse_element(parsedXml), sort_keys=True, indent=4)
        return response.content#HttpResponse(jsonData)
    return "ERROR"#HttpResponse("Some Error Occurred")

def fulfillOrderAmazon(request):
    am = AmazonAccessToken.objects.get(pk=request.user)
    SELLER_ID = am.seller_id #'A2875UKYFKYCX5'
    MWSAuthToken = am.access_token #'amzn.mws.58ae5f81-5528-6d07-3494-1241aa3de1fd'

    MARKETPLACE_ID = 'ATVPDKIKX0DER'

    domain = 'mws.amazonservices.com'
    Action = 'SubmitFeed'
    FeedType = '_POST_ORDER_FULFILLMENT_DATA_'
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2009-01-01'

    URI = '/Feeds/2009-01-01'
    proto = 'https://'
    method = 'POST'

    lineItemObjs = AmazonLineItem.objects.filter(order_id=request.POST['po'])
    lineItemObj = AmazonLineItem.objects.get(pk=lineItemObjs[0].lineitem_id)
    lineItemObj.tracking_id = request.POST['traking_number']
    lineItemObj.save()

    orderId = "	123-1234567-1234567" #str(lineItemObj.order_id) # should be 3-7-7 digits
    itemId = "12345678901234" #str(lineItemObj.lineitem_id) # should be 14 digits
    quantity = "2" #lineItemObj.item_quantity
    courierMethod = "UPS Ground" #Change according to seller request
    ShippingMethod = "Second Day" #Change according to Seller request
    trackingNumber = "1234567890" #str(lineItemObj.tracking_id)

    fulfillmentDate = datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")

    feedContent = """<?xml version="1.0" encoding="UTF-8"?>
                        <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amznenvelope.xsd">
                            <Header>
                                <DocumentVersion>1.01</DocumentVersion>
                                <MerchantIdentifier>{SELLER_ID}</MerchantIdentifier>
                            </Header>
                            <MessageType>OrderFulfillment</MessageType>
                            <Message>
                                <MessageID>1</MessageID>
                                <OrderFulfillment>
                                    <AmazonOrderID>{orderId}</AmazonOrderID>
                                    <FulfillmentDate>{fulfillmentDate}</FulfillmentDate>
                                    <FulfillmentData>
                                        <CarrierCode>{courierMethod}</CarrierCode>
                                        <ShippingMethod>{ShippingMethod}</ShippingMethod>
                                        <ShipperTrackingNumber>{trackingNumber}</ShipperTrackingNumber>
                                    </FulfillmentData>
                                    <Item>
                                        <AmazonOrderItemCode>{itemId}</AmazonOrderItemCode>
                                        <Quantity>{quantity}</Quantity>
                                    </Item>
                                </OrderFulfillment>
                            </Message>
                        </AmazonEnvelope>
                    """.format(**vars())
    base64EncodedMD5Value = get_contentMD5_base64_value(feedContent)
    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken': MWSAuthToken,
        'SellerId': SELLER_ID,
        'SignatureMethod': SignatureMethod,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        'Version': Version,
        'FeedType': FeedType,
        'ContentMD5Value': base64EncodedMD5Value,
        'MarketplaceIdList.Id.1': MARKETPLACE_ID
    }
    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])
    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)
    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))
    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }
    response = requests.request(method, url, headers=headers, data=feedContent)
    if response.status_code == 200:
        parsedXml = minidom.parseString(response.content)
        jsonData = json.dumps(parse_element(parsedXml), sort_keys=True, indent=4)
        feedResponse = json.loads(jsonData)
        feedID = feedResponse[1]['SubmitFeedResponse']['SubmitFeedResult']['FeedSubmissionInfo']['FeedSubmissionId']['#text']['data']
        print ("================> Feed Id")
        print (feedID )
        try:
            itemObj = AmazonLineItem.objects.get(lineitem_id=itemId)
            itemObj.feedId = feedID
            itemObj.save()
        except:
            print ("Line item not found")
        # status = getFeedStatusAmazon(request,feedID)
        # if status == "_DONE_":
        #     feedResult = getFeedResultAmazon(request,feedID)
        #     if feedResult == "ERROR":
        #         return HttpResponse("Some Error Occurred")
        #     else:
        #         parsedXml = minidom.parseString(response.content)
        #         jsonData = json.dumps(parse_element(parsedXml), sort_keys=True, indent=4)
        #         return HttpResponse(jsonData)
        # elif status == "ERROR":
        #     return HttpResponse("Some Error Occurred")
        # else:
        #     return HttpResponse("PROCESSING NOT COMPLETED")
        # return HttpResponse(jsonData)
        return render(request, "json.json" , {'dict':jsonData}, content_type='application/json')
    return HttpResponse("Some Error Occurred In FulFillOrder API")

def addProductAmazon(request):
    am = AmazonAccessToken.objects.get(pk=request.user)
    SELLER_ID = am.seller_id #'A2875UKYFKYCX5'
    MWSAuthToken = am.access_token #'amzn.mws.58ae5f81-5528-6d07-3494-1241aa3de1fd'

    MARKETPLACE_ID = 'ATVPDKIKX0DER'

    domain = 'mws.amazonservices.com'
    Action = 'SubmitFeed'
    FeedType = '_POST_ORDER_FULFILLMENT_DATA_'
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2009-01-01'

    URI = '/Feeds/2009-01-01'
    proto = 'https://'
    method = 'POST'

    

    feedContent = """<?xml version="1.0" encoding="UTF-8"?>
                        <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amznenvelope.xsd">
                            <Header>
                                <DocumentVersion>1.01</DocumentVersion>
                                <MerchantIdentifier>{SELLER_ID}</MerchantIdentifier>
                            </Header>
                            <MessageType>OrderFulfillment</MessageType>
                            <Message>
                                <MessageID>1</MessageID>
                                <OrderFulfillment>
                                    <AmazonOrderID>{orderId}</AmazonOrderID>
                                    <FulfillmentDate>{fulfillmentDate}</FulfillmentDate>
                                    <FulfillmentData>
                                        <CarrierCode>{courierMethod}</CarrierCode>
                                        <ShippingMethod>{ShippingMethod}</ShippingMethod>
                                        <ShipperTrackingNumber>{trackingNumber}</ShipperTrackingNumber>
                                    </FulfillmentData>
                                    <Item>
                                        <AmazonOrderItemCode>{itemId}</AmazonOrderItemCode>
                                        <Quantity>{quantity}</Quantity>
                                    </Item>
                                </OrderFulfillment>
                            </Message>
                        </AmazonEnvelope>
                    """.format(**vars())
    
    base64EncodedMD5Value = get_contentMD5_base64_value(feedContent)
    
    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken': MWSAuthToken,
        'SellerId': SELLER_ID,
        'SignatureMethod': SignatureMethod,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        'Version': Version,
        'FeedType': FeedType,
        'ContentMD5Value': base64EncodedMD5Value,
        'MarketplaceIdList.Id.1': MARKETPLACE_ID
    }
    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])
    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)
    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))
    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }
    response = requests.request(method, url, headers=headers, data=feedContent)
    print ("RESPONSE ADD PRODUCT FEED:")
    print (response)
    print (response.content)
    if response.status_code == 200:
        parsedXml = minidom.parseString(response.content)
        jsonData = json.dumps(parse_element(parsedXml), sort_keys=True, indent=4)
        feedResponse = json.loads(jsonData)
        feedID = feedResponse[1]['SubmitFeedResponse']['SubmitFeedResult']['FeedSubmissionInfo']['FeedSubmissionId']['#text']['data']
        print ("================> Feed Id")
        print (feedID)
        # try:
        #     itemObj = AmazonLineItem.objects.get(lineitem_id=itemId)
        #     itemObj.feedId = feedID
        #     itemObj.save()
        # except:
        #     print "Line item not found"
        # status = getFeedStatusAmazon(request,feedID)
        # if status == "_DONE_":
        #     feedResult = getFeedResultAmazon(request,feedID)
        #     if feedResult == "ERROR":
        #         return HttpResponse("Some Error Occurred")
        #     else:
        #         parsedXml = minidom.parseString(response.content)
        #         jsonData = json.dumps(parse_element(parsedXml), sort_keys=True, indent=4)
        #         return HttpResponse(jsonData)
        # elif status == "ERROR":
        #     return HttpResponse("Some Error Occurred")
        # else:
        #     return HttpResponse("PROCESSING NOT COMPLETED")
        # return HttpResponse(jsonData)
        return render(request, "json.json" , {'dict':jsonData}, content_type='application/json')
    return HttpResponse("Some Error Occurred In Add Product API")

def getProductsAmazon(request):
    am = AmazonAccessToken.objects.get(pk=request.user)
    SELLER_ID = am.seller_id #'A2875UKYFKYCX5'
    MWSAuthToken = am.access_token #'amzn.mws.58ae5f81-5528-6d07-3494-1241aa3de1fd'

    MARKETPLACE_ID = 'ATVPDKIKX0DER'

    domain = 'mws.amazonservices.com'
    Action = 'ListInventorySupply'
    SignatureMethod = 'HmacSHA256'
    SignatureVersion = '2'
    Timestamp = get_timestamp()
    Version = '2010-10-01'

    QueryStartDateTime = (datetime.datetime.utcnow() - datetime.timedelta(days=100)).strftime("%Y-%m-%dT%H:%M:%SZ")
    ResponseGroup = "Basic " # "Detailed"

    URI = '/FulfillmentInventory/2010-10-01'
    proto = 'https://'
    method = 'POST'
    payload = {
        'AWSAccessKeyId': AWS_ACCESS_KEY,
        'Action': Action,
        'MWSAuthToken': MWSAuthToken,
        'SellerId': SELLER_ID,
        'SignatureMethod': SignatureMethod,
        'SignatureVersion': SignatureVersion,
        'Timestamp': Timestamp,
        'Version': Version,
        'QueryStartDateTime': QueryStartDateTime,
        'ResponseGroup': ResponseGroup,
        'MarketplaceId': MARKETPLACE_ID
    }
    request_description = '&'.join(['%s=%s' % (k, urllib.quote(payload[k], safe='-_.~').encode('utf-8')) for k in sorted(payload)])
    sig = calc_signature(method, domain, URI, request_description, SECRET_KEY)
    url = '%s%s?%s&Signature=%s' % \
        (proto+domain, URI, request_description, urllib.quote(sig))
    headers = {
        'Host': domain,
        'Content-Type': 'text/xml',
        'x-amazon-user-agent': 'python-requests/1.2.0 (Language=Python)'
    }
    response = requests.request(method, url, headers=headers)
    if response.status_code == 200:
        parsedXml = minidom.parseString(response.content)
        jsonData = json.dumps(parse_element(parsedXml), sort_keys=True, indent=4)
        # return HttpResponse(jsonData)
        return render(request, "json.json" , {'dict':jsonData}, content_type='application/json')
    return HttpResponse("Some Error Occurred")

