from django.conf.urls import url
from django.conf.urls.static import static
from . import views
 
urlpatterns = [
    url(r'^getProducts/$', views.getProductsAmazon, name="getProductsAmazon"),
    url(r'^addProduct/$', views.addProductAmazon, name="addProductAmazon"),
    url(r'^getOrders/$', views.getOrdersAmazon, name="getOrdersAmazon"),
    url(r'^fulfillOrder/$', views.fulfillOrderAmazon, name="fulfillOrderAmazon"),
    url(r'^getFeedStatus/$', views.getFeedStatusAmazon, name="getFeedStatusAmazon"),
    # url(r'^getFeedResult/$', views.getFeedResultAmazon, name="getFeedResultAmazon"),
]
