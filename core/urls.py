from django.conf.urls import url, include
from django.contrib import admin
import django.core.handlers.wsgi
from . import views 
from django.conf.urls.static import static
from django.conf import settings
from .forms import LoginForm, SignUpForm, AuthenticationForm, EmailAuthenticationForm
from django.contrib.auth import views as auth_views


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', django.contrib.auth.views.LoginView.as_view(template_name= 'mvp-2.html', authentication_form= LoginForm), name='mvp'),
    url(r'^ordermanage/', include("ordermanage.urls")),
    url(r'^signup/$', views.signup, name='signup'),      
    url(r'^logout/$',auth_views.LogoutView.as_view(), name='logout'),
    url(r'^shop/', include('shop.urls',  namespace='shop')),
    url(r'^wallmart/', include("wallmart.urls")),
    url(r'^ups/', include('ups.urls')),
    url(r'^shopify_app/', include('shopify_app.urls')),
    url(r'^ebay/', include("ebayshop.urls")),
    url(r'^amazon/', include("amazon.urls")),
    url(r'^fedex/', include("fedex.urls")),
    
    # PASSWWORD VIEWS - AUTH BASED.

    url(r'^password_reset/$', auth_views.PasswordResetView.as_view(), name='password_reset'),
    url(r'^password_reset/done/$', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    url(r'^auth/', include('rest_framework_social_oauth2.urls')), #heroku comment
]


"""for Lodaing Static files in django """

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

"""End Of  Lodaing Static files in django """