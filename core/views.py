from django.http import HttpResponse, HttpResponseRedirect,JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, Permission
from .forms import LoginForm, SignUpForm, AuthenticationForm
from django.contrib.auth.decorators import login_required
from rolepermissions.roles import assign_role
from django.views.decorators.csrf import csrf_exempt
# import shopify
# from shopify_app.decorators import shop_login_required
from django.contrib.auth.models import Group
# from django.contrib.auth import get_user_model
'''==========================================================================
    Project Name: OrderManage
    Function Name: MVP Page
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def mvp(request):
    return render(request,"mvp.html", {})


'''==========================================================================
    Project Name: OrderManage
    Function Name: LogIn
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            login(request, user)
            if user is not None:
                #if user.is_superuser:
                return HttpResponseRedirect('ordermanage/')            
            #elif user.is_staff:
                #return HttpResponseRedirect('ordermanage/staff')

            else:
                return HttpResponse('Invalid login')

    else:
        form = LoginForm()
    return render(request, 'mvp.html', {'form': form})
@csrf_exempt
def user_login(request):
    pass
    # if request.method == 'POST' :
    #     form = LoginForm(request.POST)
    #     print form
    #     if form.is_valid():
    #         cd = form.cleaned_data
    #         user = authenticate(username=cd['username'], password=cd['password'])
    #         print user
    #         # login(request, user)
    #         if user is not None and  user.is_active :                
    #             login(request, user)
    #             # return redirect('ordermanage/')
    #         else:
    #             return redirect('mvp')
    #             # if user.is_superuser:
    #             #     return HttpResponseRedirect('/ordermanage/')            
    #         # elif user.is_staff:
    #         #     return HttpResponseRedirect('ordermanage/staff')

    #         # else:
    #         #     return HttpResponse('Invalid login')

    # else:
    #     form = LoginForm()
    # return render(request, 'mvp.html', {'form': form})
    # # return render_to_response('mvp.html', {'shouts' : shouts,
    #                                          'form' : form },
    #                           context_instance = RequestContext(request))


'''==========================================================================
    Project Name: OrderManage
    Function Name: LogOut
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''


# def user_logout(request):
#     # form = LoginForm()
#     return render(request, 'logout.html', {'section': 'logout'})

def user_logout(request):
    logout(request)
    request.session.flush()
    return redirect('mvp')
    
    # 404 page
# def errorpage(request):
#     return render(request,"404.html", {})   

# def signup(request):
#     if request.method == "POST":
#         form = SignUpForm(request.POST)
#         if form.is_valid():
#             user = form.save(commit=False)
#             user.save()
#             username = form.cleaned_data['username']
#             email = form.cleaned_data['email']
#             password = form.cleaned_data['password1']
#             User.objects.create_user(
#                 username=username, email=email, password=password)
#             # username = form.cleaned_data.get('username')
#             # raw_password = form.cleaned_data.get('password1')
#             # user = authenticate(username=username, password=raw_password)
#             # login(request, user)
#             return redirect('mvp',)
#     else:
#         form = SignUpForm()
#     return render(request, 'mvp.html', {'form': form})

'''==========================================================================
    Project Name: OrderManage
    Function Name: SignUp
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            user=form.save()
            group = Group.objects.get(name=request.POST.get('Select_Group'))
            user.groups.add(group) 
            print (group)
            user.save()
            return redirect('mvp',)
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})



