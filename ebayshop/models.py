# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

from ordermanage.models import *

# Create your models here.

class EbayAccessToken(models.Model):
    user_id = models.OneToOneField(User, primary_key=True,on_delete=models.CASCADE)
    access_token = models.TextField()
    refresh_token = models.CharField(max_length=500)

class EbayLineItem(models.Model):
    order_id = models.ForeignKey(Order, on_delete=models.CASCADE)
    lineitem_id = models.CharField(max_length=100, primary_key=True)
    company_name = models.CharField(max_length=15, default="Ebay")
    item_quantity = models.IntegerField(default = 1,null = False, blank = False)
    tracking_id = models.CharField(max_length=50, null=True, default='')
