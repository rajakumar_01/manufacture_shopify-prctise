# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import *

# Register your models here.

class EbayLineItemAdmin(admin.ModelAdmin):
    list_display = ["order_id","lineitem_id","item_quantity","company_name","tracking_id"]

admin.site.register(EbayLineItem, EbayLineItemAdmin)

class EbayAccessTokenAdmin(admin.ModelAdmin):
    list_display = ["user_id","access_token","refresh_token"]

admin.site.register(EbayAccessToken, EbayAccessTokenAdmin)