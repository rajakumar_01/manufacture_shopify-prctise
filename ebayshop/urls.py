from django.conf.urls import url
from django.conf.urls.static import static
from . import views

urlpatterns = [
    url(r'^oauthEbay/$', views.ebayoauthEbay, name="oauthEbay"), #main dashbord
    url(r'^oauthEbayReturn/$', views.ebayoauthEbayReturn, name="oauthEbayReturn"),
    url(r'^setMerchantLocation/$', views.ebaysetMerchantLocationKey, name="setMerchantLocation"),
    url(r'^setMerchantPolicies/$', views.ebaysetMerchantPolicies, name="setMerchantPolicies"),
    url(r'^getProducts/$', views.ebaygetProducts, name="getProducts"),
    url(r'^addProduct/$', views.ebayaddProduct, name="addProduct"),
    url(r'^getOrders/$', views.ebaygetOrders, name="getOrders"),
    url(r'^ebayFulfillOrder/$', views.ebayfulfillOrder, name="ebayFulfillOrder"),
]
