# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import *
from django.contrib.auth.models import User
import requests, json, base64

from ordermanage.models import *
from shop.models import *

# Create your views here. 

clientID = "suryakar-testorde-SBX-af8fe58ca-89463e2c"#"suryakar-testorde-PRD-cf8f85d15-81ad47bc"#
clientSecret = "SBX-f8fe58ca9329-533c-4014-95e4-4616"#"PRD-f8f85d159e60-72c2-45d4-9535-ce0f"#
redirectUri = "surya_karki-suryakar-testor-jngrvfl"#"surya_karki-suryakar-testor-gxrmgxhwb"#
ebayApiUrl = "api.sandbox.ebay.com" # for production use 'api.ebay.com'
scope = "https://api.ebay.com/oauth/api_scope/sell.account https://api.ebay.com/oauth/api_scope/sell.inventory https://api.ebay.com/oauth/api_scope/sell.fulfillment"

def ebayoauthEbay(request):
    url = "https://auth.sandbox.ebay.com/oauth2/authorize?client_id=" + clientID + "&redirect_uri=" + redirectUri + "&response_type=code&scope=" + scope
    return HttpResponseRedirect(url)

def ebayoauthEbayReturn(request):
    code = str(request.GET.get('code'))
    authHeaderData =  clientID+':'+clientSecret
    encodedAuthHeader = base64.b64encode(authHeaderData)
    headers = {
        'Content-Type' : 'application/x-www-form-urlencoded',
        'Authorization' : 'Basic ' + encodedAuthHeader,
    }
    body = {
        'grant_type' : 'authorization_code',
        'code' : code,
        'redirect_uri' : redirectUri,
    }
    response = requests.post("https://" + ebayApiUrl + "/identity/v1/oauth2/token", headers=headers, data=body)
    data = json.loads(response.content)
    try:
        eatObj = EbayAccessToken(user_id=request.user, access_token=data['access_token'], refresh_token=data['refresh_token'])
        eatObj.save()
    except:
        return HttpResponseRedirect("/ebay/oauthEbay")
    return HttpResponseRedirect("/ordermanage")

def refreshAccessToken(refreshToken):
    authHeaderData =  clientID+':'+clientSecret
    encodedAuthHeader = base64.b64encode(authHeaderData)
    headers = {
        'Content-Type' : 'application/x-www-form-urlencoded',
        'Authorization' : 'Basic ' + encodedAuthHeader,
    }
    body = {
        'grant_type' : 'refresh_token',
        'refresh_token' : str(refreshToken),
        'scope' : scope,
    }
    response = requests.post("https://"+ebayApiUrl+"/identity/v1/oauth2/token", headers=headers, data=body)
    data = json.loads(response.content)
    try:
        if data['access_token']:
            return data['access_token']
    except:
        return response.status_code

def getAppAccessToken():
    authHeaderData =  clientID+':'+clientSecret
    encodedAuthHeader = base64.b64encode(authHeaderData)
    headers = {
        'Content-Type' : 'application/x-www-form-urlencoded',
        'Authorization' : 'Basic ' + encodedAuthHeader,
    }
    body = {
        'grant_type' : 'client_credentials',
        'redirect_uri' : redirectUri,
        'scope' : 'https://api.ebay.com/oauth/api_scope'
    }
    response = requests.post("https://" + ebayApiUrl + "/identity/v1/oauth2/token", headers=headers, data=body)
    data = json.loads(response.content)
    return data['access_token']

def ebaysetMerchantLocationKey(request):
    eb = EbayAccessToken.objects.get(pk=request.user)
    lid = "orderManage2"
    headers = {
        'Authorization' : 'Bearer '+ eb.access_token,
        'Content-Type' : 'application/json'
    }
    body = {
        "location": {
            "address": {
                "postalCode": "07882",
                "country": "US"
            }
        }
    }
    response = requests.post("https://" + ebayApiUrl + "/sell/inventory/v1/location/" + lid, headers=headers, data=json.dumps(body))
    if response.status_code == 401:
        newAccessToken = refreshAccessToken(eb.refresh_token)
        if newAccessToken == 400:
            return HttpResponseRedirect("/ebay/oauthEbay")
        else:
            eatObj = EbayAccessToken(user_id=request.user, access_token=newAccessToken, refresh_token=eb.refresh_token)
            eatObj.save()
            headers = {
                'Authorization' : 'Bearer '+ str(newAccessToken),
                'Content-Type' : 'application/json'
            }
            response = requests.post("https://" + ebayApiUrl + "/sell/inventory/v1/location/" + lid, headers=headers, data=json.dumps(body))
    print ("RESPONSE")
    print (response)
    return HttpResponse(response.content)

def ebaysetMerchantPolicies(request):
    eb = EbayAccessToken.objects.get(pk=request.user)
    marketplaceId = "EBAY_US"
    emailForPaypal = "surya@eknous.com"
    headers = {
        'Authorization' : 'Bearer '+ eb.access_token,
        'Content-Type' : 'application/json'
    }
    body = {
            "name": "minimal Payment Policy",
            "marketplaceId": marketplaceId,
            "categoryTypes": [
                {
                    "name": "ALL_EXCLUDING_MOTORS_VEHICLES"
                }
            ],
            "paymentMethods": [
                {
                    "paymentMethodType": "PAYPAL",
                    "recipientAccountReference" :{
                    "referenceId" : emailForPaypal,
                    "referenceType" : "PAYPAL_EMAIL"
                    }
                }
            ]
        }
    response = requests.post("https://" + ebayApiUrl + "/sell/account/v1/payment_policy", headers=headers, data=json.dumps(body))
    if response.status_code == 401:
        newAccessToken = refreshAccessToken(eb.refresh_token)
        if newAccessToken == 400:
            return HttpResponseRedirect("/ebay/oauthEbay")
        else:
            eatObj = EbayAccessToken(user_id=request.user, access_token=newAccessToken, refresh_token=eb.refresh_token)
            eatObj.save()
            headers = {
                'Authorization' : 'Bearer '+ str(newAccessToken),
                'Content-Type' : 'application/json'
            }
            response = requests.post("https://" + ebayApiUrl + "/sell/account/v1/payment_policy", headers=headers, data=json.dumps(body))
    print ("RESPONSE Payment Policy")
    print (response)
    print (response.content)
    body = {
        "categoryTypes": [
            {
                "name": "ALL_EXCLUDING_MOTORS_VEHICLES"
            }
        ],
        "marketplaceId": marketplaceId,
        "name": "Detailed shipping options: FLAT_RATE domestic, CALCULATED int'l"
    }
    response = requests.post("https://" + ebayApiUrl + "/sell/account/v1/fulfillment_policy", headers=headers, data=json.dumps(body))
    if response.status_code == 401:
        newAccessToken = refreshAccessToken(eb.refresh_token)
        if newAccessToken == 400:
            return HttpResponseRedirect("/ebay/oauthEbay")
        else:
            eatObj = EbayAccessToken(user_id=request.user, access_token=newAccessToken, refresh_token=eb.refresh_token)
            eatObj.save()
            headers = {
                'Authorization' : 'Bearer '+ str(newAccessToken),
                'Content-Type' : 'application/json'
            }
            response = requests.post("https://" + ebayApiUrl + "/sell/account/v1/fulfillment_policy", headers=headers, data=json.dumps(body))
    print ("RESPONSE Fulfillment Policy")
    print (response)
    print (response.content)
    body = {
        "name": "30-day domestic and international return policy",
        "marketplaceId": marketplaceId,
        "description": "Policy specifies an international return policy in addition to the domestic return policy.",
        "refundMethod": "MONEY_BACK",
        "returnsAccepted": True,
        "returnPeriod": {
            "value": 30,
            "unit": "DAY"
        },
        "returnShippingCostPayer": "SELLER",
        "internationalOverride": {
            "returnsAccepted": True,
            "returnPeriod": {
                "unit": "DAY",
                "value": 30
            },
            "returnShippingCostPayer": "BUYER"
        }
    }
    response = requests.post("https://" + ebayApiUrl + "/sell/account/v1/return_policy", headers=headers, data=json.dumps(body))
    if response.status_code == 401:
        newAccessToken = refreshAccessToken(eb.refresh_token)
        if newAccessToken == 400:
            return HttpResponseRedirect("/ebay/oauthEbay")
        else:
            eatObj = EbayAccessToken(user_id=request.user, access_token=newAccessToken, refresh_token=eb.refresh_token)
            eatObj.save()
            headers = {
                'Authorization' : 'Bearer '+ str(newAccessToken),
                'Content-Type' : 'application/json'
            }
            response = requests.post("https://" + ebayApiUrl + "/sell/account/v1/return_policy", headers=headers, data=json.dumps(body))
    print ("RESPONSE Return Policy")
    print (response)
    print (response.content)
    return HttpResponse(response.content)

def ebaygetProducts(request):
    eb = EbayAccessToken.objects.get(pk=request.user)
    headers = {
        'Authorization' : 'Bearer '+ eb.access_token,
    }
    response = requests.get("https://" + ebayApiUrl + "/sell/inventory/v1/inventory_item", headers=headers)
    if response.status_code == 401:
        newAccessToken = refreshAccessToken(eb.refresh_token)
        if newAccessToken == 400:
            return HttpResponseRedirect("/ebay/oauthEbay")
        else:
            eatObj = EbayAccessToken(user_id=request.user, access_token=newAccessToken, refresh_token=eb.refresh_token)
            eatObj.save()
            headers = {
                'Authorization' : 'Bearer '+ str(newAccessToken),
            }
            response = requests.get("https://" + ebayApiUrl + "/sell/inventory/v1/inventory_item", headers=headers)
    data = json.loads(response.content)
    print ("RESPONSE")
    print (data)
    return HttpResponse(response.content)

def ebayaddProduct(request, productInfo):
    eb = EbayAccessToken.objects.get(pk=request.user)
    paymentPolicyId = ""
    fulfillmentPolicyId = ""
    returnPolicyId = ""
    marketplaceId = "EBAY_US"
    merchantLocationKey = "orderManage2"
    ContentLanguage = 'en-US'
    sku = productInfo.company_sku#"MYPROD13"#"GP-Cam-01"
    category = str(productInfo.Product_SKU.category)
    imageUrl = request.scheme + "://" + str(request.META['HTTP_HOST']) + settings.MEDIA_URL + str(productInfo.Product_SKU.image)
    headers = {
        'Authorization' : 'Bearer '+ eb.access_token,
        'Content-Type' : 'application/json',
        'Content-Language' : ContentLanguage,
    }
    # Getting Payment Policy Id for Seller
    response = requests.get("https://" + ebayApiUrl + "/sell/account/v1/payment_policy?marketplace_id=" + marketplaceId, headers=headers)
    if response.status_code == 401:
        newAccessToken = refreshAccessToken(eb.refresh_token)
        if newAccessToken == 400:
            return HttpResponseRedirect("/ebay/oauthEbay")
        else:
            eatObj = EbayAccessToken(user_id=request.user, access_token=newAccessToken, refresh_token=eb.refresh_token)
            eatObj.save()
            headers = {
                'Authorization' : 'Bearer '+ str(newAccessToken),
                'Content-Type' : 'application/json',
                'Content-Language' : ContentLanguage,
            }
            response = requests.get("https://" + ebayApiUrl + "/sell/account/v1/payment_policy?marketplace_id=" + marketplaceId, headers=headers)
    data = json.loads(response.content)
    if 'paymentPolicies' in data and 'total' in data:
        if data['total'] > 0:
            paymentPolicyId = data['paymentPolicies'][0]['paymentPolicyId']
        else:
            return True
    else:
        return False
    # Getting Fulfillment Policy Id for Seller
    response = requests.get("https://" + ebayApiUrl + "/sell/account/v1/fulfillment_policy?marketplace_id=" + marketplaceId, headers=headers)
    if response.status_code == 401:
        newAccessToken = refreshAccessToken(eb.refresh_token)
        if newAccessToken == 400:
            return HttpResponseRedirect("/ebay/oauthEbay")
        else:
            eatObj = EbayAccessToken(user_id=request.user, access_token=newAccessToken, refresh_token=eb.refresh_token)
            eatObj.save()
            headers = {
                'Authorization' : 'Bearer '+ str(newAccessToken),
                'Content-Type' : 'application/json',
                'Content-Language' : ContentLanguage,
            }
            response = requests.get("https://" + ebayApiUrl + "/sell/account/v1/fulfillment_policy?marketplace_id=" + marketplaceId, headers=headers)
    data = json.loads(response.content)
    if 'fulfillmentPolicies' in data and 'total' in data:
        if data['total'] > 0:
            fulfillmentPolicyId = data['fulfillmentPolicies'][0]['fulfillmentPolicyId']
        else:
            return True
    else:
        return False
    # Getting Return Policy Id for Seller
    response = requests.get("https://" + ebayApiUrl + "/sell/account/v1/return_policy?marketplace_id=" + marketplaceId, headers=headers)
    if response.status_code == 401:
        newAccessToken = refreshAccessToken(eb.refresh_token)
        if newAccessToken == 400:
            return HttpResponseRedirect("/ebay/oauthEbay")
        else:
            eatObj = EbayAccessToken(user_id=request.user, access_token=newAccessToken, refresh_token=eb.refresh_token)
            eatObj.save()
            headers = {
                'Authorization' : 'Bearer '+ str(newAccessToken),
                'Content-Type' : 'application/json',
                'Content-Language' : ContentLanguage,
            }
            response = requests.get("https://" + ebayApiUrl + "/sell/account/v1/return_policy?marketplace_id=" + marketplaceId, headers=headers)
    data = json.loads(response.content)
    if 'returnPolicies' in data and 'total' in data:
        if data['total'] > 0:
            returnPolicyId = data['returnPolicies'][0]['returnPolicyId']
        else:
            return True
    else:
        return False
    body = {
        "availability": {
                "shipToLocationAvailability": {
                    "quantity": int(productInfo.Product_SKU.available_quentity)
                }
            },
        "condition": "NEW",
        # "packageWeightAndSize" :{
        #         "dimensions" :{
        #             "length" : 21.5,
        #             "width" : 15.0,
        #             "height" : 12.0,
        #             "unit" : "INCH" # FEET, CENTIMETER, METER
        #         },
                #"packageType" : "PackageTypeEnum : [LETTER,BULKY_GOODS,CARAVAN...]",
                # "weight" :{
                #     "unit" : "WeightUnitOfMeasureEnum : [POUND,KILOGRAM,OUNCE...]",
                #     "value" : "number"
                # }
            # },
        "product": {
            "title": str(productInfo.Product_SKU.name),
            "description": str(productInfo.Product_SKU.description),
            # "aspects": {
            #     "Brand": [
            #         "GoPro"
            #     ],
            #     "Type": [
            #         "Helmet/Action"
            #     ],
            #     "Storage Type": [
            #         "Removable"
            #     ],
            #     "Recording Definition": [
            #         "High Definition"
            #     ],
            #     "Media Format": [
            #         "Flash Drive (SSD)"
            #     ],
            #     "Optical Zoom": [
            #         "10x"
            #     ]
            # },
            # "brand": "GoPro",
            # "mpn": "CHDHX-401",
            "imageUrls": [
                imageUrl,
            ]
        }
    }
    response = requests.put("https://" + ebayApiUrl + "/sell/inventory/v1/inventory_item/" + sku, headers=headers, data=json.dumps(body))
    if response.status_code == 401:
        newAccessToken = refreshAccessToken(eb.refresh_token)
        if newAccessToken == 400:
            return HttpResponseRedirect("/ebay/oauthEbay")
        else:
            eatObj = EbayAccessToken(user_id=request.user, access_token=newAccessToken, refresh_token=eb.refresh_token)
            eatObj.save()
            headers = {
                'Authorization' : 'Bearer '+ str(newAccessToken),
                'Content-Type' : 'application/json',
                'Content-Language' : ContentLanguage,
            }
            response = requests.put("https://" + ebayApiUrl + "/sell/inventory/v1/inventory_item/" + sku, headers=headers, data=json.dumps(body))
    # print "RESPONSE Adding to inventory"
    # print response
    # print response.content
    appAccessToken = getAppAccessToken()
    headersApp = {
        'Authorization' : 'Bearer '+ appAccessToken,
        'Content-Type' : 'application/json',
        'Content-Language' : ContentLanguage,
    }
    response = requests.get("https://" + ebayApiUrl + "/commerce/taxonomy/v1_beta/get_default_category_tree_id?marketplace_id=" + marketplaceId, headers=headersApp)
    data = json.loads(response.content)
    categoryTreeId = data['categoryTreeId']
    # print "CATEGORY Tree ID"
    # print response
    # print categoryTreeId
    response = requests.get("https://" + ebayApiUrl + "/commerce/taxonomy/v1_beta/category_tree/" + categoryTreeId + "/get_category_suggestions?q=" + category, headers=headersApp)
    data = json.loads(response.content)
    categoryId = data['categorySuggestions'][0]['category']['categoryId']
    # print "CATEGORY ID"
    # print categoryId
    body = {
        "sku": sku,
        "marketplaceId": marketplaceId,
        "merchantLocationKey" : merchantLocationKey,
        "categoryId" : categoryId,
        "format": "FIXED_PRICE",
        "pricingSummary":
            { 
            "price":
                { 
                "currency": "USD",
                "value": int(productInfo.Product_SKU.price)
                }
            },
        "listingPolicies":
            {
            "paymentPolicyId": paymentPolicyId,
            "fulfillmentPolicyId": fulfillmentPolicyId,
            "returnPolicyId": returnPolicyId
            }
        }
    response = requests.post("https://" + ebayApiUrl + "/sell/inventory/v1/offer", headers=headers, data=json.dumps(body))
    if response.status_code == 401:
        newAccessToken = refreshAccessToken(eb.refresh_token)
        if newAccessToken == 400:
            return HttpResponseRedirect("/ebay/oauthEbay")
        else:
            eatObj = EbayAccessToken(user_id=request.user, access_token=newAccessToken, refresh_token=eb.refresh_token)
            eatObj.save()
            headers = {
                'Authorization' : 'Bearer '+ str(newAccessToken),
                'Content-Type' : 'application/json',
                'Content-Language' : ContentLanguage,
            }
            response = requests.post("https://" + ebayApiUrl + "/sell/inventory/v1/offer", headers=headers, data=json.dumps(body))
    data = json.loads(response.content)
    # print "RESPONSE OFFER"
    # print response
    # print data
    if 'offerId' in data:
        offerId = data['offerId']
        response = requests.post("https://" + ebayApiUrl + "/sell/inventory/v1/offer/" + offerId + "/publish/", headers=headers)
        if response.status_code == 401:
            newAccessToken = refreshAccessToken(eb.refresh_token)
            if newAccessToken == 400:
                return HttpResponseRedirect("/ebay/oauthEbay")
            else:
                eatObj = EbayAccessToken(user_id=request.user, access_token=newAccessToken, refresh_token=eb.refresh_token)
                eatObj.save()
                headers = {
                    'Authorization' : 'Bearer '+ str(newAccessToken),
                    'Content-Type' : 'application/json',
                    'Content-Language' : ContentLanguage,
                }
                response = requests.post("https://" + ebayApiUrl + "/sell/inventory/v1/offer/" + offerId + "/publish/", headers=headers)
        data = json.loads(response.content)
    else:
        return False
    # print "RESPONSE OFFER PUBLISH"
    # print response
    # print data
    # Sample data for Adding Product
    data = {
        "listingId": "223412345678"
    }
    print ("LISTING ID: " + str(data['listingId']))
    return HttpResponse(response.content)

def ebaygetOrders(request):
    eb = EbayAccessToken.objects.get(pk=request.user)
    headers = {
        'Authorization' : 'Bearer '+ eb.access_token,
    }
    response = requests.get("https://" + ebayApiUrl + "/sell/fulfillment/v1/order" , headers=headers)
    if response.status_code == 401:
        newAccessToken = refreshAccessToken(eb.refresh_token)
        if newAccessToken == 400:
            return HttpResponseRedirect("/ebay/oauthEbay")
        else:
            eatObj = EbayAccessToken(user_id=request.user, access_token=newAccessToken, refresh_token=eb.refresh_token)
            eatObj.save()
            headers = {
                'Authorization' : 'Bearer '+ str(newAccessToken),
            }
            response = requests.get("https://" + ebayApiUrl + "/sell/fulfillment/v1/order" , headers=headers)
    data = json.loads(response.content)
    # Sample data for orders
    data = {
            "href": "http://localhost:8080/sell/fulfillment/v1/order?filter=creationdate:%5B2016-09-29T15:05:43.026Z..%5D&limit=50&offset=0",
            "total": 4,
            "limit": 50,
            "offset": 0,
            "orders": [
                {
                    "orderId": "6498414015!260000000562911",
                    "creationDate": "2016-09-29T21:50:57.000Z",
                    "lastModifiedDate": "2016-09-29T21:53:00.000Z",
                    "legacyOrderId": "6498414015",
                    "orderFulfillmentStatus": "NOT_STARTED",
                    "orderPaymentStatus": "PENDING",
                    "sellerId": "ru_publicapi",
                    "buyer": {
                        "username": "rus_buyer"
                    },
                    "buyerCheckoutNotes": "Please show up!",
                    "pricingSummary": {
                        "priceSubtotal": {
                            "value": "99.96",
                            "currency": "USD"
                        },
                        "deliveryCost": {
                            "value": "0.0",
                            "currency": "USD"
                        },
                        "tax": {
                            "value": "4.5",
                            "currency": "USD"
                        },
                        "total": {
                            "value": "94.46",
                            "currency": "USD"
                        }
                    },
                    "paymentSummary": {
                        "totalDueSeller": {
                            "value": "94.46",
                            "currency": "USD"
                        },
                        "refunds": [],
                        "payments": [
                            {
                                "paymentMethod": "PAYPAL",
                                "amount": {
                                    "value": "94.46",
                                    "currency": "USD"
                                },
                                "paymentStatus": "PENDING",
                                "paymentHolds": [
                                    {
                                        "holdReason": "NEW_SELLER",
                                        "holdAmount": {
                                            "value": "94.46",
                                            "currency": "USD"
                                        },
                                        "holdState": "HELD"
                                    }
                                ]
                            }
                        ]
                    },
                    "fulfillmentStartInstructions": [
                        {
                            "fulfillmentInstructionsType": "SHIP_TO",
                            "shippingStep": {
                                "shipTo": {
                                    "fullName": "Dear friend",
                                    "contactAddress": {
                                        "addressLine1": "1395 Saratoga Ave Apt 101",
                                        "city": "San Jose",
                                        "stateOrProvince": "CA",
                                        "postalCode": "95129-4453",
                                        "countryCode": "US"
                                    },
                                    "email": "buyer1@sample.com",
                                    "primaryPhone": {
                                        "phoneNumber": "408 464 2712"
                                    }
                                },
                                "shippingServiceCode": "Promotional Shipping (varies)"
                            }
                        }
                    ],
                    "fulfillmentHrefs": [],
                    "lineItems": [
                        {
                            "lineItemId": "5575863026",
                            "legacyItemId": "350007451113",
                            "title": "Hanes T-ShirtMaker Plus Deluxe S00703",
                            "lineItemCost": {
                                "value": "79.98",
                                "currency": "USD"
                            },
                            "quantity": 2,
                            "soldFormat": "FIXED_PRICE",
                            "listingMarketplaceId": "EBAY_US",
                            "purchaseMarketplaceId": "EBAY_US",
                            "lineItemFulfillmentStatus": "NOT_STARTED",
                            "total": {
                                "value": "83.98",
                                "currency": "USD"
                            },
                            "deliveryCost": {},
                            "appliedPromotions": [
                                {
                                    "discountAmount": {
                                        "value": "0.0",
                                        "currency": "USD"
                                    },
                                    "promotionCode": "5001614705",
                                    "description": "Free shipping on orders over $50"
                                }
                            ],
                            "taxes": [
                                {
                                    "amount": {
                                        "value": "4.0",
                                        "currency": "USD"
                                    }
                                }
                            ]
                        },
                        {
                            "lineItemId": "5575864026",
                            "legacyItemId": "350007396635",
                            "legacyVariationId": "510002164224",
                            "title": "Good Quality Shirt",
                            "lineItemCost": {
                                "value": "19.98",
                                "currency": "USD"
                            },
                            "discountedLineItemCost": {
                                "value": "9.98",
                                "currency": "USD"
                            },
                            "quantity": 2,
                            "soldFormat": "FIXED_PRICE",
                            "listingMarketplaceId": "EBAY_US",
                            "purchaseMarketplaceId": "EBAY_US",
                            "lineItemFulfillmentStatus": "NOT_STARTED",
                            "total": {
                                "value": "10.48",
                                "currency": "USD"
                            },
                            "deliveryCost": {},
                            "appliedPromotions": [
                                {
                                    "discountAmount": {
                                        "value": "10.0",
                                        "currency": "USD"
                                    },
                                    "promotionCode": "5001692805",
                                    "description": "Buy 1, get 1 free"
                                },
                                {
                                    "discountAmount": {
                                        "value": "0.0",
                                        "currency": "USD"
                                    },
                                    "promotionCode": "5001614705",
                                    "description": "Free shipping on orders over $50"
                                }
                            ],
                            "taxes": [
                                {
                                    "amount": {
                                        "value": "0.5",
                                        "currency": "USD"
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    "orderId": "350007451113-10101916011!6064659011",
                    "creationDate": "2016-09-29T21:05:23.000Z",
                    "lastModifiedDate": "2016-09-29T21:06:14.000Z",
                    "legacyOrderId": "6498414015",
                    "orderFulfillmentStatus": "NOT_STARTED",
                    "orderPaymentStatus": "PENDING",
                    "sellerId": "ru_publicapi",
                    "buyer": {
                        "username": "sh_aish20"
                    },
                    "pricingSummary": {
                        "priceSubtotal": {
                            "value": "79.98",
                            "currency": "USD"
                        },
                        "deliveryCost": {
                            "value": "0.0",
                            "currency": "USD"
                        },
                        "tax": {
                            "value": "4.0",
                            "currency": "USD"
                        },
                        "total": {
                            "value": "83.98",
                            "currency": "USD"
                        }
                    },
                    "paymentSummary": {
                        "totalDueSeller": {
                            "value": "83.98",
                            "currency": "USD"
                        },
                        "refunds": [],
                        "payments": [
                            {
                                "paymentMethod": "PAYPAL",
                                "amount": {
                                    "value": "83.98",
                                    "currency": "USD"
                                },
                                "paymentStatus": "PENDING",
                                "paymentHolds": [
                                    {
                                        "holdReason": "NEW_SELLER",
                                        "holdAmount": {
                                            "value": "83.98",
                                            "currency": "USD"
                                        },
                                        "holdState": "NOT_HELD"
                                    }
                                ]
                            }
                        ]
                    },
                    "fulfillmentStartInstructions": [
                        {
                            "fulfillmentInstructionsType": "SHIP_TO",
                            "shippingStep": {
                                "shipTo": {
                                    "fullName": "qibus seller",
                                    "contactAddress": {
                                        "addressLine1": "2145 Hamilton Avenue",
                                        "city": "San Jose",
                                        "stateOrProvince": "CA",
                                        "postalCode": "95125",
                                        "countryCode": "US"
                                    },
                                    "email": "buyer1@sample.com",
                                    "primaryPhone": {
                                        "phoneNumber": "408 367 1234"
                                    }
                                },
                                "shippingServiceCode": "Promotional Shipping (varies)"
                            }
                        }
                    ],
                    "fulfillmentHrefs": [],
                    "lineItems": [
                        {
                            "lineItemId": "6264984011",
                            "legacyItemId": "350007451113",
                            "title": "Hanes T-ShirtMaker Plus Deluxe S00703",
                            "lineItemCost": {
                                "value": "79.98",
                                "currency": "USD"
                            },
                            "quantity": 2,
                            "soldFormat": "FIXED_PRICE",
                            "listingMarketplaceId": "EBAY_US",
                            "purchaseMarketplaceId": "EBAY_US",
                            "lineItemFulfillmentStatus": "NOT_STARTED",
                            "total": {
                                "value": "83.98",
                                "currency": "USD"
                            },
                            "deliveryCost": {},
                            "appliedPromotions": [
                                {
                                    "discountAmount": {
                                        "value": "0.0",
                                        "currency": "USD"
                                    },
                                    "promotionCode": "5001614705",
                                    "description": "Free shipping on orders over $50"
                                }
                            ],
                            "taxes": [
                                {
                                    "amount": {
                                        "value": "4.0",
                                        "currency": "USD"
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    "orderId": "350007451113-10101913011!6064657011",
                    "creationDate": "2016-09-29T20:40:49.000Z",
                    "lastModifiedDate": "2016-09-29T20:41:13.000Z",
                    "legacyOrderId": "6498414015",
                    "orderFulfillmentStatus": "NOT_STARTED",
                    "orderPaymentStatus": "PENDING",
                    "sellerId": "ru_publicapi",
                    "buyer": {
                        "username": "sh_aish20"
                    },
                    "pricingSummary": {
                        "priceSubtotal": {
                            "value": "79.98",
                            "currency": "USD"
                        },
                        "deliveryCost": {
                            "value": "0.0",
                            "currency": "USD"
                        },
                        "tax": {
                            "value": "4.0",
                            "currency": "USD"
                        },
                        "total": {
                            "value": "83.98",
                            "currency": "USD"
                        }
                    },
                    "paymentSummary": {
                        "totalDueSeller": {
                            "value": "83.98",
                            "currency": "USD"
                        },
                        "refunds": [],
                        "payments": [
                            {
                                "paymentMethod": "PAYPAL",
                                "amount": {
                                    "value": "83.98",
                                    "currency": "USD"
                                },
                                "paymentStatus": "PENDING",
                                "paymentHolds": [
                                    {
                                        "holdReason": "NEW_SELLER",
                                        "holdAmount": {
                                            "value": "83.98",
                                            "currency": "USD"
                                        },
                                        "holdState": "NOT_HELD"
                                    }
                                ]
                            }
                        ]
                    },
                    "fulfillmentStartInstructions": [
                        {
                            "fulfillmentInstructionsType": "SHIP_TO",
                            "shippingStep": {
                                "shipTo": {
                                    "fullName": "qibus seller",
                                    "contactAddress": {
                                        "addressLine1": "2145 Hamilton Avenue",
                                        "city": "San Jose",
                                        "stateOrProvince": "CA",
                                        "postalCode": "95125",
                                        "countryCode": "US"
                                    },
                                    "email": "buyer1@sample.com",
                                    "primaryPhone": {
                                        "phoneNumber": "408 367 1234"
                                    }
                                },
                                "shippingServiceCode": "Promotional Shipping (varies)"
                            }
                        }
                    ],
                    "fulfillmentHrefs": [],
                    "lineItems": [
                        {
                            "lineItemId": "6264982011",
                            "legacyItemId": "350007451113",
                            "title": "Hanes T-ShirtMaker Plus Deluxe S00703",
                            "lineItemCost": {
                                "value": "79.98",
                                "currency": "USD"
                            },
                            "quantity": 2,
                            "soldFormat": "FIXED_PRICE",
                            "listingMarketplaceId": "EBAY_US",
                            "purchaseMarketplaceId": "EBAY_US",
                            "lineItemFulfillmentStatus": "NOT_STARTED",
                            "total": {
                                "value": "83.98",
                                "currency": "USD"
                            },
                            "deliveryCost": {},
                            "appliedPromotions": [
                                {
                                    "discountAmount": {
                                        "value": "0.0",
                                        "currency": "USD"
                                    },
                                    "promotionCode": "5001614705",
                                    "description": "Free shipping on orders over $50"
                                }
                            ],
                            "taxes": [
                                {
                                    "amount": {
                                        "value": "4.0",
                                        "currency": "USD"
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    for order in data['orders']:
        orderCount = Order.objects.filter(po=str(order['orderId'])).count()
        if orderCount < 1:
            dateOrd = str(order['creationDate']).split("T")
            orderdate = datetime.datetime.strptime(dateOrd[0], '%Y-%m-%d')
            # sku = "Ebay001" # change with sku of each line item
            company = (Company.objects.filter(company_name=CompanyName.objects.get(companyname="eBay").companyname_id))[0]
            # company = Company.objects.get(company_sku=sku)
            courier = CourierMethod.objects.get(courier_method="UPS Ground")
            orderObj = Order(po=order['orderId'], user=request.user, company_name=company, order_date=orderdate.date(), courier_method=courier)
            orderObj.save()
            if 'shippingStep' in order['fulfillmentStartInstructions'][0]:
                name = order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo']['fullName']
                address = order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo']['contactAddress']['addressLine1']
                city = order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo']['contactAddress']['city']
                state = order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo']['contactAddress']['stateOrProvince'] if 'stateOrProvince' in order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo']['contactAddress'] else ""
                postalcode = order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo']['contactAddress']['postalCode'] if 'postalCode' in order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo']['contactAddress'] else ""
                phone = order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo']['primaryPhone']['phoneNumber'] if 'primaryPhone' in order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo'] else ""
                addressObj = Address(po_number=orderObj, customer_name=name, address=address, city=city, state=state, zip=postalcode, phone_number=phone)
                addressObj.save()
            for lineitem in order['lineItems']:
                lineItemObj = EbayLineItem(lineitem_id=lineitem['lineItemId'],order_id=orderObj,company_name="eBay",item_quantity=lineitem['quantity'])
                lineItemObj.save()
                ebay_order_qty = LineTable
                ebay_lineItem =ebay_order_qty(purchase_order =orderObj, vendor_SKU = lineitem['lineItemId'],order_quantity =lineitem['quantity']  )
                ebay_lineItem.save()
        else:
            addressCount = Address.objects.filter(po_number=str(order['orderId'])).count()
            if (addressCount < 1) and ('shippingStep' in order['fulfillmentStartInstructions'][0]):
                name = order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo']['fullName']
                address = order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo']['contactAddress']['addressLine1']
                city = order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo']['contactAddress']['city']
                state = order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo']['contactAddress']['stateOrProvince'] if 'stateOrProvince' in order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo']['contactAddress'] else ""
                postalcode = order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo']['contactAddress']['postalCode'] if 'postalCode' in order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo']['contactAddress'] else ""
                phone = order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo']['primaryPhone']['phoneNumber'] if 'primaryPhone' in order['fulfillmentStartInstructions'][0]['shippingStep']['shipTo'] else ""
                orderObj = Order.objects.get(po=str(order['orderId']))
                addressObj = Address(po_number=orderObj, customer_name=name, address=address, city=city, state=state, zip=postalcode, phone_number=phone)
                addressObj.save()
            else:
                pass
    return HttpResponseRedirect("/ordermanage")#HttpResponse(response.content)#HttpResponse(json.dumps(data))

def ebayfulfillOrder(request):
    eb = EbayAccessToken.objects.get(pk=request.user)
    lineItemObjs = EbayLineItem.objects.filter(order_id=request.POST['po'])
    lineItemObj = EbayLineItem.objects.get(pk=lineItemObjs[0].lineitem_id)
    lineItemObj.tracking_id = request.POST['traking_number']
    lineItemObj.save()
    headers = {
        'Authorization' : 'Bearer '+ eb.access_token,
        'Content-Type' : 'application/json',
    }
    body = {
        "lineItems" : [
            {
                "lineItemId" : str(lineItemObj.lineitem_id),
                "quantity" : lineItemObj.item_quantity
            }
        ],
        # "shippedDate" : "string",
        "shippingCarrierCode" : "UPS Ground",#"FedEx", #Change according to seller request
        "trackingNumber" : str(lineItemObj.tracking_id) #request.POST['traking_number']
    }
    response = requests.get("https://" + ebayApiUrl + "/sell/fulfillment/v1/order/" + str(lineItemObj.order_id) + "/shipping_fulfillment" , headers=headers, data=body)
    if response.status_code == 401:
        newAccessToken = refreshAccessToken(eb.refresh_token)
        if newAccessToken == 400:
            return HttpResponseRedirect("/ebay/oauthEbay")
        else:
            eatObj = EbayAccessToken(user_id=request.user, access_token=newAccessToken, refresh_token=eb.refresh_token)
            eatObj.save()
            headers = {
                'Authorization' : 'Bearer '+ str(newAccessToken),
                'Content-Type' : 'application/json',
            }
            response = requests.get("https://" + ebayApiUrl + "/sell/fulfillment/v1/order/" + str(lineItemObj.order_id) + "/shipping_fulfillment" , headers=headers, data=body)
    # if response.status_code == 201:
    #     locationHeader = str(response.headers['Location'])
    '''
        >>>> Dummy Response we will get after success of fulfillment api
    '''
    locationHeader = "https://api.ebay.com/sell/fulfillment/v1/order/6498414015!260000000562911/shipping_fulfillment/1Z50992656936"
    lh = locationHeader.split('/')
    fulfillmentId = lh[-1]
    print ("===============> FULFILLMENT ID: ",fulfillmentId)
    return HttpResponse(response)

