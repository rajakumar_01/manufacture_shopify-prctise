from django.conf.urls import url
from django.conf.urls.static import static
from . import views

urlpatterns = [
    url(r'^$', views.ShopifyRequest, name='shopifyrequest'), # index page 
    # url(r'^response/$', views.ReciveOrder, name='response'), # recive order
    # url(r'^fullfill/$', views.OrderFullfill, name='fullfill'),
    # url(r'^inventory/$', views.CreateInventory, name='inventory'), # ctreate inventory
    # url(r'^create_product/$', views.CreateProduct, name='create_product'),
    url(r'^token1/$', views.ShopifyLogin, name='token1'), # token page 
    url(r'^catch/$', views.ShopifyRedirect, name='catch'), 
    url(r'^orderresponse/$', views.ReciveOrderByToken, name='orderresponse'),
    url(r'^createproduct/$', views.products, name='createproduct'),
    url(r'^updateproduct/$', views.CreatePublicInventory, name='updateproduct'),
    url(r'^orderfullfill/$', views.OrderPublicFullfill, name='orderfullfill'),
    url(r'^allproducts/$', views.all_products, name='allproducts'),
    

]
 