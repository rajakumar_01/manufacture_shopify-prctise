# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class ShopifyAppConfig(AppConfig):
    name = 'shopify_app'
