# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse
from .models import *
from ordermanage.models import *
import json, requests


# Create your views here.
##########################################################
#=============shopify Request ============================
##########################################################

def ShopifyRequest(request):
    return render(request, 'shopify_app/index.html', {})

'''==========================================================================
    Project Name: Shopify Integration
    Function Name: Recive Order request Codes 
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def ReciveOrder(request): 
    if request.method == 'GET':
        # API_KEY = 'a5aab5ac3960fc8a59f975825ab6b759'
        # PASSWORD = 'adc4aed5302ff9b07e10d5f864806d4a'
        API_KEY = '9d2f128f479705d229ad06bce043165e'
        PASSWORD = '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'
        shop_url = "https://ordermanaged.myshopify.com/admin/orders.json"        
        response = requests.get(shop_url ,verify=False, auth=(API_KEY, PASSWORD) )
        print ("=========> shopify response")
        data = json.loads(response.content)       
        order_request = GetOrder
        
        shopify_order = Order       # Order(main table )Objects     
        for order in data["orders"]:            
            print (order["id"])
            po =order["id"]
            print (type(po))
            dd= order["created_at"]
            dateOrd = str(dd).split("T")
            print (dateOrd)
            orderdate= datetime.datetime.strptime(dateOrd[0], '%Y-%m-%d')
            print (orderdate)
            print (type(orderdate))
            id = order['customer']['id']
            name = order['customer']['first_name']
            contact_number =order['customer']['phone']
            shopify_recive_order = shopify_order(po=str(po) ,user =User.objects.get(id=2) ,company_name=Company.objects.get(id=3) ,courier_method=CourierMethod.objects.get(id=1), order_date= orderdate)
            shopify_recive_order.save()            
            order_recive = order_request(customer_id=id ,customer_name= name, contact_number =contact_number  )
            order_recive.save()
        return redirect('index')

'''==========================================================================
    Project Name: Shopify Integration
    Function Name: OrderFullfill request Codes 
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def OrderFullfill(request):
    query_set = Order.objects.all().filter(company_name =3) 
    for i in query_set:
        po = i.po
        print (po)          
        # API_KEY = 'a5aab5ac3960fc8a59f975825ab6b759'
        # PASSWORD = 'adc4aed5302ff9b07e10d5f864806d4a'
        API_KEY = '9d2f128f479705d229ad06bce043165e'
        PASSWORD = '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'
        #shop_url = "https://ordermanage.myshopify.com/admin/orders/805312987194/fulfillments.json"    
        headers  = {'Content-type':'application/json'}
        datas = {
                    "fulfillment": {
                        "order_id":po,                    
                }
            }   
        response = requests.post('https://ordermanage.myshopify.com/admin/orders/'+po+ '/fulfillments.json', headers =headers, auth=(API_KEY, PASSWORD ), verify=True, data= json.dumps(datas))

    return HttpResponse(response) 

'''==========================================================================
    Project Name: Shopify Integration
    Function Name: CreateInventory request Codes 
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def CreateInventory(request):
    # API_KEY = 'a5aab5ac3960fc8a59f975825ab6b759'
    # PASSWORD = 'adc4aed5302ff9b07e10d5f864806d4a'
    API_KEY = '9d2f128f479705d229ad06bce043165e'
    PASSWORD = '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'
    headers  = {'Content-type':'application/json'}
    shop_url = "https://ordermanage.myshopify.com/admin/inventory_items.json"
    datas = {
                "inventory_item": { 
                                    'id':1942458302524,     
                                    "sku": "001 new sku",
                                  }
            }
    response = requests.put("https://ordermanage.myshopify.com/admin/inventory_items.json" , headers =headers, auth=(API_KEY, PASSWORD ), verify=True, data= json.dumps(datas))
    # response = requests.put("https://ordermanage.myshopify.com/admin/inventory_items.json" , data=datas)
    
    print (response)
    return HttpResponse(response) 

'''==========================================================================
    Project Name: Shopify Integration
    Function Name: CreateProduct request Codes 
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def CreateProduct(request):
    # API_KEY = '9d2f128f479705d229ad06bce043165e'
    # PASSWORD = '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'
    api_key = '9d2f128f479705d229ad06bce043165e'
    api_secret_key = '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'
    headers  = {'Content-type':'application/json',
                # 'X-Shopify-Access-Token':'72b3bee1ce7aae5a958f6b5db3eba5a3',
                
                }

    datas = {
                "product": {
                                "title": "new product by ordermanage with api",
                                "body_html": "<strong>Good snowboard!with api</strong>",
                                "vendor": "ordermanage with api",
                                "product_type": "ordermanage",
                                "tags": "bedsheets, Bedsheets;"
                            }
                }
    # print(API_KEY)
    # print(PASSWORD)
    # request.put( "https://ordermanage.myshopify.com/admin/inventory_items.json", auth=(API_KEY, PASSWORD ), verify=True, data= json.dumps(datas) )
    # response = requests.put("https://ordermanage.myshopify.com/admin/inventory_items.json" , headers =headers, auth=(API_KEY, PASSWORD ), verify=True, data= json.dumps(datas))
   
    # response= requests.post("https://ordermanaged.myshopify.com/admin/products.json", headers =headers, auth=(API_KEY, PASSWORD ), verify=True, data= json.dumps(datas), )
    response= requests.post("https://ordermanaged.myshopify.com/admin/products.json",auth=(api_key,api_secret_key))
    print (response)
    return HttpResponse(response)

'''==========================================================================
    Project Name: Shopify Integration
    Function Name: ShopifyLogin request Codes 
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def ShopifyLogin(request):
    api_key = '9d2f128f479705d229ad06bce043165e'
    api_secret_key = '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'
    # api_key = 'ead441fdcf80b836985b08647f6119c0'
    # api_secret_key= '8e243f0dd108f7402056831f5d72c22d'
    scope =str(['read_products'])    
    response = "https://ordermanaged.myshopify.com/admin/oauth/authorize?client_id="+api_key +'&scope=write_products,read_orders,write_orders' +'&redirect_uri='+'http://127.0.0.1:8000/shopify_app/catch'+'&state=12345'
    return HttpResponseRedirect(response)

def ShopifyRedirect(request):
    api_key = '9d2f128f479705d229ad06bce043165e'
    api_secret_key = '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'
    getresponse  = request.GET.get('shop')
    code= str(request.GET.get('code'))   
    timestamp= request.GET.get('timestamp')
    hmac = request.GET.get('hmac')
    headers  = {'Content-type':'application/json'}
    datas = {        
        'client_id':api_key,
        'client_secret':api_secret_key,
        'code':code,
        }
    url =requests.post('https://ordermanaged.myshopify.com/admin/oauth/access_token', data=datas)
    print(url.content)
    a=  json.loads(url.content)
    query_accesstoken = ShopifyAccessToken
    if a:
        instance = query_accesstoken(shopify_shop=getresponse ,access_token =a['access_token'], scope = a['scope'] )
        instance.save()    
    return redirect('shopifyrequest')

"""===========================================================
===================recive Order with token key ==============
============================================================"""
def ReciveOrderByToken(request): 
    access_token = ShopifyAccessToken.objects.get(shopify_shop= "ordermanaged.myshopify.com")  
    print (access_token.access_token)
    if request.method == 'GET':
        headers ={
            'Content-Type':'application/json',
            'X-Shopify-Access-Token':access_token.access_token,       
        }
        shop_url = "https://ordermanaged.myshopify.com/admin/orders.json"        
        response = requests.get(shop_url , headers = headers )
        data = json.loads(response.content)       
        order_request = GetOrder
        customer_address = Address        
        shopify_order = Order       # Order(main table )Objects
        company = (Company.objects.filter(company_name=CompanyName.objects.get(companyname="Shopify").companyname_id))[0]     
        courier = CourierMethod.objects.get(courier_method="UPS Ground")
        shopi_order_qty = LineTable      
        for order in data["orders"]:    
            po =order["id"]
            dd= order["created_at"]
            dateOrd = str(dd).split("T")
            orderdate= datetime.datetime.strptime(dateOrd[0], '%Y-%m-%d')
            id = order['customer']['id']
            name = order['customer']['first_name']
            contact_number =order['customer']['phone']
            shopify_recive_order = shopify_order(po=str(po) ,user =User.objects.get(id=request.user.id) ,company_name=company ,courier_method=courier, order_date= orderdate)
            shopify_recive_order.save()            
            order_recive = order_request(customer_id=id ,customer_name= name, contact_number =contact_number  )
            order_recive.save()
            shopify_lineItem =shopi_order_qty(purchase_order =Order.objects.get(po=po), vendor_SKU = order['line_items'][0]['sku'],order_quantity =order['line_items'][0]['fulfillable_quantity']  )
            shopify_lineItem.save()
            phone = ""
            if order['phone']:
                phone = order['phone']
            else:
                phone = order['email']
            customer_address_query = customer_address(po_number=Order.objects.get(po=po),
                                     customer_name=order['customer']['default_address']['name'],
                                     city =order['customer']['default_address']['city'],
                                     address =(order['customer']['default_address']['name']                        
                                     +', ' +order['customer']['default_address']['address1']
                                     +', ' +order['customer']['default_address']['address2']
                                     +', ' +order['customer']['default_address']['city']
                                     + ', ' +order['customer']['state']
                                     + ', ' +order['customer']['default_address']['country'] ),
                                     state = order['customer']['state'],
                                     zip =order['customer']['default_address']['zip'],
                                    phone_number =phone,                                     
                                    )
            customer_address_query.save()
        return redirect('index')


def products(request):
    access_token = ShopifyAccessToken.objects.get(shopify_shop= "ordermanaged.myshopify.com")  
    print (access_token.access_token)
    headers ={
        'Content-Type':'application/json',
        'X-Shopify-Access-Token':access_token.access_token,       
    }
    datas = {
                "product": {    
                                
                                "title": "autkey1",
                                "body_html": "<strong>Good snowboard!</strong>",
                                "vendor": "ordermanaged",
                                "product_type": "ordermanaged",
                                "tags": "bedsheets, Bedsheets;"
                            }
                }
    response= requests.post("https://ordermanaged.myshopify.com/admin/products.json", data=json.dumps(datas), headers=headers)    
    print (response)
    return HttpResponse(response)


"""====================================================
create Invetory With access Token 
====================================================="""


def CreatePublicInventory(request):
    access_token = ShopifyAccessToken.objects.get(shopify_shop= "ordermanaged.myshopify.com")

    headers ={
        'Content-Type':'application/json',
        'X-Shopify-Access-Token':access_token.access_token,       
    }
    # shop_url = "https://ordermanaged.myshopify.com/admin/inventory_items.json"
    datas = {
                "variant": { 
                                    'price':'252.00',   
                                    'option1': 'yellow'
                                  }
            }                                                                   #adding product id with the url to add inventory varient
    response = requests.post("https://ordermanaged.myshopify.com/admin/products/4557537181749/variants.json" , data= json.dumps(datas), headers =headers,)
    print(response)
    return HttpResponse(response)

"""====================================================
Order Fullfillment With access Token 
====================================================="""

def OrderPublicFullfill(request):
    access_token = ShopifyAccessToken.objects.get(shopify_shop= "ordermanaged.myshopify.com")
    headers ={
        'Content-Type':'application/json',
        'X-Shopify-Access-Token':access_token.access_token,       
    }
    query_set = Order.objects.all().filter(company_name__company_name__companyname='Shopify')
    for i in Order.objects.all():
        print(i.company_name.company_name.companyname)
        print(type(i.company_name.company_name.companyname))
    print(query_set)
    for i in query_set:
        po = i.po
        print (po       )
    datas = {
                "fulfillment": {
                    "order_id":po,                    
            }
        }   
    response = requests.post('https://ordermanaged.myshopify.com/admin/orders/'+po+ '/fulfillments.json', headers =headers, data= json.dumps(datas))
    return HttpResponse(response)

'''==========================================
   ==============UPS INtegration ============
   =========================================='''
def all_products(request):
    access_token = ShopifyAccessToken.objects.get(shopify_shop= "ordermanaged.myshopify.com")
    headers ={
        'Content-Type':'application/json',
        'X-Shopify-Access-Token':access_token.access_token,       
    }
    response=requests.get("https://ordermanaged.myshopify.com/admin/products.json",headers= headers)
    print(response)
    return HttpResponse(response)

def createOrder(request):
    access_token=access_token = ShopifyAccessToken.objects.get(shopify_shop= "ordermanaged.myshopify.com")