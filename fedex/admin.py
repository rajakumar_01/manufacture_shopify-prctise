# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models  import * 

# Register your models here.
admin.site.register(FedexConfigration)
admin.site.register(ShipperContactInfo)
admin.site.register(ShipperAddress)
admin.site.register(FedExServiceType)
admin.site.register(FedExServices)




