# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import messages
from django.shortcuts import render
import requests
import datetime
from .forms import *
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse
from xml.dom import minidom
import urllib, requests, json, base64, hmac, hashlib, datetime
# from fedex.config import FedexConfig
import os
import logging
import sys
#from fedex.config import FedexConfig 
# from fedex.services.ship_service import FedexProcessShipmentRequest

def AccountDetails():
   context ={
      "Key":"ljhXbT89AEedhHW1",
      "Password":"htpN72nNmkbZXMPiROe7yO7nz",
      "AccountNumber":"510088000",
      "MeterNumber":"119096606",
   }
   return(context)

'''==========================================================================
    Project Name: FedExApi Integration
    Function Name: Fedex configrations Request
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''
def parse_element(element):
    dict_data = dict()
    if element.nodeType == element.TEXT_NODE:
        dict_data['data'] = element.data
    if element.nodeType not in [element.TEXT_NODE, element.DOCUMENT_NODE, 
                                element.DOCUMENT_TYPE_NODE]:
        for item in element.attributes.items():
            dict_data[item[0]] = item[1]
    if element.nodeType not in [element.TEXT_NODE, element.DOCUMENT_TYPE_NODE]:
        for child in element.childNodes:
            child_name, child_dict = parse_element(child)
            if child_name in dict_data:
                try:
                    dict_data[child_name].append(child_dict)
                except AttributeError:
                    dict_data[child_name] = [dict_data[child_name], child_dict]
            else:
                dict_data[child_name] = child_dict 
    return element.nodeName, dict_data
"""====================
*** Working Code ******
======================="""
'''==========================================================================
    Project Name: FedExApi Integration
    Function Name: Fedex configrations Request
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''
def FedexConfigrations(request):
   
   if request.method == "POST":
      form = FedexConfigrationsForms(request.POST or None)
      if form.is_valid():
         instance = form.save(commit=False)         
         instance.save()
         messages.success(request, "successfully created")
   else:
      form = FedexConfigrationsForms()

   context = {'form':form, "query_set":FedexConfigration.objects.all()}
   return render(request, "fedex_configrations.html", context)

"""====================
*** Working Code ******
======================="""
'''==========================================================================
    Project Name: FedExApi Integration
    Function Name: ShipService Request
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''
def make_unicode(input):
    if type(input) != unicode:
        input =  input.decode('UTF-8')
        return input
    else:
        return input

def ShipService(request):
    headers = {'Content-Type':'text/xml'}
    b = AccountDetails()    
    date =(datetime.datetime.now()).isoformat()   

    data = """<?xml version="1.0" encoding="UTF-8"?>
                <ProcessShipmentRequest xmlns="http://fedex.com/ws/ship/v23" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	              xsi:noNamespaceSchemaLocation='https://www.fedex.com/us/developer/downloads/xml/2018/advanced/ShipService_v23.xsd'>
                <WebAuthenticationDetail>
                    <UserCredential>
                        <Key>{b[Key]}</Key>
                        <Password>{b[Password]}</Password>
                    </UserCredential>
                </WebAuthenticationDetail>
                <ClientDetail>
                    <AccountNumber>{b[AccountNumber]}</AccountNumber>
                    <MeterNumber>{b[MeterNumber]}</MeterNumber>
                </ClientDetail>
                <!--<TransactionDetail>
                   // <CustomerTransactionId>ProcessShipmentRequest</CustomerTransactionId>
                //</TransactionDetail>-->
                <Version>
                    <ServiceId>ship</ServiceId>
                    <Major>23</Major>
                    <Intermediate>0</Intermediate>
                    <Minor>0</Minor>
                </Version>
                <RequestedShipment>
                    <ShipTimestamp>{date}</ShipTimestamp>
                    <DropoffType>REGULAR_PICKUP</DropoffType>
                    <ServiceType>FEDEX_GROUND</ServiceType>
                    <PackagingType>YOUR_PACKAGING</PackagingType>
                    <Shipper>
                        <AccountNumber>{b[AccountNumber]}</AccountNumber>
                        <Contact>
                            <PersonName>Shivam</PersonName>
                            <CompanyName>eKnous</CompanyName>
                            <PhoneNumber>6097590669</PhoneNumber>
                            <EMailAddress>shivam@eknous.com</EMailAddress>
                        </Contact>
                        <Address>
                            <StreetLines>Sender_Address_Line1</StreetLines>
                            <StreetLines>Sender_Address_Line2</StreetLines>
                            <City>MEMPHIS</City>
                            <StateOrProvinceCode>TN</StateOrProvinceCode>
                            <PostalCode>38117</PostalCode>
                            <CountryCode>US</CountryCode>
                        </Address>
                    </Shipper>
                    <Recipient>
                        <Contact>
                            <PersonName>Shivam</PersonName>
                            <CompanyName>eKnous</CompanyName>
                            <PhoneNumber>6097590669</PhoneNumber>
                            <EMailAddress>vastavshivam@gmail.com</EMailAddress>
                        </Contact>
                        <Address>
                            <StreetLines>Recipient_Address_Line1</StreetLines>
                            <StreetLines>Recipient_Address_Line2</StreetLines>
                            <City>Herndon</City>
                            <StateOrProvinceCode>VA</StateOrProvinceCode>
                            <PostalCode>20171</PostalCode>
                            <CountryCode>US</CountryCode>
                        </Address>
                    </Recipient>
                    <ShippingChargesPayment>
                        <PaymentType>SENDER</PaymentType>
                        <Payor>
                            <ResponsibleParty>
                                <AccountNumber>{b[AccountNumber]}</AccountNumber>
                                <Tins>
                                    <TinType>BUSINESS_STATE</TinType>
                                    <Number>119096606</Number>
                                </Tins>
                                <Contact>
                                    <ContactId>12345</ContactId>
                                    <PersonName>Anuj Kumar</PersonName>
                                </Contact>
                            </ResponsibleParty>
                        </Payor>
                    </ShippingChargesPayment>                    
                    <LabelSpecification>
                        <LabelFormatType>COMMON2D</LabelFormatType>
                        <ImageType>PNG</ImageType>
                        <LabelStockType>PAPER_7X4.75</LabelStockType>
                    </LabelSpecification>
                    <RateRequestTypes>LIST</RateRequestTypes>
                    <PackageCount>1</PackageCount>
                    <RequestedPackageLineItems>
                        <SequenceNumber>1</SequenceNumber>
                        <Weight>
                            <Units>LB</Units>
                            <Value>20.0</Value>
                        </Weight>
                        <Dimensions>
                            <Length>12</Length>
                            <Width>12</Width>
                            <Height>12</Height>
                            <Units>IN</Units>
                        </Dimensions>
                        <CustomerReferences>
                            <CustomerReferenceType>CUSTOMER_REFERENCE</CustomerReferenceType>
                            <Value>string</Value>
                        </CustomerReferences>
                    </RequestedPackageLineItems>                    
            </RequestedShipment>       
        </ProcessShipmentRequest>""".format(**vars())                            
    response = requests.post('https://wsbeta.fedex.com:443/xml', data=data, headers=headers)
   #  response = requests.post( "https://ws.fedex.com:443/xml", headers=headers, data=data )
    print (response)
   #  parsedXml = minidom.parseString(response.content)
   #  jsonData = json.dumps(parse_element(parsedXml), sort_keys=True, indent=4)
   #  feedResponse = json.loads(jsonData)
   #  print feedResponse
   #  return render(request, "json.json" , {'dict':feedResponse}, content_type='application/json')
   # #  return HttpResponse(feedResponse)
    return HttpResponse(response)

'''==========================================================================
    Project Name: FedExApi Integration
    Function Name: FdxRate Request
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def FdxRate(request):
    headers = {'Content-Type': 'text/xml'}
    b = AccountDetails()
    data = """<?xml version="1.0" encoding="UTF-8"?>
        <RateRequest xmlns="http://fedex.com/ws/rate/v24" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xs="http://www.w3.org/2001/XMLSchema" xsi:noNamespaceSchemaLocation='https://www.fedex.com/us/developer/downloads/xml/2018/standard/RateService_v24.xsd'>
         <WebAuthenticationDetail>
         	<ParentCredential>
               <Key>{b[Key]}</Key>
               <Password>{b[Password]}</Password>
            </ParentCredential>
            <UserCredential>
               <Key>{b[Key]}</Key>
               <Password>{b[Password]}</Password>
            </UserCredential>
         </WebAuthenticationDetail>
         <ClientDetail>
            <AccountNumber>{b[AccountNumber]}</AccountNumber>
            <MeterNumber>{b[MeterNumber]}</MeterNumber>
         </ClientDetail>
         <!--<TransactionDetail>
            <CustomerTransactionId>INPUT YOUR INFORMATION</CustomerTransactionId>
         </TransactionDetail>-->
         <Version>
            <ServiceId>crs</ServiceId>
            <Major>24</Major>
            <Intermediate>0</Intermediate>
            <Minor>0</Minor>
         </Version>
         <RequestedShipment>
            <ShipTimestamp>2018-12-18T12:34:56-06:00</ShipTimestamp>
            <DropoffType>REGULAR_PICKUP</DropoffType>
            <TotalWeight>
               <Units>LB</Units>
               <Value>50.0</Value>
            </TotalWeight>
            <Shipper>
               <Contact>
                  <CompanyName>eKnous LLC</CompanyName>
                  <PhoneNumber>12312345678</PhoneNumber>
               </Contact>
               <Address>
                  <StreetLines>100, Overlook Princeton</StreetLines>
                  <StreetLines>Princeton</StreetLines>
                  <City>Princeton</City>
                  <StateOrProvinceCode>NJ</StateOrProvinceCode>
                  <PostalCode>08540</PostalCode>
                  <CountryCode>US</CountryCode>
               </Address>
            </Shipper>
            <Recipient>
               <Contact>
                  <PersonName>shivam</PersonName>
                  <PhoneNumber>1234561234</PhoneNumber>
               </Contact>
               <Address>
                  <StreetLines>100, Overlook Princeton</StreetLines>
                  <StreetLines>Princeton</StreetLines>
                  <City>Princeton</City>
                  <StateOrProvinceCode>NJ</StateOrProvinceCode>
                  <PostalCode>08540</PostalCode>
                  <CountryCode>US</CountryCode>
               </Address>
            </Recipient>
            <ShippingChargesPayment>
               <PaymentType>SENDER</PaymentType>
               <Payor>
                  <ResponsibleParty>
                     <AccountNumber>510088000</AccountNumber>
                  </ResponsibleParty>
               </Payor>
            </ShippingChargesPayment>
            <RateRequestTypes>LIST</RateRequestTypes>
            <PackageCount>1</PackageCount>
            <RequestedPackageLineItems>
               <SequenceNumber>1</SequenceNumber>
               <GroupNumber>1</GroupNumber>
               <GroupPackageCount>1</GroupPackageCount>
               <Weight>
                  <Units>LB</Units>
                  <Value>20.0</Value>
               </Weight>
               <Dimensions>
            	<Length>12</Length>
                <Width>12</Width>
                <Height>12</Height>
                <Units>IN</Units>
               </Dimensions>
               <!--<ContentRecords>
                  <PartNumber>INPUT YOUR INFORMATION</PartNumber>
                  <ItemNumber>INPUT YOUR INFORMATION</ItemNumber>
                  <ReceivedQuantity>INPUT YOUR INFORMATION</ReceivedQuantity>
                  <Description>INPUT YOUR INFORMATION</Description>
               </ContentRecords>-->
            </RequestedPackageLineItems>
         </RequestedShipment>
      </RateRequest>""".format(**vars())
    response = requests.post( "https://wsbeta.fedex.com:443/xml", data=data, headers= headers)
    print (response.content )
    return HttpResponse(response) 

'''==========================================================================
    Project Name: FedExApi Integration
    Function Name: FdxTrackRequest Request
    URL: https://eknous.com
    Author: Eknous Technology Solutions   
    License: https://eknous.com/license/
==========================================================================='''

def FdxTrackRequest(request):
    headers = {'Content-Type': 'text/xml'}
    b = AccountDetails()
    data = """<?xml version="1.0" encoding="UTF-8"?>
        <TrackRequest xmlns="http://fedex.com/ws/track/v3">
        <WebAuthenticationDetail>
            <UserCredential>
                <Key>{b[Key]}</Key>
                <Password>{b[Password]}</Password>
            </UserCredential>
        </WebAuthenticationDetail>
        <ClientDetail>
            <AccountNumber>{b[AccountNumber]}</AccountNumber>
            <MeterNumber>{b[MeterNumber]}</MeterNumber>
        </ClientDetail>
        <TransactionDetail>
            <CustomerTransactionId>ActiveShipping</CustomerTransactionId>
        </TransactionDetail>
        <Version>
            <ServiceId>trck</ServiceId>
            <Major>3</Major>
            <Intermediate>0</Intermediate>
            <Minor>0</Minor>
        </Version>
        <PackageIdentifier>
            <Value>111111111111</Value>
            <Type>TRACKING_NUMBER_OR_DOORTAG</Type>
        </PackageIdentifier>
        <TrackingNumberUniqueIdentifier>
            
        </TrackingNumberUniqueIdentifier>
        <IncludeDetailedScans>1</IncludeDetailedScans>
        </TrackRequest>""".format(**vars())

    # response = requests.post( "https://wsbeta.fedex.com:443/web-services/track", data=data)
    response = requests.post( "https://wsbeta.fedex.com:443/xml", data=data)
    # response = requests.post( "https://gatewaybeta.fedex.com/xml", data=data)
    print (response.content )
    return HttpResponse(response)

'''==========================================================================
    Project Name: FedExApi Integration
    Function Name: LocationServicesByPhone Request
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def LocationServicesByPhone(request):
    headers = {'Content-Type': 'text/xml'}
    b = AccountDetails()
    data = """<?xml version="1.0" encoding="UTF-8"?>
          <SearchLocationsRequest xmlns="http://fedex.com/ws/locs/v9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation='https://www.fedex.com/us/developer/downloads/xml/2018/standard/LocationService_v9.xsd'>
         <WebAuthenticationDetail>
            <UserCredential>
                <Key>{b[Key]}</Key>
                <Password>{b[Password]}</Password>
            </UserCredential>
         </WebAuthenticationDetail>
         <ClientDetail>
             <AccountNumber>{b[AccountNumber]}</AccountNumber>
            <MeterNumber>{b[MeterNumber]}</MeterNumber>
            <Region>US</Region>
         </ClientDetail>
         <Version>
            <ServiceId>locs</ServiceId>
            <Major>9</Major>
            <Intermediate>0</Intermediate>
            <Minor>0</Minor>
         </Version>
         <LocationsSearchCriterion>PHONE_NUMBER</LocationsSearchCriterion>
         <Address>
            <StreetLines>100, Overlook Princeton</StreetLines>
            <City>Princeton</City>
            <StateOrProvinceCode>NJ</StateOrProvinceCode>
            <PostalCode>08540</PostalCode>
            <CountryCode>US</CountryCode>
            <Residential>0</Residential>
            <GeographicCoordinates>sciret dare</GeographicCoordinates>
         </Address>
         <PhoneNumber>6097590669</PhoneNumber>
         <MultipleMatchesAction>RETURN_ALL</MultipleMatchesAction>
      </SearchLocationsRequest>""".format(**vars())
    response = requests.post( "https://wsbeta.fedex.com:443/xml", data=data, headers=headers)    
    print (response.content )
    return HttpResponse(response)

'''==========================================================================
    Project Name: FedExApi Integration
    Function Name: LocationServicesByPostalCode Request
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def LocationServicesByPostalCode(request):
    headers = {'Content-Type': 'text/xml'}
    b = AccountDetails()
    data = """<?xml version="1.0" encoding="UTF-8"?>
        <v9:SearchLocationsRequest xmlns:v9="http://fedex.com/ws/locs/v9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation='https://www.fedex.com/us/developer/downloads/xml/2018/standard/LocationService_v9.xsd'>
         <v9:WebAuthenticationDetail>
            <v9:UserCredential>
               <v9:Key>{b[Key]}</v9:Key>
               <v9:Password>{b[Password]}</v9:Password>
            </v9:UserCredential>
         </v9:WebAuthenticationDetail>
         <v9:ClientDetail>
            <v9:AccountNumber>{b[AccountNumber]}</v9:AccountNumber>
            <v9:MeterNumber>{b[MeterNumber]}</v9:MeterNumber>
            <v9:Region>US</v9:Region>
         </v9:ClientDetail>
         <v9:Version>
            <v9:ServiceId>locs</v9:ServiceId>
            <v9:Major>9</v9:Major>
            <v9:Intermediate>0</v9:Intermediate>
            <v9:Minor>0</v9:Minor>
         </v9:Version>
         <v9:EffectiveDate>2018-12-18T18:43:56-06:00</v9:EffectiveDate>
         <v9:LocationsSearchCriterion>ADDRESS</v9:LocationsSearchCriterion>
         <v9:Address>
            <StreetLines>100, Overlook Princeton</StreetLines>
            <City>Princeton</City>
            <StateOrProvinceCode>NJ</StateOrProvinceCode>
            <PostalCode>08540</PostalCode>
            <CountryCode>US</CountryCode>
            <Residential>0</Residential>
            <GeographicCoordinates>sciret dare</GeographicCoordinates>
         </v9:Address>
         <v9:PhoneNumber/>""".format(**vars())
    response = requests.post( "https://wsbeta.fedex.com:443/xml", data=data, headers=headers)    
    print (response.content )
    return HttpResponse(response)

'''==========================================================================
    Project Name: FedExApi Integration
    Function Name: ValidationAvailabilityAndCommitmentService Request
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def ValidationAvailabilityAndCommitmentService(request):
  headers = {'Content-Type': 'text/xml'}
  b = AccountDetails()
  data = """<?xml version="1.0" encoding="UTF-8"?>
    <v8:ServiceAvailabilityRequest xmlns:v8="http://fedex.com/ws/vacs/v8"
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:noNamespaceSchemaLocation='https://www.fedex.com/us/developer/downloads/xml/2018/standard/ValidationAvailabilityAndCommitmentService_v8.xsd'>
         <v8:WebAuthenticationDetail>
            <v8:ParentCredential>
               <v8:Key>{b[Key]}</v8:Key>
               <v8:Password>{b[Password]}</v8:Password>
            </v8:ParentCredential>
            <v8:UserCredential>
               <v8:Key>ljhXbT89AEedhHW1</v8:Key>
               <v8:Password>htpN72nNmkbZXMPiROe7yO7nz</v8:Password>
            </v8:UserCredential>
         </v8:WebAuthenticationDetail>
         <v8:ClientDetail>
            <v8:AccountNumber>{b[AccountNumber]}</v8:AccountNumber>
            <v8:MeterNumber>{b[MeterNumber]}</v8:MeterNumber>
            <v8:Region>US</v8:Region>
         </v8:ClientDetail>
         <v8:TransactionDetail>
            <v8:CustomerTransactionId>ServiceAvailabilityRequest</v8:CustomerTransactionId>
         </v8:TransactionDetail>
         <v8:Version>
            <v8:ServiceId>vacs</v8:ServiceId>
            <v8:Major>8</v8:Major>
            <v8:Intermediate>0</v8:Intermediate>
            <v8:Minor>0</v8:Minor>
         </v8:Version>
         <v8:Origin>
            <v8:PostalCode>38017</v8:PostalCode>
            <v8:CountryCode>US</v8:CountryCode>
         </v8:Origin>
         <v8:Destination>
            <v8:PostalCode>03032</v8:PostalCode>
            <v8:CountryCode>US</v8:CountryCode>
         </v8:Destination>
         <v8:ShipDate>2019-01-01</v8:ShipDate>
         <v8:CarrierCode>FDXE</v8:CarrierCode>
      </v8:ServiceAvailabilityRequest>""".format(**vars())
  response = requests.post( "https://wsbeta.fedex.com:443/xml", data=data, headers=headers)    
  print (response.content )
  return HttpResponse(response)
    
'''==========================================================================
    Project Name: FedExApi Integration
    Function Name: Validate Address Codes Request
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def CountryService(request):
  headers = {'Content-Type': 'text/xml'}
  b = AccountDetails()
  data = """<?xml version="1.0" encoding="UTF-8"?>
    <v8:GetAvailablePostalsRequest xmlns:v8="http://fedex.com/ws/cnty/v8"
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:noNamespaceSchemaLocation='https://www.fedex.com/us/developer/downloads/xml/2018/standard/CountryService_v8.xsd'>
         <v8:WebAuthenticationDetail>
            <v8:UserCredential>
               <v8:Key>{b[Key]}</v8:Key>
               <v8:Password>{b[Password]}</v8:Password>
            </v8:UserCredential>
         </v8:WebAuthenticationDetail>         
         <v8:ClientDetail>
            <v8:AccountNumber>{b[AccountNumber]}</v8:AccountNumber>
            <v8:GroundShipperNumber>INPUT YOUR INFORMATION</v8:GroundShipperNumber>
            <v8:MeterNumber>{b[MeterNumber]}</v8:MeterNumber>
            <v8:MasterMeterNumber>INPUT YOUR INFORMATION</v8:MasterMeterNumber>
            <v8:MeterInstance>INPUT YOUR INFORMATION</v8:MeterInstance>
            <v8:CompanyId>INPUT YOUR INFORMATION</v8:CompanyId>
            <v8:SoftwareId>INPUT YOUR INFORMATION</v8:SoftwareId>
            <v8:SoftwareRelease>INPUT YOUR INFORMATION</v8:SoftwareRelease>
            <v8:ClientProductId>INPUT YOUR INFORMATION</v8:ClientProductId>
            <v8:ClientProductVersion>INPUT YOUR INFORMATION</v8:ClientProductVersion>
            <v8:MiddlewareProductId>INPUT YOUR INFORMATION</v8:MiddlewareProductId>
            <v8:MiddlewareProductVersion>INPUT YOUR INFORMATION</v8:MiddlewareProductVersion>
            <v8:IntegratorId>INPUT YOUR INFORMATION</v8:IntegratorId>
            <v8:Region>US</v8:Region>
            <v8:AutoConfigurationType>INPUT YOUR INFORMATION</v8:AutoConfigurationType>
            <v8:Localization>
               <v8:LanguageCode>EN</v8:LanguageCode>
               <v8:LocaleCode>en</v8:LocaleCode>
            </v8:Localization>
         </v8:ClientDetail>
         <v8:UserDetail>
            <v8:UserId>INPUT YOUR INFORMATION</v8:UserId>
            <v8:Password>INPUT YOUR INFORMATION</v8:Password>
            <v8:UniqueUserId>INPUT YOUR INFORMATION</v8:UniqueUserId>
         </v8:UserDetail>
         <v8:TransactionDetail>
            <v8:CustomerTransactionId>INPUT YOUR INFORMATION</v8:CustomerTransactionId>
            <v8:Localization>
               <v8:LanguageCode>EN</v8:LanguageCode>
               <v8:LocaleCode>en</v8:LocaleCode>
            </v8:Localization>
            <v8:InternalTransactionId>12dsd</v8:InternalTransactionId>
            <v8:Tracing>false</v8:Tracing>
            <v8:SourceFormat>INPUT YOUR INFORMATION</v8:SourceFormat>
            <v8:Environment>INPUT YOUR INFORMATION</v8:Environment>
         </v8:TransactionDetail>
         <v8:Version>
            <v8:ServiceId>cnty</v8:ServiceId>
            <v8:Major>8</v8:Major>
            <v8:Intermediate>0</v8:Intermediate>
            <v8:Minor>0</v8:Minor>
         </v8:Version>
         <v8:ShipDateTime>2016-12-12T16:16:44-06:00</v8:ShipDateTime>
         <v8:CarrierCodes>INPUT YOUR INFORMATION</v8:CarrierCodes>
         <v8:MatchAndResultCriteria>
            <v8:MatchAddress>
               <v8:PostalCode>INPUT YOUR INFORMATION</v8:PostalCode>
               <v8:CountryCode>INPUT YOUR INFORMATION</v8:CountryCode>
               <v8:Residential>INPUT YOUR INFORMATION</v8:Residential>
               <v8:GeographicCoordinates>cum murmure</v8:GeographicCoordinates>
            </v8:MatchAddress>
            <v8:MatchStoreNumber>15</v8:MatchStoreNumber>
            <v8:MatchLocationTypes>FEDEX_OFFICE</v8:MatchLocationTypes>
            <v8:MatchLocationId>10</v8:MatchLocationId>
            <v8:MatchConditions>MATCH_BY_EXACT_POSTAL_CODE</v8:MatchConditions>
            <v8:ResultsToSkip>0</v8:ResultsToSkip>
            <v8:ResultsRequested>15</v8:ResultsRequested>
            <v8:ResultConditions>INCLUDE_POSTAL_CODES</v8:ResultConditions>
         </v8:MatchAndResultCriteria>
      </v8:GetAvailablePostalsRequest>""".format(**vars())
  response = requests.post( "https://wsbeta.fedex.com:443/xml", data=data, headers=headers)    
#   print response.content 
  return HttpResponse(response)

'''==========================================================================
    Project Name: FedExApi Integration
    Function Name: Validate Address Codes Request
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def UploadDocumentService(request):
  headers = {'Content-Type': 'text/xml'}
  b = AccountDetails()
  data = """<?xml version="1.0" encoding="UTF-8"?>
    <v11:UploadImagesRequest xmlns:v11="http://fedex.com/ws/uploaddocument/v11"
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:noNamespaceSchemaLocation='https://www.fedex.com/us/developer/downloads/xml/2018/advanced/UploadDocumentService_v11.xsd'>
         <v11:WebAuthenticationDetail>
            <v11:UserCredential>
               <v11:Key>{b[Key]}</v11:Key>
               <v11:Password>{b[Password]}</v11:Password>
            </v11:UserCredential>
         </v11:WebAuthenticationDetail>
         <v11:ClientDetail>
            <v11:AccountNumber>{b[AccountNumber]}</v11:AccountNumber>
            <v11:MeterNumber>{b[MeterNumber]}</v11:MeterNumber>            
         </v11:ClientDetail>
         <v11:TransactionDetail>
            <v11:CustomerTransactionId>UploadImagesRequest_v11</v11:CustomerTransactionId>
         </v11:TransactionDetail>
         <v11:Version>
            <v11:ServiceId>cdus</v11:ServiceId>
            <v11:Major>11</v11:Major>
            <v11:Intermediate>0</v11:Intermediate>
            <v11:Minor>0</v11:Minor>
         </v11:Version>
         <v11:Images>
            <v11:Id>IMAGE_1</v11:Id>
            <v11:Image>R0lGODlhvAIyAPcAAJiYmP///2UBmMbGxoxAsrapvOPj49fX1613yO7q7/r6+/Pz8/n2+9e85PXv+ejZ8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwAAAAAvAIyAAAI/wADCBxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzatzIsaPHjyBDihxJsqTJkyhTqlzJsqXLlzBjypxJs6bNmzhz6tzJs6fPn0CDCh1KtKjRo0iTKl3KtKnTp1CjSp1KtarVq1izat3KtavXr2DDih1LtqzZs2jTql3Ltq3bt3Djyp1Lt67du3jz6t3Lt6/fv4ADCx5MuLDhw4gTK17MuLHjx5AjS55MubLly5gza97MubPnz6BDi67LwIHp06hTmxZZOjUDBbBjy56tYLTt24QF6N7Nu/duBiF9CwBAvLjx48Rr417OPK/w57+D+0ZO3bjy5tizv4UOHThI4dXDX//XTr58We7PvX8EH576ePPw429FL1y9R/btj7+Xz79/VPq+2dcRfvkVt59/CCZ4FIC9CcgRgQUCcKCCFFbY03MsQVichRx2WBSGK2lInIcklsgTiA4x8MCKDkB0GkEiAmDijDTOhKECDOSoo30PIEBAbwQg0IBCDfi4W5AJBBAjbbG9xmRCTE5Y45RTMqgbAQ8EwAAC6GFp0AM/PtdAjOENUN0BBhlQAHVoUunmm1bq9oADYdI35EBjxqlbhMYxsCZ1BhCUQHUDvGkolXo2UCeAWQbwgJ678bmhAn8iF2gACxB66KY06rnokQgYCWRtn1opaXIBDFrdApRSV4CUnMb/iiCkvt2pJZe9NZCnb0IWUGqkpyqnKnIFVGpcAQ7KquystO5m60CfEvDrswU8d6qEAxlw6gLLdkshigUJd9CvvBr067XjHcBnkt626x+4Az3arHDsErQrb9U9ZGZ+l7rrb3zwCnTvvFceJG9v+erbXpv/NlxewAEMTDABByUQY0TGGleowxxrB7HE81L85cUPqRtevx2nfBvEBx+p2svV+tZiQSAn3JC2+dWr8s6gQcxAqTMr1PJuCJhL8kHjDZsftwklcMABOvMsNWMQB4Arb0UTBGZvovZWAEFXIxzsQJnyiSxCBwzw9AAbT+12YlUPfWUDCSSgKK8gCxBk19ONjI1pxsQBHvh+CQxgANuHM/z24oNVbbWnDihALn3oBtAqsQqYjFzbAxme+AELcM746H45Lnmcd8rNYOX7Iseu5seJ7vkABRiggOik5+6cuAn9zOCzeR9JZr6tH6dz8RoT9LQBUB9wuO7Q6+X4QDFD5yXN5GI5PHXIF4eyQN0Tx/DtAyQQegFMR68+WAEBADs=</v11:Image>
         </v11:Images>
      </v11:UploadImagesRequest>""".format(**vars())
  response = requests.post( "https://wsbeta.fedex.com:443/xml", data=data, headers=headers)    
#   print response.content 
  return HttpResponse(response)

'''==========================================================================
    Project Name: FedExApi Integration
    Function Name: Validate Address Codes Request
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def PickupService(request):
  headers = {'Content-Type': 'text/xml'}
  b = AccountDetails()
   #print type(a.encode('UTF-8'))
  data = """<CreatePickupRequest xmlns="http://fedex.com/ws/pickup/v17" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation='https://www.fedex.com/us/developer/downloads/xml/2018/advanced/PickupService_v17.xsd'>
         <WebAuthenticationDetail>
            <UserCredential>
               <Key>{b[Key]}</Key>
               <Password>{b[Password]}</Password>
            </UserCredential>
         </WebAuthenticationDetail>
         <ClientDetail>
            <AccountNumber>{b[AccountNumber]}</AccountNumber>
            <MeterNumber>{b[MeterNumber]}</MeterNumber>
            <Localization>
               <LanguageCode>EN</LanguageCode>
               <LocaleCode>ES</LocaleCode>
            </Localization>          
         </ClientDetail>
         <TransactionDetail>
            <CustomerTransactionId>CreatePickupRequest</CustomerTransactionId>
            <Localization>
               <LanguageCode>EN</LanguageCode>
               <LocaleCode>ES</LocaleCode>
            </Localization>
         </TransactionDetail>
         <Version>
            <ServiceId>disp</ServiceId>
            <Major>17</Major>
            <Intermediate>0</Intermediate>
            <Minor>0</Minor>
         </Version>
         <AssociatedAccountNumber>
            <Type>FEDEX_FREIGHT</Type>
            <AccountNumber>{account_number}</AccountNumber>
         </AssociatedAccountNumber>
         <OriginDetail>
            <PickupLocation>
               <Contact>
                  <ContactId>5531</ContactId>
                  <PersonName>shivam</PersonName>
                  <Title>Mr.</Title>
                  <CompanyName>eKnous LLC</CompanyName>
                  <PhoneNumber>6097590669</PhoneNumber>
                  <PhoneExtension></PhoneExtension>
                  <PagerNumber>6097590669</PagerNumber>
                  <FaxNumber>6097590669</FaxNumber>
                  <EMailAddress>shivam@eknous.com</EMailAddress>
               </Contact>
               <Address>
                  <StreetLines>1202 Chalet Ln</StreetLines>
                  <StreetLines></StreetLines>
                  <StreetLines></StreetLines>
                  <City>Harrison</City>
                  <StateOrProvinceCode>AR</StateOrProvinceCode>
                  <PostalCode>72601</PostalCode>
                  <CountryCode>US</CountryCode>
               </Address>
            </PickupLocation>
            <PackageLocation>FRONT</PackageLocation>
            <BuildingPart>DEPARTMENT</BuildingPart>
            <BuildingPartDescription>BuildingPartDescription</BuildingPartDescription>
            <ReadyTimestamp>2018-12-27T12:34:56-06:00</ReadyTimestamp>
            <CompanyCloseTime>19:00:00</CompanyCloseTime>
            <Location>NQAA</Location>
            <SuppliesRequested>SuppliesRequested</SuppliesRequested>
         </OriginDetail>
         <PackageCount>1</PackageCount>
         <TotalWeight>
            <Units>LB</Units>
            <Value>50.0</Value>
         </TotalWeight>
         <CarrierCode>FDXE</CarrierCode>
         <OversizePackageCount>0</OversizePackageCount>
         <Remarks>Remarks</Remarks>
         <CommodityDescription>>TEST ENVIRONMENT - PLEASE DO NOT PROCESS PICKUP</CommodityDescription>
         <CountryRelationship>DOMESTIC</CountryRelationship>
      </CreatePickupRequest>""".format(**vars())
  response = requests.post( "https://wsbeta.fedex.com:443/xml", data=data, headers=headers)  
#   print response 
  return HttpResponse(response.content)
  
'''==========================================================================
    Project Name: FedExApi Integration
    Function Name: pickup request Codes Request
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

def CreatePickupReply(request):
   headers = {'Content-Type': 'text/xml'}
   b = AccountDetails()
   data = """<?xml version="1.0" encoding="UTF-8"?>
      <v17:CreatePickupRequest xmlns:v17="http://fedex.com/ws/pickup/v17" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation='https://www.fedex.com/us/developer/downloads/xml/2018/advanced/PickupService_v17.xsd'>
        <v17:WebAuthenticationDetail>
            <!--<v17:ParentCredential>
               <v17:Key>{b[Key]}</v17:Key>
               <v17:Password>{b[Password]}</v17:Password>
            </v17:ParentCredential>-->
            <v17:UserCredential>
               <v17:Key>ljhXbT89AEedhHW1</v17:Key>
               <v17:Password>htpN72nNmkbZXMPiROe7yO7nz</v17:Password>
            </v17:UserCredential>
         </v17:WebAuthenticationDetail>
         <v17:ClientDetail>
            <v17:AccountNumber>{b[AccountNumber]}</v17:AccountNumber>
            <v17:MeterNumber>{b[MeterNumber]}</v17:MeterNumber>
            <v17:IntegratorId>123</v17:IntegratorId>
         </v17:ClientDetail>
         <v17:TransactionDetail>
            <v17:CustomerTransactionId>v17 CreatePickup Freight_FFE</v17:CustomerTransactionId>
         </v17:TransactionDetail>
         <v17:Version>
            <v17:ServiceId>disp</v17:ServiceId>
            <v17:Major>17</v17:Major>
            <v17:Intermediate>0</v17:Intermediate>
            <v17:Minor>0</v17:Minor>
         </v17:Version>
         <v17:OriginDetail>
            <v17:UseAccountAddress>0</v17:UseAccountAddress>
            <v17:PickupLocation>
               <v17:Contact>
                  <v17:PersonName>shivam</v17:PersonName>
                  <v17:CompanyName>eKnous LLT</v17:CompanyName>
                  <v17:PhoneNumber>6762415000</v17:PhoneNumber>
                  <v17:EMailAddress>Anuj@eknous.com</v17:EMailAddress>
               </v17:Contact>
               <v17:Address>
                  <v17:StreetLines>1202 Chalet Ln</v17:StreetLines>
                  <v17:City>Harrison</v17:City>
                  <v17:StateOrProvinceCode>AR</v17:StateOrProvinceCode>
                  <v17:PostalCode>72601</v17:PostalCode>
                  <v17:CountryCode>US</v17:CountryCode>
               </v17:Address>
            </v17:PickupLocation>
            <v17:PackageLocation>FRONT</v17:PackageLocation>
            <v17:BuildingPart>APARTMENT</v17:BuildingPart>
            <v17:BuildingPartDescription>BuildingPartDescription</v17:BuildingPartDescription>
            <v17:ReadyTimestamp>${= String.format('%tF', new Date() )}T09:30:47-05:00</v17:ReadyTimestamp>
            <v17:CompanyCloseTime>17:00:00</v17:CompanyCloseTime>
         </v17:OriginDetail>
         <v17:FreightPickupDetail>
            <v17:ApprovedBy>
               <v17:PersonName>Anuj</v17:PersonName>
               <v17:CompanyName>eKnous</v17:CompanyName>
               <v17:PhoneNumber>6079708450</v17:PhoneNumber>
               <v17:EMailAddress>INPUT YOUR INFORMATION</v17:EMailAddress>
            </v17:ApprovedBy>
            <v17:Payment>SENDER</v17:Payment>
            <v17:Role>SHIPPER</v17:Role>
            <v17:SubmittedBy>
               <v17:PersonName>Anuj</v17:PersonName>
               <v17:CompanyName>eKnous</v17:CompanyName>
               <v17:PhoneNumber>6079708450</v17:PhoneNumber>
               <v17:EMailAddress>INPUT YOUR INFORMATION</v17:EMailAddress>
            </v17:SubmittedBy>
            <v17:LineItems>
               <v17:Service>FedEx_Freight</v17:Service>
               <v17:SequenceNumber>1</v17:SequenceNumber>
               <v17:Destination>
                  <v17:StreetLines>100, Overlook</v17:StreetLines>
                  <v17:City>Princeton</v17:City>
                  <v17:StateOrProvinceCode>NJ</v17:StateOrProvinceCode>
                  <v17:PostalCode>08540</v17:PostalCode>
                  <v17:CountryCode>US</v17:CountryCode>
               </v17:Destination>
               <v17:Packaging>INPUT YOUR INFORMATION</v17:Packaging>
               <v17:Pieces>INPUT YOUR INFORMATION</v17:Pieces>
               <v17:Weight>
                  <v17:Units>LB</v17:Units>
                  <v17:Value>145.0</v17:Value>
               </v17:Weight>
               <v17:TotalHandlingUnits>INPUT YOUR INFORMATION</v17:TotalHandlingUnits>
               <v17:JustOneMore>INPUT YOUR INFORMATION</v17:JustOneMore>
               <v17:Description>INPUT YOUR INFORMATION</v17:Description>
            </v17:LineItems>
         </v17:FreightPickupDetail>
         <v17:PackageCount>INPUT YOUR INFORMATION</v17:PackageCount>
         <v17:TotalWeight>
            <v17:Units>LB</v17:Units>
            <v17:Value>145.0</v17:Value>
         </v17:TotalWeight>
         <v17:CarrierCode>INPUT YOUR INFORMATION</v17:CarrierCode>
         <v17:OversizePackageCount>INPUT YOUR INFORMATION</v17:OversizePackageCount>
         <v17:Remarks>INPUT YOUR INFORMATION</v17:Remarks>
         <v17:CommodityDescription>INPUT YOUR INFORMATION</v17:CommodityDescription>
      </v17:CreatePickupRequest>"""
   response = requests.post( "https://wsbeta.fedex.com:443/xml", data=data, headers=headers)  
   # print response 
   return HttpResponse(response.content)


'''==========================================================================
    Project Name: FedExApi Integration
    Function Name: Pickup-Global request Codes 
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''
   
def PickupGlobal(request):
   headers = headers = {'Content-Type': 'text/xml'}
   b = AccountDetails()
   data = """<?xml version="1.0" encoding="UTF-8"?>
      <CreatePickupRequest xmlns="http://fedex.com/ws/pickup/v17" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation='https://www.fedex.com/us/developer/downloads/xml/2018/advanced/PickupService_v17.xsd'>
         <WebAuthenticationDetail>
            <ParentCredential>
               <Key>{b[Key]}</Key>
               <Password>{b[Password]}</Password>
            </ParentCredential>
            <UserCredential>
               <Key>ljhXbT89AEedhHW1</Key>
               <Password>htpN72nNmkbZXMPiROe7yO7nz</Password>
            </UserCredential>
         </WebAuthenticationDetail>
         <ClientDetail>
            <AccountNumber>{b[AccountNumber]}</AccountNumber>
            <MeterNumber>{b[MeterNumber]}</MeterNumber>
            <Localization>
               <LanguageCode>EN</LanguageCode>
               <LocaleCode>ES</LocaleCode>
            </Localization>
         </ClientDetail>
         <TransactionDetail>
            <CustomerTransactionId>123</CustomerTransactionId>
            <Localization>
               <LanguageCode>EN</LanguageCode>
               <LocaleCode>ES</LocaleCode>
            </Localization>
         </TransactionDetail>
         <Version>
            <ServiceId>disp</ServiceId>
            <Major>17</Major>
            <Intermediate>0</Intermediate>
            <Minor>0</Minor>
         </Version>
         <AssociatedAccountNumber>
            <Type>FEDEX_EXPRESS</Type>
            <AccountNumber>510088000</AccountNumber>
         </AssociatedAccountNumber>
         <OriginDetail>
            <PickupLocation>
               <Contact>
                  <ContactId>INPUT YOUR INFORMATION</ContactId>
                  <PersonName>INPUT YOUR INFORMATION</PersonName>
                  <Title>INPUT YOUR INFORMATION</Title>
                  <CompanyName>INPUT YOUR INFORMATION</CompanyName>
                  <PhoneNumber>INPUT YOUR INFORMATION</PhoneNumber>
                  <PhoneExtension>INPUT YOUR INFORMATION</PhoneExtension>
                  <PagerNumber>INPUT YOUR INFORMATION</PagerNumber>
                  <FaxNumber>INPUT YOUR INFORMATION</FaxNumber>
                  <EMailAddress>INPUT YOUR INFORMATION</EMailAddress>
               </Contact>
               <Address>
                  <StreetLines>INPUT YOUR INFORMATION</StreetLines>
                  <StreetLines>INPUT YOUR INFORMATION</StreetLines>
                  <StreetLines>INPUT YOUR INFORMATION</StreetLines>
                  <City>INPUT YOUR INFORMATION</City>
                  <StateOrProvinceCode>INPUT YOUR INFORMATION</StateOrProvinceCode>
                  <PostalCode>INPUT YOUR INFORMATION</PostalCode>
                  <CountryCode>INPUT YOUR INFORMATION</CountryCode>
               </Address>
            </PickupLocation>
            <PackageLocation>FRONT</PackageLocation>
            <BuildingPart>DEPARTMENT</BuildingPart>
            <BuildingPartDescription>BuildingPartDescription</BuildingPartDescription>
            <ReadyTimestamp>${=String.format('%tF', new Date())}T12:34:56-06:00</ReadyTimestamp>
            <CompanyCloseTime>19:00:00</CompanyCloseTime>
            <Location>NQAA</Location>
            <SuppliesRequested>SuppliesRequested</SuppliesRequested>
         </OriginDetail>
         <PackageCount>1</PackageCount>
         <TotalWeight>
            <Units>LB</Units>
            <Value>50.0</Value>
         </TotalWeight>
         <CarrierCode>INPUT YOUR INFORMATION</CarrierCode>
         <OversizePackageCount>0</OversizePackageCount>
         <Remarks>Remarks</Remarks>
         <CommodityDescription>INPUT YOUR INFORMATION</CommodityDescription>
         <CountryRelationship>DOMESTIC</CountryRelationship>
      </CreatePickupRequest>""" 
   response = requests.post( "https://wsbeta.fedex.com:443/xml", data=data, headers=headers)  
   # print response
   parsedXml = minidom.parseString(response.content)
   jsonData = json.dumps(parse_element(parsedXml), sort_keys=True, indent=4)
   feedResponse = json.loads(jsonData)
   # print feedResponse
   return HttpResponse(feedResponse)

'''==========================================================================
    Project Name: FedExApi Integration
    Function Name: Fedex-api request Codes 
    URL: https://eknous.com
    Author: Eknous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''  

def Fedex(request):
    label_link = ''
    expiration_date = datetime.datetime.now() + datetime.timedelta(days=10)

    formatted_date = "%s-%s-%s" % (expiration_date.year, expiration_date.month, expiration_date.day)
    formatted_date = datetime.datetime.now()

   #  if quote.device_type != 'laptop':
    box_length = 9
    box_width = 12
    box_height = 3
   #  else:
   #      box_length = 12
   #      box_width = 14
   #      box_height = 3 

    logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    ## Page 411 of FedEx Dev Guide - 20.14 Email Labels

    CONFIG_OBJ = FedexConfig(key="ljhXbT89AEedhHW1", password="htpN72nNmkbZXMPiROe7yO7nz", account_number="510088000",
                             meter_number="119096606", use_test_server="https://wsbeta.fedex.com:443/xml")

   #  fxreq = FedexCreatePendingShipRequestEmail(CONFIG_OBJ, customer_transaction_id='xxxxxx id:01')
    fxreq = FedexProcessShipmentRequest(CONFIG_OBJ, customer_transaction_id="*** ShipService Request v17 using Python ***")
    fxreq.RequestedShipment.ServiceType = 'FEDEX_GROUND'
    fxreq.RequestedShipment.PackagingType = 'YOUR_PACKAGING'
    fxreq.RequestedShipment.DropoffType = 'REGULAR_PICKUP'
    fxreq.RequestedShipment.ShipTimestamp = datetime.datetime.now()

    # Special fields for the email label
    fxreq.RequestedShipment.SpecialServicesRequested.SpecialServiceTypes = ('RETURN_SHIPMENT', 'PENDING_SHIPMENT')
    fxreq.RequestedShipment.SpecialServicesRequested.PendingShipmentDetail.Type = 'EMAIL'
    fxreq.RequestedShipment.SpecialServicesRequested.PendingShipmentDetail.ExpirationDate = formatted_date
    email_address = fxreq.create_wsdl_object_of_type('EMailRecipient')
    email_address.EmailAddress = "vasytavshivam@gmail.com"
    email_address.Role = 'SHIPMENT_COMPLETOR'

    # RETURN SHIPMENT DETAIL
    fxreq.RequestedShipment.SpecialServicesRequested.ReturnShipmentDetail.ReturnType = ('PENDING')
    fxreq.RequestedShipment.SpecialServicesRequested.ReturnShipmentDetail.ReturnEMailDetail = fxreq.create_wsdl_object_of_type(
        'ReturnEMailDetail')
    fxreq.RequestedShipment.SpecialServicesRequested.ReturnShipmentDetail.ReturnEMailDetail.MerchantPhoneNumber = 'x-xxx-xxx-xxxx'

    fxreq.RequestedShipment.SpecialServicesRequested.PendingShipmentDetail.EmailLabelDetail.Recipients = [email_address]
    fxreq.RequestedShipment.SpecialServicesRequested.PendingShipmentDetail.EmailLabelDetail.Message = "Xxxxxx Xxxxxx"

    fxreq.RequestedShipment.LabelSpecification = {'LabelFormatType': 'COMMON2D', 'ImageType': 'PDF'}

    fxreq.RequestedShipment.Shipper.Contact.PersonName = "shivam"
    fxreq.RequestedShipment.Shipper.Contact.CompanyName = 'eKnous'
    fxreq.RequestedShipment.Shipper.Contact.PhoneNumber = "9891266450"
   #  fxreq.RequestedShipment.Shipper.Address.StreetLines.append(quote.address)
    fxreq.RequestedShipment.Shipper.Address.StreetLines = ['100, overlook']
    fxreq.RequestedShipment.Shipper.Address.City = 'Princetone'
    fxreq.RequestedShipment.Shipper.Address.StateOrProvinceCode = 'NJ'
    fxreq.RequestedShipment.Shipper.Address.PostalCode ="08540"
    fxreq.RequestedShipment.Shipper.Address.CountryCode = 'US'

    fxreq.RequestedShipment.Recipient.Contact.PhoneNumber = '6097590669'
    fxreq.RequestedShipment.Recipient.Address.StreetLines = ['100, overlook']
    fxreq.RequestedShipment.Recipient.Address.City = 'Princetone'
    fxreq.RequestedShipment.Recipient.Address.StateOrProvinceCode = 'NJ'
    fxreq.RequestedShipment.Recipient.Address.PostalCode = '08540'
    fxreq.RequestedShipment.Recipient.Address.CountryCode = 'US'

    fxreq.RequestedShipment.Recipient.AccountNumber = '510088000'
    fxreq.RequestedShipment.Recipient.Contact.PersonName = ''
    fxreq.RequestedShipment.Recipient.Contact.CompanyName = 'Xxxxxx Xxxxxx'
    fxreq.RequestedShipment.Recipient.Contact.EMailAddress = 'xxxxxx@xxxxxxxxx'

    # Details of Person Who is Paying for the Shipping
    fxreq.RequestedShipment.ShippingChargesPayment.PaymentType = 'SENDER'
    fxreq.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.AccountNumber = '510088000'
    fxreq.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.Contact.PersonName = 'Xxxxx Xxxxx'
    fxreq.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.Contact.CompanyName = 'Xxxxx Xxxxxx'
    fxreq.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.Contact.PhoneNumber = 'x-xxx-xxx-xxxx'
    fxreq.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.Contact.EMailAddress = 'xxxxxxx@xxxxxxxxx'

    fxreq.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.Address.StreetLines = 'Xxxxx N. xXxxxxx'
    fxreq.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.Address.City = 'Xxxxxxx'
    fxreq.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.Address.StateOrProvinceCode = 'XX'
    fxreq.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.Address.PostalCode = 'xxxxx'
    fxreq.RequestedShipment.ShippingChargesPayment.Payor.ResponsibleParty.Address.CountryCode = 'US'

    # Package Info
    package1 = fxreq.create_wsdl_object_of_type('RequestedPackageLineItem')
    package1.SequenceNumber = '1'
    package1.Weight.Value = 1
    package1.Weight.Units = "LB"
    package1.Dimensions.Length = box_length
    package1.Dimensions.Width = box_width
    package1.Dimensions.Height = box_height
    package1.Dimensions.Units = "IN"
    package1.ItemDescription = 'Phone'
    fxreq.RequestedShipment.RequestedPackageLineItems.append(package1)
    fxreq.RequestedShipment.PackageCount = '1'

    try:
        fxreq.send_request()
        label_link = str(fxreq.response.CompletedShipmentDetail.CompletedPackageDetails[0].TrackingIds[0].TrackingNumber)
      #   label_link = str(fxreq.response.CompletedShipmentDetail.AccessDetail.AccessorDetails[0].EmailLabelUrl)
    except Exception as exc:
        print('Fedex Error')
        print('===========')
        print(exc)
        print('==========')

    return HttpResponse(label_link)