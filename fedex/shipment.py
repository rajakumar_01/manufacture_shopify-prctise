data  = 
"""<?xml version="1.0" encoding="UTF-8"?>
        <ProcessShipmentRequest xmlns="http//fedex.com/ws/ship/v23">
        <WebAuthenticationDetail>
            <UserCredential>
                <Key>ljhXbT89AEedhHW1</Key>
                <Password>htpN72nNmkbZXMPiROe7yO7nz</Password>
            </UserCredential>
        </WebAuthenticationDetail>
        <ClientDetail>
            <AccountNumber>510088000</AccountNumber>
            <MeterNumber>119096606</MeterNumber>
        </ClientDetail>
        <TransactionDetail>
            <CustomerTransactionId>ProcessShipmentRequest</CustomerTransactionId>
         </TransactionDetail>
          <Version>
            <ServiceId>ship</ServiceId>
            <Major>23</Major>
            <Intermediate>0</Intermediate>
            <Minor>0</Minor>
         <Version>
        <RequestedShipment>
         <ShipTimestamp>{{datetime.date.today}}</ShipTimestamp>

            <DropoffType>REGULAR_PICKUP</DropoffType>

            <ServiceType>FEDEX_GROUND</ServiceType>

            <PackagingType>YOUR_PACKAGING</PackagingType>

            <PreferredCurrency>USD</PreferredCurrency>

            <Shipper>

               <Contact>

                  <PersonName>Shivam</PersonName>

                  <CompanyName>eKnous</CompanyName>

                  <PhoneNumber>9891266450</PhoneNumber>

                  <EMailAddress>shivam@eknous.com</EMailAddress>

               </Contact>

               <Address>

                  <StreetLines>Sender_Address_Line1</StreetLines>

                  <StreetLines>Sender_Address_Line2</StreetLines>

                  <City>Herndon</City>

                  <StateOrProvinceCode>VA</StateOrProvinceCode>

                  <PostalCode>20171</PostalCode>

                  <CountryCode>US</CountryCode>

               </Address>

            </Shipper>

            <Recipient>

               <Contact>

                  <PersonName>Shivam</PersonName>

                  <CompanyName>eKnous</CompanyName>

                  <PhoneNumber>9891266450</PhoneNumber>

                  <EMailAddress>vastavshivam@gmail.com</EMailAddress>

               </Contact>

               <Address>

                  <StreetLines>Recipient_Address_Line1</StreetLines>

                  <StreetLines>Recipient_Address_Line2</StreetLines>

                  <City>Herndon</City>

                  <StateOrProvinceCode>VA</StateOrProvinceCode>

                  <PostalCode>20171</PostalCode>

                  <CountryCode>US</CountryCode>

               </Address>

            </Recipient>

            <ShippingChargesPayment>

               <PaymentType>SENDER</PaymentType>

               <Payor>

                  <ResponsibleParty>

                     <AccountNumber>XXXX</AccountNumber>

                     <Tins>

                        <TinType>BUSINESS_STATE</TinType>

                        <Number>XXXX</Number>

                     </Tins>

                     <Contact>

                        <ContactId>12345</ContactId>

                        <PersonName>XXXX</PersonName>

                     </Contact>

                  </ResponsibleParty>

               </Payor>

            </ShippingChargesPayment>

            <CustomsClearanceDetail>

               <DutiesPayment>

                  <PaymentType>SENDER</PaymentType>

                  <Payor>

                     <ResponsibleParty>

                        <AccountNumber>XXXXXX</AccountNumber>

                        <Tins>

                           <TinType>BUSINESS_STATE</TinType>

                           <Number>XXXX</Number>

                        </Tins>

                        <Contact>

                           <ContactId>12345</ContactId>

                           <PersonName>XXXX</PersonName>

                        </Contact>

                     </ResponsibleParty>

                  </Payor>

               </DutiesPayment>

               <DocumentContent>DOCUMENTS_ONLY</DocumentContent>

               <CustomsValue>

                  <Currency>USD</Currency>

                  <Amount>100.00</Amount>

               </CustomsValue>

               <CommercialInvoice>

                  <TermsOfSale>FOB</TermsOfSale>

               </CommercialInvoice>

               <Commodities>

                  <NumberOfPieces>1</NumberOfPieces>

                  <Description>ABCD</Description>

                  <CountryOfManufacture>US</CountryOfManufacture>

                  <Weight>

                     <Units>LB</Units>

                     <Value>1.0</Value>

                  </Weight>

                  <Quantity>1</Quantity>

                  <QuantityUnits>cm</QuantityUnits>

                  <UnitPrice>

                     <Currency>USD</Currency>

                     <Amount>1.000000</Amount>

                  </UnitPrice>

                  <CustomsValue>

                     <Currency>USD</Currency>

                     <Amount>100.000000</Amount>

                  </CustomsValue>

               </Commodities>

               <ExportDetail>

                  <ExportComplianceStatement>30.37(f)</ExportComplianceStatement>

               </ExportDetail>

            </CustomsClearanceDetail>

            <LabelSpecification>

               <LabelFormatType>COMMON2D</LabelFormatType>

               <ImageType>PNG</ImageType>

               <LabelStockType>PAPER_7X4.75</LabelStockType>

            </LabelSpecification>

            <RateRequestTypes>LIST</RateRequestTypes>

            <PackageCount>1</PackageCount>

            <RequestedPackageLineItems>

               <SequenceNumber>1</SequenceNumber>

               <Weight>

                  <Units>LB</Units>

                  <Value>20.0</Value>

               </Weight>

               <Dimensions>

                  <Length>12</Length>

                  <Width>12</Width>

                  <Height>12</Height>

                  <Units>IN</Units>

               </Dimensions>

               <CustomerReferences>

                  <CustomerReferenceType>CUSTOMER_REFERENCE</CustomerReferenceType>

                  <Value>string</Value>

               </CustomerReferences>

            </RequestedPackageLineItems>
        </RequestedShipment>       
        </ProcessShipmentRequest>"""