from django.conf.urls import url
from django.conf.urls.static import static
from . import views
# from fedex.views import *
 
urlpatterns = [ 
    # fedex Standard web services    
    url(r'^createf/$', views.ShipService, name="createf"),    
    url(r'^track/$', views.FdxTrackRequest, name="track"),
    url(r'^rate_request/$', views.FdxRate, name="rate_request"),
    url(r'^location_phone/$', views.LocationServicesByPhone, name="location_phone"),
    url(r'^location_zip/$', views.LocationServicesByPhone, name="location_zip"),
    url(r'^availability/$', views.ValidationAvailabilityAndCommitmentService, name="availability"),
    url(r'^country_service/$', views.CountryService, name="country_service"),

    # fedex Advanced web services 
    url(r'^doc_service/$', views.UploadDocumentService, name="doc_service"),
    url(r'^pickup_service/$', views.PickupService, name="pickup_service"),
    url(r'^create_pickup/$', views.CreatePickupReply, name="create_pickup"),
    url(r'^pickup_global/$', views.PickupGlobal, name="pickup_global"),
    url(r'^configration/$', views.FedexConfigrations, name="configration"),
    url(r'^Fedex/$', views.Fedex, name="Fedex"),





    
]