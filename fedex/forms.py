from django import forms
from django.forms import MultiWidget
from .models import *

class FedexConfigrationsForms(forms.ModelForm):

    class Meta:
        model = FedexConfigration
        fields = ["key","password" ,"account_number", "meter_number"]
