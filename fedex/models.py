# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class FedexConfigration(models.Model):
    key = models.CharField(max_length=500, 
                                    blank=True, 
                                    null=True, 
                                    verbose_name='Development Key',
                                    unique=True)
    password = models.CharField(max_length=12, 
                                    blank=True, 
                                    null=True,                                    
                                     )
    account_number = models.CharField(max_length=12, 
                                    blank=True, 
                                    null=True,                                    
                                    unique=True )
    meter_number = models.CharField(max_length=12, 
                                    blank=True, 
                                    null=True, 
                                    unique=True )

    def __unicode__(self):
        return (self.account_number)



class ShipperContactInfo(models.Model):
    shipper_account_number = models.ForeignKey(FedexConfigration, related_name='shipper_account_number',on_delete=models.CASCADE)
    person_name = models.CharField(max_length=50, 
                                    blank=True, 
                                    null=True, )
    company_name = models.CharField(max_length=50, 
                                    blank=True, 
                                    null=True, )
    phone_number = models.CharField(max_length=15, 
                                    blank=True, 
                                    null=True, )
    def __unicode__(self):
        return (self.person_name)


class ShipperAddress(models.Model):
    shipper_person_name = models.ForeignKey(ShipperContactInfo, related_name='shipper_person_name',on_delete=models.CASCADE)
    street_line = models.CharField(max_length=50, 
                                    blank=True, 
                                    null=True, )
    city =  models.CharField(max_length=50, 
                                    blank=True, 
                                    null=True, )  
    State_or_province_code = models.CharField(max_length=5, 
                                    blank=True, 
                                    null=True, )
    CountryCode = models.CharField(max_length=5, 
                                    blank=True, 
                                    null=True, ) 
    def __unicode__(self):
        return (self.street_line)                 

class FedExServiceType(models.Model):
    fedex_service_name = models.CharField(max_length=50, 
                                    blank=True, 
                                    null=True, )
    def __unicode__(self):
        return (self.fedex_service_name)

class FedExServices(models.Model):
    service_type =models.ForeignKey(FedExServiceType, related_name='fedex_services_type',on_delete=models.CASCADE) 
    service_name =models.CharField(max_length=50, 
                                    blank=True, 
                                    null=True, )
    service_code = models.CharField(max_length=50, 
                                    blank=True, 
                                    null=True, )
    def __unicode__(self):
        return (self.service_name)

                                

