from __future__ import unicode_literals
from django.shortcuts import render, get_object_or_404, render_to_response, redirect
# from django.shortcuts import render, redirect
from django.http import HttpResponse
from django import forms
import django_excel as excel
from openpyxl import *
from wallmart.models import *
from ordermanage.models import *
import datetime
from django.utils import timezone
import xlsxwriter
import math
# from io import BytesIO
#######################################################
########## Ankit's Code  Start#########################
#######################################################
class UploadFileForm(forms.Form):
    file = forms.FileField() 

def exceltodatabase(request):
    #obj1 = Wallmart.objects.all()
    obj1 = Wallmart
    if request.method == "POST":
        form = UploadFileForm(request.POST,
                              request.FILES)
        if form.is_valid():
            # filename1 = 'WAL10.xlsx'
            file_in_memory = request.FILES['file']
            wb = load_workbook(file_in_memory)
            # wb = load_workbook(filename1)
            sheet = wb.active
            # obj = Wallmart

            c = 1   # Column no. for sheet 1
            d = 0
            e = 0
            f = 0
            # Shee2 Colum Assignment-----------------

            for row in sheet.iter_rows():  # Pyxl function to iterate on all rows of file
            # Shee2 Colum Assignment-----------------
                #----------Assign Index position -----------------
                # Based on the pattern of PDF extracted worksheet.Assign value
                Afld = 'A' + str(c)  # To check first field
                Bfld = 'C' + str(c)  # To check the value of second field
                Cfld = 'F' + str(c)  # Tocheck first field
                Dfld = 'P' + str(c)  # To check the value of second field
                Efld = 'Y' + str(c)  # To check the value of second field
                Ifld = 'E' + str(c)
                if sheet[Afld].value == 'Purchase Order Number':
                    if e == f:
                        if sheet[Cfld].value != None:
                            abc = sheet[Cfld].value
                            obj1(Purchase_Order_Number=abc) # Assign value. to next sheet

                        else:
                            abc = sheet[Dfld].value  # Assign value to next sheet
                            obj1(Purchase_Order_Number=abc)
                        e += 1
                    else:
                        pass

                elif sheet[Afld].value == 'Purchase Order Date':
                    if sheet[Cfld].value != None:
                        abc1 = sheet[Cfld].value
                        obj1(Purchase_Order_Date=abc1)
                    else:
                        abc1 = sheet[Dfld].value
                        obj1(Purchase_Order_Date=abc1)

                elif sheet[Afld].value == 'Ship Not Before':
                    if sheet[Cfld].value != None:
                        abc2 = sheet[Cfld].value
                        obj1(Ship_Not_Before=abc2)
                    else:
                        abc2 = sheet[Dfld].value
                        obj1(Ship_Not_Before=abc2)

                elif sheet[Afld].value == 'Ship No Later Than':
                    if sheet[Cfld].value != None:
                        abc3 = sheet[Cfld].value
                        obj1(Ship_No_Later_Than=abc3)
                    else:
                        abc3 = sheet[Dfld].value
                        obj1(Ship_No_Later_Than=abc3)

                elif sheet[Afld].value == 'Must Arrive By':

                    if sheet[Cfld].value != None:
                        abc4 = sheet[Cfld].value
                        obj1(Must_Arrive_By=abc4)
                    else:
                        abc4 = sheet[Dfld].value
                        obj1(Must_Arrive_By=abc4)


                elif sheet[Afld].value == 'Order Type':

                    if sheet[Cfld].value != None:
                        abc5 = sheet[Cfld].value
                        obj1(Order_Type=abc5)

                    else:

                        abc5 = sheet[Dfld].value
                        obj1(Order_Type=abc5)

                elif sheet[Afld].value == 'Currency':

                    if sheet[Cfld].value != None:
                        abc6 = sheet[Cfld].value
                        obj1(Currency=abc6)

                    else:

                        abc6 = sheet[Dfld].value
                        obj1(Currency=abc6)

                elif sheet[Afld].value == 'Department':

                    if sheet[Cfld].value != None:
                        abc7 = sheet[Cfld].value
                        obj1(Department=abc7)


                    else:

                        abc7 = sheet[Dfld].value
                        obj1(Department=abc7)


                elif sheet[Afld].value == 'Promotional Event':

                    if sheet[Cfld].value != None:
                        abc8 = sheet[Cfld].value
                        obj1(Promotional_Event=abc8)


                    else:

                        abc8 = sheet[Dfld].value
                        obj1(Promotional_Event=abc8)


                elif sheet[Afld].value == 'Payment Terms':

                    if sheet[Cfld].value != None:
                        abc9 = sheet[Cfld].value
                        obj1(Payment_Terms=abc9)


                    else:

                        abc9 = sheet[Dfld].value
                        obj1(Payment_Terms=abc9)


                elif sheet[Afld].value == 'F.O.B.':

                    if sheet[Cfld].value != None:
                        abc10 = sheet[Cfld].value
                        obj1(FOB=abc10)


                    else:

                        abc10 = sheet[Dfld].value
                        obj1(FOB=abc10)


                elif sheet[Afld].value == 'F.O.B. Ship Point':

                    if sheet[Cfld].value != None:
                        abc11 = sheet[Cfld].value
                        obj1(FOB_Ship_Point=abc11)


                    else:

                        abc11 = sheet[Dfld].value
                        obj1(FOB_Ship_Point=abc11)


                elif sheet[Afld].value == 'Ship To':

                    d = c+1
                    Ffld = 'A' + str(d)
                    abc12 = sheet[Ffld].value
                    obj1(Ship_Address_1=abc12)

                    d = d+1
                    Ffld = 'A' + str(d)
                    abc13 = sheet[Ffld].value
                    obj1(Ship_Address_2=abc13)

                    d = d+1
                    Ffld = 'A' + str(d)
                    abc14 = sheet[Ffld].value
                    obj1(Ship_Address_3=abc14)

                    d = 0

                elif sheet[Afld].value == 'Bill To':

                    d = c+1
                    Ffld = 'A' + str(d)
                    abc15 = sheet[Ffld].value
                    obj1(Bill_Address_1=abc15)

                    d = d+1
                    Ffld = 'A' + str(d)
                    abc16 = sheet[Ffld].value
                    obj1(Bill_Address_2=abc16)

                    d = d+1
                    Ffld = 'A' + str(d)
                    abc17 = sheet[Ffld].value
                    obj1(Bill_Address_3=abc17)

                    d = 0


                elif sheet[Afld].value == 'Carrier':

                    if sheet[Cfld].value != None:
                        abc18 = sheet[Cfld].value
                        obj1(Carrier=abc18)


                    else:

                        abc18 = sheet[Dfld].value
                        obj1(Carrier=abc18)


                elif sheet[Afld].value == 'Line':
                    d = c+1
                    Ffld = 'A' + str(d)
                    abc19 = sheet[Ffld].value
                    obj1(Line=abc19)

                    Ffld = 'B' + str(d)
                    abc20 = sheet[Ffld].value
                    obj1(Item=abc20)

                    Ffld = 'D' + str(d)
                    abc21 = sheet[Ffld].value
                    obj1(Gtin = abc21)

                    Ffld = 'G' + str(d)
                    abc22 = sheet[Ffld].value
                    obj1(Supplier_Stock=abc22)

                    Ffld = 'I' + str(d)
                    abc23 = sheet[Ffld].value
                    obj1(Color=abc23)

                    Ffld = 'L' + str(d)
                    abc24 = sheet[Ffld].value
                    obj1(Size=abc24)

                    Ffld = 'M' + str(d)
                    abc25 = sheet[Ffld].value
                    obj1(Quantity_Ordered=abc25)

                    Ffld = 'P' + str(d)
                    abc26 = sheet[Ffld].value
                    obj1(UOM=abc26)

                    Ffld = 'R' + str(d)
                    abc27= sheet[Ffld].value
                    obj1(Pack=abc27)

                    Ffld= 'S' + str(d)
                    abc28= sheet[Ffld].value
                    obj1(Cost=abc28)

                    Ffld= 'U' + str(d)
                    abc29= sheet[Ffld].value
                    obj1(Extended_Cost=abc29)

                    d=0


                elif sheet[Afld].value == 'Truck-Vendor Pool':

                    if sheet[Cfld].value != None:
                        abc30 = sheet[Cfld].value
                        obj1(Truck_Vendor_Pool=abc30)



                    else:

                        abc30 = sheet[Dfld].value
                        obj1(Truck_Vendor_Pool=abc30)


                elif sheet[Afld].value == 'Supplier Name':

                    if sheet[Ifld].value != None:
                        abc31 = sheet[Ifld].value
                        obj1(Supplier_Name=abc31)


                    else:
                        abc31 = sheet[Cfld].value
                        obj1(Supplier_Name=abc31)


                elif sheet[Afld].value == 'Supplier Number':

                    if sheet[Ifld].value != None:
                        abc32 = sheet[Ifld].value
                        obj1(Supplier_Number=abc32)


                    else:
                        abc32 = sheet[Cfld].value
                        obj1(Supplier_Number=abc32)


                elif sheet[Afld].value == 'Total Order Amount (Before Adjustments)':
                    Ffld= 'U' + str(c)
                    abc33 = sheet[Ffld].value
                    obj1(Total_Order_Amount=abc33)


                elif sheet[Afld].value == 'Total Line Items':

                    if sheet[Cfld].value != None:
                        abc34 = sheet[Cfld].value
                        obj1(Total_Line_Items=abc34)


                    else:
                        abc34 = sheet[Dfld].value
                        obj1(Total_Line_Items=abc34)


                elif sheet[Afld].value == 'Allowance':
                    Ffld = 'U' + str(c)
                    abc35 = sheet[Ffld].value
                    obj1(Allowance=abc35)
                    d= c+1
                    Ffld = 'U' + str(d)
                    abc035 = sheet[Ffld].value
                    obj1(Allowance2=abc035)


                elif sheet[Afld].value == 'Total Units Ordered':

                    if sheet[Cfld].value != None:
                        abc36 = sheet[Cfld].value
                        obj1(Total_Units_Ordered=abc36)
                        # obj1(abc, abc1, abc2, abc3, abc4, abc5, abc6, abc7, abc8, abc9, abc10, abc11, abc12, abc13, abc14, abc15, abc16, abc17, abc18, abc19, abc20, abc21, abc22, abc23, abc24, abc25, abc26, abc27, abc28, abc29, abc30, abc31, abc32, abc33, abc34, abc35, abc36).save()
                        obj1(Purchase_Order_Number=abc, Purchase_Order_Date=abc1, Ship_Not_Before=abc2, Ship_No_Later_Than=abc3, Must_Arrive_By=abc4, Order_Type=abc5, Currency=abc6, Department=abc7, Promotional_Event=abc8, Payment_Terms=abc9, FOB=abc10, FOB_Ship_Point=abc11, Ship_Address_1=abc12, Ship_Address_2=abc13, Ship_Address_3=abc14, Bill_Address_1=abc15, Bill_Address_2=abc16, Bill_Address_3=abc17, Carrier=abc18, Line=abc19,
                        Item=abc20, Gtin=abc21, Supplier_Stock=abc22, Color=abc23, Size=abc24, Quantity_Ordered=abc25, UOM=abc26, Pack=abc27, Cost=abc28, Extended_Cost=abc29, Truck_Vendor_Pool=abc30, Supplier_Name=abc31, Supplier_Number=abc32, Total_Order_Amount=abc33, Total_Line_Items=abc34, Allowance=abc35, Allowance2=abc035, Total_Units_Ordered=abc36).save()

                    else:
                        abc36 = sheet[Dfld].value
                        obj1(Total_Units_Ordered=abc36)
                        # obj1(abc, abc1, abc2, abc3, abc4, abc5, abc6, abc7, abc8, abc9, abc10, abc11, abc12, abc13, abc14, abc15, abc16, abc17, abc18, abc19, abc20, abc21, abc22, abc23, abc24, abc25, abc26, abc27, abc28, abc29, abc30, abc31, abc32, abc33, abc34, abc35, abc36).save()
                        obj1(Purchase_Order_Number=abc, Purchase_Order_Date=abc1, Ship_Not_Before=abc2, Ship_No_Later_Than=abc3, Must_Arrive_By=abc4, Order_Type=abc5, Currency=abc6, Department=abc7, Promotional_Event=abc8, Payment_Terms=abc9, FOB=abc10, FOB_Ship_Point=abc11, Ship_Address_1=abc12, Ship_Address_2=abc13, Ship_Address_3=abc14, Bill_Address_1=abc15, Bill_Address_2=abc16, Bill_Address_3=abc17, Carrier=abc18, Line=abc19,
                        Item=abc20, Gtin=abc21, Supplier_Stock=abc22, Color=abc23, Size=abc24, Quantity_Ordered=abc25, UOM=abc26, Pack=abc27, Cost=abc28, Extended_Cost=abc29, Truck_Vendor_Pool=abc30, Supplier_Name=abc31, Supplier_Number=abc32, Total_Order_Amount=abc33, Total_Line_Items=abc34, Allowance=abc35, Allowance2=abc035, Total_Units_Ordered=abc36).save()# .save()
                    f +=1
                c += 1  # Increase row counter for first sheet.
            wb.close  # Close the file

            return HttpResponse("Done")
        else:
            return HttpResponse("Unique Identification Error")
    else:
        form = UploadFileForm()
    return render(
        request,
        'upload_form.html',
        {'form': form})

def db2xlsx(request):
    obj1 = Wallmart.objects.filter(Purchase_Order_Date=datetime.date.today())
    form = UploadFileForm(request.POST,
                          request.FILES)
    wb = Workbook()
    ws = wb.active
    b = 2
    qw =1
    for obj in obj1:
        a = obj.Purchase_Order_Number
        b1 = obj.Purchase_Order_Date
        c = obj.Ship_Not_Before
        d = obj.Must_Arrive_By
        e = obj.Ship_No_Later_Than
        f = obj.Order_Type
        g = obj.Currency
        h = obj.Department
        i = obj.Promotional_Event
        j = obj.FOB
        k = obj.Payment_Terms
        l = obj.FOB_Ship_Point
        m = obj.Carrier
        n = obj.Truck_Vendor_Pool
        o = obj.Supplier_Name
        p = obj.Supplier_Number
        q = obj.Total_Line_Items
        r = obj.Total_Units_Ordered
        s = obj.Allowance
        s2 = obj.Allowance2
        t = obj.Total_Order_Amount
        u = obj.Ship_Address_1
        v = obj.Ship_Address_2
        w = obj.Ship_Address_3
        x = obj.Bill_Address_1
        y = obj.Bill_Address_2
        z = obj.Bill_Address_3
        aa = obj.Line
        ab = obj.Item
        ac = obj.Gtin
        ad = obj.Supplier_Stock
        ae = obj.Color
        af = obj.Size
        ag = obj.Quantity_Ordered
        ah = obj.UOM
        ai = obj.Pack
        aj = obj.Cost
        ak = obj.Extended_Cost
        col1 = 'A' + str(qw)  # Col1 till 70 in new file for output
        col2 = 'A' + str(b)
        col3 = 'B' + str(qw)
        col4 = 'B' + str(b)
        col5 = 'C' + str(qw)
        col6 = 'C' + str(b)
        col7 = 'D' + str(qw)
        col8 = 'D' + str(b)
        col9 = 'E' + str(qw)
        col10 = 'E' + str(b)
        col11 = 'F' + str(qw)
        col12 = 'F' + str(b)
        col13 = 'G' + str(qw)
        col14 = 'G' + str(b)
        col15 = 'H' + str(qw)
        col16 = 'H' + str(b)
        col17 = 'I' + str(qw)
        col18 = 'I' + str(b)
        col19 = 'J' + str(qw)
        col20 = 'J' + str(b)
        col21 = 'K' + str(qw)
        col22 = 'K' + str(b)
        col23 = 'L' + str(qw)
        col24 = 'L' + str(b)
        col25 = 'M' + str(qw)
        col26 = 'M' + str(b)
        col27 = 'N' + str(qw)
        col28 = 'N' + str(b)
        col29 = 'O' + str(qw)
        col30 = 'O' + str(b)
        col31 = 'P' + str(qw)
        col32 = 'P' + str(b)
        col33 = 'Q' + str(qw)
        col34 = 'Q' + str(b)
        col35 = 'R' + str(qw)
        col36 = 'R' + str(b)
        col37 = 'S' + str(qw)
        col38 = 'S' + str(b)
        col39 = 'T' + str(qw)
        col40 = 'T' + str(b)

        col41 = 'U' + str(qw)
        col42 = 'U' + str(b)
        col43 = 'V' + str(b)
        col44 = 'W' + str(b)

        col45 = 'X' + str(qw)
        col46 = 'X' + str(b)
        col47 = 'Y' + str(b)
        col48 = 'Z' + str(b)
        col49 = 'AA' + str(qw)
        col50 = 'AA' + str(b)
        col51 = 'AB' + str(qw)
        col52 = 'AB' + str(b)
        col53 = 'AC' + str(qw)
        col54 = 'AC' + str(b)
        col55 = 'AD' + str(qw)
        col56 = 'AD' + str(b)
        col57 = 'AE' + str(qw)
        col58 = 'AE' + str(b)
        col59 = 'AF' + str(qw)
        col60 = 'AF' + str(b)
        col61 = 'AG' + str(qw)
        col62 = 'AG' + str(b)
        col63 = 'AH' + str(qw)
        col64 = 'AH' + str(b)
        col65 = 'AI' + str(qw)
        col66 = 'AI' + str(b)
        col67 = 'AJ' + str(qw)
        col68 = 'AJ' + str(b)
        col69 = 'AK' + str(qw)
        col70 = 'AK' + str(b)
        col71 = 'AL' + str(qw)
        col72 = 'AL' + str(b)

        ws[col1] = "Purchase Order Number"
        ws[col2] = a
        ws[col3] = "Purchase Order Date"
        ws[col4] = b1
        ws[col5] = "Ship Not Before"
        ws[col6] = c
        ws[col7] = "Must Arrive By"
        ws[col8] = d
        ws[col9] = "Ship No Later Than"
        ws[col10] = e
        ws[col11] = "Order Type"
        ws[col12] = f
        ws[col13] = "Currency"
        ws[col14] = g
        ws[col15] = "Department"
        ws[col16] = h
        ws[col17] = "Promotional Event"
        ws[col18] = i
        ws[col19] = "FOB"
        ws[col20] = j
        ws[col21] = "Payment Terms"
        ws[col22] = k
        ws[col23] = "FOB Ship Point"
        ws[col24] = l
        ws[col25] = "Carrier"
        ws[col26] = m
        ws[col27] = "Truck Vendor Pool"
        ws[col28] = n
        ws[col29] = "Supplier Name"
        ws[col30] = o
        ws[col31] = "Supplier Number"
        ws[col32] = p
        ws[col33] = "Total Line Items"
        ws[col34] = q
        ws[col35] = "Total Units Ordered"
        ws[col36] = r
        ws[col37] = "Allowance"
        ws[col38] = s
        ws[col39] = "Allowance"
        ws[col40] = s2
        ws[col41] = "Total Order Amount"
        ws[col42] = t
        ws[col43] = "Ship To"
        ws[col44] = u
        ws[col45] = v
        ws[col46] = w
        ws[col47] = "Bill To"
        ws[col48] = x
        ws[col49] = y
        ws[col50] = z
        ws[col51] = "Line"
        ws[col52] = aa
        ws[col53] = "Item"
        ws[col54] = ab
        ws[col55] = "Gtin"
        ws[col56] = ac
        ws[col57] = "Supplier Stock"
        ws[col58] = ad
        ws[col59] = "Color"
        ws[col60] = ae
        ws[col61] = "Size"
        ws[col62] = af
        ws[col63] = "Quantity Ordered"
        ws[col64] = ag
        ws[col65] = "UOM"
        ws[col66] = ah
        ws[col67] = "Pack"
        ws[col68] = ai
        ws[col69] = "Cost"
        ws[col70] = aj
        ws[col71] = "Extended Cost"
        ws[col72] = ak
        b += 1
    wb.save("sample.xlsx")
    wb.close
    # print "Done"
    # print a

    return render(
        request,
        'upload_form.html',
        {'form': form})

#################################################
############routing list#########################
#################################################

def routing(request):
    obj1 = ROUT
    if request.method == "POST":
        form = UploadFileForm(request.POST,
                              request.FILES)
        if form.is_valid():

            file_in_memory = request.FILES['file']
            wb = load_workbook(file_in_memory)

            sheet = wb.active

            c=2
            for row in sheet.iter_rows():
                Afld = 'A' + str(c)
                Bfld = 'B' + str(c)
                Cfld = 'C' + str(c)
                Dfld = 'D' + str(c)
                Efld = 'E' + str(c)
                Ffld = 'F' + str(c)
                Gfld = 'G' + str(c)
                Hfld = 'H' + str(c)
                Ifld = 'I' + str(c)
                Jfld = 'J' + str(c)
                Kfld = 'K' + str(c)
                Lfld = 'L' + str(c)
                Mfld = 'M' + str(c)
                Nfld = 'N' + str(c)
                Ofld = 'O' + str(c)
                Pfld = 'P' + str(c)



                a1 = sheet[Afld].value
                obj1(STATUS=a1)
                a2 = sheet[Bfld].value
                obj1(Ship_On_Date=a2)
                a3 = sheet[Cfld].value
                obj1(Carrier_PU_Date=a3)
                a4 = sheet[Dfld].value
                obj1(Carrier_Due_Date=a4)
                a5 = sheet[Efld].value
                obj1(Carrier_Name=a5)
                a6 = sheet[Ffld].value
                obj1(Mode=a6)
                a7 = sheet[Gfld].value
                obj1(Load_Dest=a7)
                a8 = sheet[Hfld].value
                obj1(Shippoint=a8)
                a9 = sheet[Ifld].value
                obj1(Load=a9)
                a10 = sheet[Jfld].value
                obj1(PO=a10)
                a11 = sheet[Kfld].value
                obj1(Cases=a11)
                a12 = sheet[Lfld].value
                obj1(Weight=a12)
                a13 = sheet[Mfld].value
                obj1(Pallets=a13)
                a14 = sheet[Nfld].value
                obj1(Cube=a14)
                a15 = sheet[Ofld].value
                obj1(PO_Type=a15)
                a16 = sheet[Pfld].value
                obj1(Dept=a16)
                obj1(STATUS=a1, Ship_On_Date=a2, Carrier_PU_Date=a3, Carrier_Due_Date=a4, Carrier_Name=a5, Mode=a6, Load_Dest=a7, Shippoint=a8, Load=a9, PO=a10, Cases=a11, Weight=a12, Pallets=a13, Cube=a14, PO_Type=a15, Dept=a16).save()
                c +=1
            wb.close
            return HttpResponse("Done")
        else:
            return HttpResponse("Unique Identification Error")
    else:
        form = UploadFileForm()
    return render(
        request,
        'upload_form.html',
        {'form': form})

############################################
#########- Address Filter- #################
############################################

def address_file(request):
    obj2 = ADDRESS
    if request.method == "POST":
        form = UploadFileForm(request.POST,
                              request.FILES)
        if form.is_valid():
            file_in_memory = request.FILES['file']
            wb = load_workbook(file_in_memory)
            sheet = wb.active
            c=2
            for row in sheet.iter_rows():
                Afld = 'A' + str(c)
                Bfld = 'B' + str(c)
                Cfld = 'C' + str(c)
                Dfld = 'D' + str(c)
                Efld = 'E' + str(c)
                Ffld = 'F' + str(c)
                Gfld = 'G' + str(c)
                g1 = sheet[Afld].value
                obj2(Store_Nbr=g1)
                g2 = sheet[Bfld].value
                obj2(Address=g2)
                g3 = sheet[Cfld].value
                obj2(City=g3)
                g4 = sheet[Dfld].value
                obj2(State_Or_Prov=g4)
                g5 = sheet[Efld].value
                obj2(Postal_Code=g5)
                g6 = sheet[Ffld].value
                obj2(Phone_Number=g6)
                g7 = sheet[Gfld].value
                obj2(Manager_Name=g7)
                obj2(Store_Nbr=g1, Address=g2, City=g3, State_Or_Prov=g4, Postal_Code=g5, Phone_Number=g6, Manager_Name=g7).save()
                c +=1
            wb.close
            return HttpResponse("Done")
        else:
            return HttpResponse("Unique Identification Error")
    else:
        form = UploadFileForm()
    return render(
        request,
        'upload_form.html',
        {'form': form})
def list(request):
    obj3 = LISTING
    if request.method == "POST":
        form = UploadFileForm(request.POST,
                              request.FILES)
        if form.is_valid():
            file_in_memory = request.FILES['file']
            wb = load_workbook(file_in_memory)
            sheet = wb.active
            c=2
            for row in sheet.iter_rows(row_offset=2):
                Afld = 'A' + str(c)
                Bfld = 'B' + str(c)
                Cfld = 'C' + str(c)
                Dfld = 'D' + str(c)
                Efld = 'E' + str(c)
                g1 = sheet[Afld].value
                obj3(Document_Number=g1)
                g2 = sheet[Bfld].value
                obj3(Document_Type=g2)
                g3 = sheet[Cfld].value
                obj3(Received_Date=g3)
                g4 = sheet[Dfld].value
                obj3(Vendor_Number=g4)
                g5 = sheet[Efld].value
                obj3(Location=g5)
                obj3(Document_Number=g1, Document_Type=g2, Received_Date=g3, Vendor_Number=g4, Location=g5).save()
                c +=1
            wb.close
            return HttpResponse("Done")
        else:
            return HttpResponse("Unique Identification Error")
    else:
        form = UploadFileForm()
    return render(
        request,
        'upload_form.html',
        {'form': form})
#######################################################
########## Ankit's Code  END#########################
#######################################################

#######################################################
########## written by shivam ##########################
################### po uplode file ####################
############## case, weight and quentity ##############
#######################################################

def UploadFile(request):
    obj1 = Wallmart.objects.all()#replase it Wallmart.objects.filter(Purchase_Order_Date=datetime.date.today())
    length = len(obj1)
    wb = Workbook()
    sheet= wb.active #puting a value on single sheet

    col=1           #for index value
    row =2
    #asssigning values to column
    col1 = 'A' + str(1)
    col2 = 'B' + str(col)
    col3 = 'C' + str(col)
    col4 = 'D' + str(col)
    col5 = 'E' + str(col)
    # loop for row itteration
    for obj in obj1:
        quantity =obj.Total_Units_Ordered
        case = quantity/3
        weight = case * 14

        rowFld1='A'+ str(row)
        rowFld2='B'+ str(row)
        rowFld3='C'+ str(row)
        rowFld4='D'+ str(row)
        rowFld5='E'+ str(row)

        sheet[col1]= "SHIPPOINT"
        sheet[rowFld1]= "61502601"
        sheet[col2]= "LOADINGMETHOD"
        sheet[rowFld2]= "Floor"
        sheet[col3]= "CASES"
        sheet[rowFld3]= case
        sheet[col4]= "WEIGHT"
        sheet[rowFld4]= weight
        sheet[col5]= "QUANTITY"
        sheet[rowFld5]= quantity
        row += 1

    wb.save("sample4.xlsx")
    wb.close

    return render(request,'display.html', {})
######################################################
###############uplode routing file ###################
######################################################

class UploadRoutingFileForm(forms.Form):
    file = forms.FileField()

def RoutingFile(request):
    routobject = ROUT
    
   
    # order_query_set =RoutingListing
    if request.method == "POST":
        
        form = UploadRoutingFileForm(request.POST, request.FILES)
        # ins = get_object_or_404(POST, pk=pk)
        # print ins
        if form.is_valid():
            file_in_memory = request.FILES['file']
            wb = load_workbook(file_in_memory)
            sheet =  wb.active
            address_object = ADDRESS
            # ws = wb.get_sheet_names()
            # print ws
            b=1
            e=0
            f=0
            for row in sheet.iter_rows():
                col1 = 'A' + str(b)  # ColA till J in new sheet for output
                col2 = 'B' + str(b)
                col3 = 'C' + str(b)
                col4 = 'D' + str(b)
                col5 = 'E' + str(b)
                col6 = 'F' + str(b)
                col7 = 'G' + str(b)
                col8 = 'H' + str(b)
                col9 = 'I' + str(b)
                col10 = 'J' + str(b)
                col11 = 'K' + str(b)
                col12 = 'L' + str(b)
                col13 = 'M' + str(b)
                col14 = 'N' + str(b)
                col15 = 'O' + str(b)
                col16 = 'P' + str(b)
                a= sheet[col1].value
                x= sheet[col2].value
                c= sheet[col3].value
                d= sheet[col4].value
                e= sheet[col5].value
                f= sheet[col6].value
                g= sheet[col7].value
                # print g
                h= sheet[col8].value
                i= sheet[col9].value
                #print type(i)
                j= sheet[col10].value
                # print j
                k= sheet[col11].value
                l= sheet[col12].value
                m= sheet[col13].value
                n= sheet[col14].value
                o= sheet[col15].value
                p= sheet[col16].value
                # print request.j.value
                # v2 = ADDRESS.objects.all().filter(Store_Nbr=int(g))
                # print v2  
                # aaa= request.POST[a]
                # print aaa
                # rst= j
                # print rst
                # rout = ROUT.objects.get(pk=1)
                # print rout
                address1 = ADDRESS.objects.get(Store_Nbr = sheet[col7].value)
                # print address1
                # rout.address_object=address1 
                # rr=rout.address_object
                # print rr
                # rr.save()
                # print rout                          
                instance= routobject(PO=j, STATUS=a, Ship_On_Date=x,
                               Carrier_PU_Date = c, Carrier_Due_Date=d,
                               Carrier_Name=e, Mode= f, Load_Dest=g ,
                               Shippoint=h, Load=str(i), Cases=k,
                               Weight=l, Pallets=m, Cube= n,
                               PO_Type=o, Dept=p, AddressStore= rr) # this is no in data base 
                instance.save()
              
                b += 1
            # print "file is loded successfully"
            
            return HttpResponse("Done")
    else:
        form=UploadRoutingFileForm()
    return render(request, 'routing_file.html', {'form': form})

#####################################################
####################Address view ####################
#####################################################
class AddressFileForm(forms.Form):
    file = forms.FileField()

def AddressFile(request):
    address_object = ADDRESS
    
    if request.method == "POST":
        form = AddressFileForm(request.POST, request.FILES)
        if form.is_valid():
            file_in_memory = request.FILES['file']
            wb = load_workbook(file_in_memory)
            sheet =  wb.active            
            b=1

            for row in sheet.iter_rows():
                col1 = 'A' + str(b)  # ColA till J in new sheet for output
                col2 = 'B' + str(b)
                col3 = 'C' + str(b)
                col4 = 'D' + str(b)
                col5 = 'E' + str(b)
                col6 = 'F' + str(b)
                col7 = 'G' + str(b)
                a= sheet[col1].value
                x= sheet[col2].value
                c= sheet[col3].value
                d= sheet[col4].value
                e= sheet[col5].value
                f= sheet[col6].value
                g= sheet[col7].value
                instance= address_object(Store_Nbr=a, Address=x,
                                City = c, State_Or_Prov=d,
                                Load_Dest=g,Postal_Code=e, Phone_Number=f, Manager_Name=g
                                )
                instance.save()
               
                b += 1
            # print "file is loded successfully"

            return HttpResponse("Done")
    else:
        form=UploadRoutingFileForm()
    return render(request, 'adress.html', {'form': form})

########################################################
#################### Bol Slip File #####################
# ######################################################

def BolListDisplay(request):    
    query_length = ROUT.objects.all()    
    context = {"query_result":query_length}    
    return render(request, 'index.html', context)


########################################################
#################### Bol Slip File #####################
# ######################################################

class UploadBolFileForm(forms.Form):
    file = forms.FileField()

def BolDisplay(request):
    bolObject = Bol
    if request.method == "POST":
        form = UploadBolFileForm(request.POST, request.FILES)
        if form.is_valid():
            file_in_memory = request.FILES['file']
            wb = load_workbook(file_in_memory)
            sheet =  wb.active
            
            # ws = wb.get_sheet_names()
            # print ws
            b=2
            
            for row in sheet.iter_rows():
                col1 = 'A' + str(b)  # ColA till J in new sheet for output
                col2 = 'B' + str(b)
                col3 = 'C' + str(b)
                col4 = 'D' + str(b)
                col5 = 'E' + str(b)
                col6 = 'F' + str(b)
                col7 = 'G' + str(b)
                col8 = 'H' + str(b)
                col9 = 'I' + str(b)
                
                a= sheet[col1].value
                x= sheet[col2].value
                c= sheet[col3].value
                d= sheet[col4].value
                e= sheet[col5].value
                f= sheet[col6].value
                g= sheet[col7].value               
                h= sheet[col8].value
                i= sheet[col9].value                                        
                instance= bolObject(PO=a, Cases=x, Weight=c,
                               Pallet = d, Ship=e,
                               Must_arrive_date=f, PO_Type= g, Dept_type=h , Destination_code=i) # this is no in data base 
                instance.save()
               
                b += 1
            # print "file is loded successfully"
            
            return HttpResponse("Done")
    else:
        form=UploadRoutingFileForm()
    return render(request, 'bolfile.html', {'form': form})

######################################################
#################### jbhunt File #####################
######################################################

class UploadjbhuntForm(forms.Form):
    file = forms.FileField()

def JbhuntDisplay(request):
    jbhuntObject = JbhuntLabel
    if request.method == "POST":
        form = UploadjbhuntForm(request.POST, request.FILES)
        if form.is_valid():
            file_in_memory = request.FILES['file']
            wb = load_workbook(file_in_memory)
            sheet =  wb.active
            
            b=2
            
            for row in sheet.iter_rows():
                col1 = 'A' + str(b)  # ColA till J in new sheet for output
                col2 = 'B' + str(b)
                col3 = 'C' + str(b)
                col4 = 'D' + str(b)
                col5 = 'E' + str(b)
                col6 = 'F' + str(b)
                col7 = 'G' + str(b)
                col8 = 'H' + str(b)
                col9 = 'I' + str(b)
                
                a= sheet[col1].value
                x= sheet[col2].value
                c= sheet[col3].value
                d= sheet[col4].value
                e= sheet[col5].value
                f= sheet[col6].value
                g= sheet[col7].value               
                h= sheet[col8].value
                i= sheet[col9].value                                        
                instance= jbhuntObject(Doc=a, Cases=x, Weight=c,
                               Address = d, City=e,
                               State_Or_Prov=f, Postal_Code= g, Destination_code=h) # this is no in data base 
                instance.save()
               
                b += 1
            # print "file is loded successfully"
            
            return HttpResponse("Done")
    else:
        form=UploadjbhuntForm()
    return render(request, 'jbhunt.html', {'form': form})

def palletDisplay(request):
    rout = ROUT.objects.all()
    address= ADDRESS
    context={
        "rout" : rout,
        "address": address,
    }

    return render(request, "palletdisplay.html", context)



