from django.conf.urls import url
from wallmart import views


urlpatterns = [
    url(r'^$', views.exceltodatabase, name="exceltodatabase"),
    url(r'^db2xlsx/$', views.db2xlsx, name="db2xlsx"),
    url(r'^routing/$', views.routing, name="routing"),
    url(r'^address/$', views.address_file, name="address"),
    url(r'^listing/$', views.list, name="listing"),
    url(r'^display/$', views.UploadFile, name="display"),
    url(r'^routing_form/$', views.RoutingFile, name="routing_form"),
    url(r'^bol_file/$', views.BolDisplay, name="bol_file"),
    url(r'^pallet_file/$', views.JbhuntDisplay, name="pallet_file"),
    url(r'^address/$', views.AddressFile, name="address"),
# these are the main display url
    url(r'^boldisplay/$', views.BolListDisplay, name="boldisplay"),
    url(r'^palletfile_display/$', views.palletDisplay, name="palletfile_display"),
    

]
