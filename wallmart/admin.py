# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import *
from django.contrib import admin

# Register your models here.

admin.site.register(Wallmart)

class RoutAdmin(admin.ModelAdmin):
    model = ROUT
    list_display =['PO','Carrier_Name','STATUS','Cases','Dept']

admin.site.register(ROUT, RoutAdmin)

class AddressAdmin(admin.ModelAdmin):
    model = ADDRESS    
    list_display = ['Store_Nbr','Manager_Name','Address','City','Postal_Code','Phone_Number']
    fields = ['Store_Nbr','Address','City','State_Or_Prov','Postal_Code','Phone_Number','Manager_Name']
admin.site.register(ADDRESS, AddressAdmin)

class ListingAdmin(admin.ModelAdmin):
    model = LISTING
    list_display = ['Document_Type','Document_Number','Received_Date','Vendor_Number','Location']
    fields = ['Document_Type','Document_Number','Received_Date','Vendor_Number','Location']

admin.site.register(LISTING, ListingAdmin)
admin.site.register(bolSlipFile)
class bolAdmin(admin.ModelAdmin):
    model = LISTING
    list_display = ['PO','Cases','Weight','Pallet','Destination_code']
    fields =['PO','Cases','Weight','Pallet','Destination_code']

admin.site.register(Bol, bolAdmin)
class JbhuntAdmin(admin.ModelAdmin):
    model = LISTING
    list_display = ['Doc','Cases','Weight','Address','City','State_Or_Prov','Postal_Code','Destination_code']
    fields =['Doc','Cases','Weight','Address','City','State_Or_Prov','Postal_Code','Destination_code']
admin.site.register(JbhuntLabel,JbhuntAdmin)

