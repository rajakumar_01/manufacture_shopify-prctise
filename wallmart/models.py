# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils import timezone
import datetime
from ordermanage.models import *

from django.db import models

  
class Wallmart(models.Model):
    Purchase_Order_Number = models.ForeignKey(Order,on_delete=models.CASCADE)
    Purchase_Order_Date = models.DateTimeField('PO date', null=True)
    Ship_Not_Before	= models.DateTimeField('Ship Not Before',null=True) #only wallmart
    Must_Arrive_By = models.DateTimeField('Must Arrive By', null=True)
    Order_Type = models.CharField(max_length=10, null=True) # only walmart
    Currency = models.CharField(max_length=3, null=True)  # only walmart
    Department = models.CharField(max_length=5, null=True) # only walmart
    Promotional_Event = models.CharField(max_length=50, null=True) # only walmart
    FOB = models.CharField(max_length=10, null=True) # only walmart
    Payment_Terms = models.CharField(max_length=50, null=True) # ordermange walmart
    FOB_Ship_Point = models.CharField(max_length=20, null=True) # only walmart
    Truck_Vendor_Pool = models.CharField(max_length=15) #only walmart
    Supplier_Name = models.CharField(max_length=30, null=True) #only walmart
    Supplier_Number = models.CharField(max_length=10, null=True) #only walmart
    Total_Line_Items = models.CharField(max_length=13, null=True)# in both table
    Total_Units_Ordered = models.IntegerField(null=True) #replace CharField if value is integer
    Allowance = models.CharField(max_length=4, null=True)
    Allowance2 = models.CharField(max_length=4, null=True)
    Total_Order_Amount = models.CharField(max_length=5, null=True)
    Ship_Address_1 = models.CharField(max_length=100, null=True)
    Ship_Address_2 = models.CharField(max_length=100, null=True)
    Ship_Address_3 = models.CharField(max_length=100, null=True)
    Bill_Address_1 = models.CharField(max_length=100, null=True)
    Bill_Address_2 = models.CharField(max_length=100, null=True)
    Bill_Address_3 = models.CharField(max_length=100, null=True)
    order_item_quentity = models.CharField(max_length=100,) # in the place of line and item
    # Line = models.IntegerField(null=True)
    # Item = models.IntegerField(null=True)
    Gtin = models.CharField(max_length=20, null=True)# inventry and wallmart
    Supplier_Stock = models.CharField(max_length=20, null=True)
    Color = models.CharField(max_length=15, null=True)
    Size = models.CharField(max_length=20, null=True)# inventry update
    Quantity_Ordered = models.CharField(max_length=20, null=True) #in ordermanage and walmart
    UOM = models.CharField(max_length=20, null=True) #in walmart
    Pack = models.CharField(max_length=20, null=True)
    Cost = models.CharField(max_length=20, null=True)
    Extended_Cost = models.CharField(max_length=20, null=True)

 
    def __str__(self):
        return (self.Supplier_Name)

class ADDRESS(models.Model):
    #PO_Number = models.ForeignKey(ROUT)
    #id =  models.AutoField(primary_key=True, default=1)
    Store_Nbr = models.IntegerField(primary_key=True, null=False)
    Address = models.CharField(max_length=100, null=True)
    City = models.CharField(max_length=50, null=True)
    State_Or_Prov = models.CharField(max_length=3, null=True)
    Postal_Code = models.IntegerField(null=True)
    Phone_Number = models.CharField(max_length=20, null=True)
    Manager_Name = models.CharField(max_length=30, null=True)

    def __str__(self):
        return (self.Address)

class ROUT(models.Model):
    id = models.IntegerField(null=False, primary_key=True)
    PO = models.IntegerField(null=True)
    STATUS = models.CharField(max_length=10, null=True)
    Ship_On_Date = models.CharField(null=True,max_length=50)
    Carrier_PU_Date = models.CharField(null=True,max_length=50)
    Carrier_Due_Date = models.CharField(null=True,max_length=50)
    Carrier_Name = models.CharField(max_length=200, null=True)
    Mode = models.CharField(max_length=200, null=True)
    ################################
    Load_Dest = models.IntegerField(null=True)
    ################################
    Shippoint = models.CharField(null=True, max_length=50)
    Load = models.CharField(null=True, max_length=50)
    Cases = models.CharField(null=True, max_length=50)
    Pallets = models.CharField(null=True, max_length=50)
    Weight = models.CharField(null=True, max_length=50)
    Cube = models.CharField(null=True, max_length=50)
    PO_Type = models.CharField(null=True, max_length=50)
    Dept = models.CharField(null=True, max_length=50)
    AddressStore = models.ForeignKey(ADDRESS, blank=True,on_delete=models.CASCADE)

    def __str__(self):
        return (self.Carrier_Name)


class LISTING(models.Model): 
    Document_Number = models.BigIntegerField(null=True)
    Document_Type = models.CharField(max_length=10, null=True, default="PO")
    Received_Date = models.CharField(max_length=25, null=True)
    Vendor_Number = models.IntegerField(null=True, default=615026)
    Location = models.IntegerField(null=True)

    def __str__(self):
        return (self.Document_Type)


class bolSlipFile(models.Model):

    PO_Number = models.ForeignKey(ROUT,on_delete=models.CASCADE)
    slip_address= models.ForeignKey(ADDRESS,on_delete=models.CASCADE)
    name = models.CharField(max_length=10, null=True)
    def __str__(self):
        return '%s','%s',(self.name)

class Bol(models.Model):
    id = models.IntegerField(null=False, primary_key=True) # for stablising relation 
    PO = models.IntegerField(null=True) # set it as a foreign ke from rout table 
    Cases = models.CharField(max_length=50, default="4", null=True,) # also from Rout table 
    Weight = models.CharField(max_length=50, default="56",null=True, ) # also from Rout table
    Pallet = models.CharField(max_length=10, default="Y", null=True,)
    Ship = models.CharField(max_length=10, default="N", null=True,) 
    Must_arrive_date = models.CharField(null=True, max_length=50) #make is as date time field    
    PO_Type = models.CharField(max_length=50, default="33", null=True,)
    Dept_type = models.CharField(max_length=50, default="17", null=True,)
    ################################
    Destination_code = models.IntegerField(null=True) # common in both address and rout
    ################################
    
    def __str__(self):
        return (self.Pallet)

class JbhuntLabel(models.Model):
    id = models.IntegerField(null=False, primary_key=True) # for stablising relation 
    Doc = models.IntegerField(null=True) # set it as a foreign ke from rout table #po
    Cases = models.CharField(null=True, max_length=50) # also from Rout table 
    Weight = models.CharField(null=True, max_length=50) # also from Rout table
    Address = models.CharField(max_length=100, null=True) #from address table 
    City = models.CharField(max_length=50, null=True) #from address table 
    State_Or_Prov = models.CharField(max_length=3, null=True) #from address table 
    Postal_Code = models.IntegerField(null=True) #from address table 
    ################################
    Destination_code = models.IntegerField(null=True) # common in both address and rout

    def __str__(self):
        return (self.Address)



