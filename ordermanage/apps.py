from django.apps import AppConfig
import django.core.handlers.wsgi

class OrdermanageConfig(AppConfig):
    name = 'ordermanage'
