from django import forms
from django.forms import ModelForm
from django.forms import MultiWidget
from django.forms import ModelChoiceField
from .models import Order, Company1, CourierMethod, Sku_Table, Address, LineTable
from shop.models import *
from django.contrib.admin.widgets import AdminDateWidget
from django.forms.fields import DateField
from django.core import validators
from django.core.validators import RegexValidator



'''==========================================================================
    Project Name: OrderManage
    Name: OrderManage Form
    URL: https://eknous.com
    Author: eKnous Technology Solutions
    License: https://eknous.com/license/
==========================================================================='''

'''=====================================#
               Form 
   #====================================#'''

class SimpleForm(ModelForm):   
    po = forms.CharField(widget = forms.TextInput(attrs={'readonly':'readonly'}))
    # order_date = forms.CharField(widget = forms.TextInput(attrs={'readonly':'readonly'}))
    pickup_date = forms.CharField(widget = forms.TextInput(attrs={'readonly':'readonly'}))
    # STATUS = forms.CharField(widget=forms.Select(choices=status))
    # asn_sent = forms.CharField(widget=forms.Select(choices= AsnSent))    
    # wharehouse_staff_status = forms.CharField(widget=forms.Select(choices=WhareHouseStatus))
    # invoice_send = forms.CharField(widget=forms.Select(choices=InvoiceStatus))
    # pickup_date = forms.DateField(widget = forms.SelectDateWidget())
    #pickup_date = forms.DateField(widget=AdminDateWidget())
    
    class Meta:
        model = Order
        fields = ["po", "pickup_date","asn_sent","status","invoice_send","traking_number","Warehouse_remark", ]
     
    def clean_asn_sent(self):
        asn_sent= self.cleaned_data.get('asn_sent')
        if "no" in asn_sent:
            raise forms.ValidationError("asn should be yes")
        else:
            return asn_sent 

    # for warehouse  

    def clean_warehouse_staff_status(self):
        warehouse_staff_status= self.cleaned_data.get('warehouse_staff_status')
        if not warehouse_staff_status:
            raise forms.ValidationError("please enter ware house status")
        else:
            return warehouse_staff_status
    
    def cleane_order_date(self):
        order_date = self.cleaned_data.get('order_date')
        pickup_date = self.cleaned_data.get('pickup_date')
        if order_date > pickup_date:
            raise forms.ValidationError('Pick up date should not be Past date')
        else:
             return order_date

class SingleEditForm(forms.ModelForm):
    # status= [
    #     ('open', 'open'),
                       
    #     ('NA', 'NA'),
    #     ]
    # WhareHouseStatus= [
    #     ('open', 'open'),
                         
    #     ('NA', 'NA'),
    #     ] 
    # InvoiceStatus= [
    #     ('Not Sent', 'Not Sent'),
              
    #     ('NA', 'NA'),
    #     ]
    # AsnSent =[
    #     ('no', 'no'),
       
    #     ]
    po = forms.CharField(widget = forms.TextInput(attrs={'readonly':'readonly'}))
    # order_date = forms.CharField(widget = forms.TextInput(attrs={'readonly':'readonly'}))
    # pickup_date = forms.CharField(widget = forms.TextInput(attrs={'readonly':'readonly'}))
    # status = forms.CharField(widget=forms.Select(choices=status))
    # asn_sent = forms.CharField(widget=forms.Select(choices= AsnSent))    
    # wharehouse_staff_status = forms.CharField(widget=forms.Select(choices=WhareHouseStatus))
    # invoice_send = forms.CharField(widget=forms.Select(choices=InvoiceStatus))
    # pickup_date = forms.DateField(widget = forms.SelectDateWidget())
    #pickup_date = forms.DateField(widget=AdminDateWidget())
    
    class Meta:
        model = Order
        # fields = ["po","order_date","asn_sent","STATUS","wharehouse_staff_status","invoice_send","Warehouse_remark"]
        fields = ["po","order_po_date","company_name","courier_method","Directory_Name","Warehouse_remark", "status","asn_sent" , "invoice_send"]
        
     
# class QuantityModelChoiceField(ModelChoiceField):
#         def label_from_instance(self, obj):
#             return obj.quantity
#     quantity = forms.QuantityModelChoiceField(queryset=Company1.objects.all())


class CreateForm(forms.ModelForm): 
   
    class Meta:
        model = Order       
        fields = ["po","order_po_date","company_name","courier_method","Directory_Name","remark"]
    
  
    def clean_po(self):
        po= self.cleaned_data.get('po')
        if po is None :
            raise forms.ValidationError("shivam fill po")
        # elif (r'^[0-9a-zA-Z]*$') not in po:
        #     raise forms.ValidationError("please fill po date")

        else:
            return po
            
    # def clean_po(self):
    #     po= self.cleaned_data.get('po')
    #     if po is None :
    #         raise forms.ValidationError("please fill po date")           
    #     else:
    #         return po
           

        # field_args = {
        #     "order_date" : {
        #         "error_messages" : {
        #             "required" : "Please let us know what to call you!"
        #         }
        #     }
        # }
    # def __init__(self, *args, **kwargs):
    #     extra_fields = kwargs.pop('extra', 1)

    #     super(CreateForm, self).__init__(*args, **kwargs)
    #     self.fields['extra_field_count'].initial = extra_fields

    #     for index in range(int(extra_fields)):
    #         # generate extra fields in the number specified via extra_fields
    #         self.fields['extra_field_{index}'.format(index=index)] = \
    #             forms.CharField()
########################################
########## end of ceate ################
########################################

class ShipmentStatus(forms.CheckboxInput):
    input_type = 'checkbox'
    
class ShipmentStatusForm((forms.ModelForm)):
    class Meta:
        model = Order
        fields = ['status']
        widgets = {
            'status': ShipmentStatus(),
        }
class inventryForm(forms.ModelForm):
    class Meta:
        model =Sku_Table
        fields =["Po_request","Product_Sku","company_name","order_quantity"]

class LineTableForm(forms.ModelForm):
    class Meta:
        model =LineTable
        fields = ['purchase_order','vendor_SKU','order_quantity']


##############################
"""active user form """
##############################

class ActiveUserForm(forms.ModelForm):    
    po = forms.CharField(widget = forms.TextInput(attrs={'readonly':'readonly'}))   
    asn_sent = forms.CharField(widget = forms.TextInput(attrs={'readonly':'readonly'}))
    status = forms.CharField(widget = forms.TextInput(attrs={'readonly':'readonly'}))
    invoice_send = forms.CharField(widget = forms.TextInput(attrs={'readonly':'readonly'}))   
    class Meta:
        model = Order
        fields = ["po", "pickup_date","asn_sent","status","warehouse_staff_status","invoice_send", "Warehouse_remark"]
 

class CourierMethodForm(forms.ModelForm):
     class Meta:
        model = CourierMethod
        fields = ['courier_method']

class AddressForm(forms.ModelForm):
    class Meta:
        model = Address
        fields = ['address', 'city', 'state','zip', 'phone_number']
