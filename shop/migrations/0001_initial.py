# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-01-10 18:17
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=200)),
                ('slug', models.SlugField(max_length=200, unique=True)),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': 'category',
                'verbose_name_plural': 'categories',
            },
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('company_sku', models.CharField(max_length=255, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='CompanyName',
            fields=[
                ('companyname_id', models.AutoField(primary_key=True, serialize=False)),
                ('companyname', models.CharField(max_length=250, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Manufacturer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('manufacturer_sku', models.CharField(max_length=250)),
                ('manufacturer_name', models.CharField(max_length=250)),
                ('manufacturer_product_Quantity', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Product_SKU', models.CharField(max_length=20, unique=True)),
                ('name', models.CharField(db_index=True, max_length=200)),
                ('slug', models.SlugField(max_length=200)),
                ('image', models.FileField(blank=True, upload_to=b'products/%Y/%m/%d')),
                ('description', models.TextField(blank=True)),
                ('bulletpoints', models.TextField(blank=True)),
                ('Product_Dimensions', models.CharField(max_length=20)),
                ('Color', models.CharField(max_length=20)),
                ('price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('quantity', models.PositiveIntegerField()),
                ('block_quantity', models.IntegerField(blank=True, default=0, null=True)),
                ('available_quentity', models.IntegerField(blank=True, default=1, null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('available', models.BooleanField(default=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('Material_Type', models.CharField(max_length=40)),
                ('Model_Number', models.CharField(blank=True, max_length=40, null=True)),
                ('ASIN', models.CharField(blank=True, max_length=20, null=True)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='products', to='shop.Category')),
            ],
            options={
                'ordering': ('-created',),
            },
        ),
        migrations.CreateModel(
            name='ShopifyProductDetails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_sku', models.CharField(max_length=256)),
                ('product_id', models.CharField(max_length=256, unique=True)),
                ('variants_id', models.CharField(max_length=256)),
                ('options_id', models.CharField(max_length=256)),
                ('images_id', models.CharField(max_length=256)),
            ],
        ),
        migrations.AddField(
            model_name='company',
            name='Product_SKU',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='prd_sku', to='shop.Product'),
        ),
        migrations.AddField(
            model_name='company',
            name='company_name',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='company_name', to='shop.CompanyName'),
        ),
        migrations.AlterIndexTogether(
            name='product',
            index_together=set([('id', 'slug')]),
        ),
    ]
