from django.contrib import admin
from .models import Category, Product, Company, Manufacturer, CompanyName
from ordermanage.models import  Order, Sku_Table, ShopifyProductDetails
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)} #prepopulated_fields:=>where the value is automatically set by using the value of other field 
admin.site.register(Category, CategoryAdmin)


class ProductAdmin(admin.ModelAdmin):
    list_display = ['Product_SKU','name', 'slug', 'category', 'price','available_quentity','block_quantity', 'quantity', 'available', 'created', 'updated']
    list_filter = ['available', 'created', 'updated', 'category']
    list_editable = ['price', 'quantity', 'available'] # that can be edited from listdisplay page
    prepopulated_fields = {'slug': ('name',)}
admin.site.register(Product, ProductAdmin)

"""company name New Admin table"""


class CompanyNameAdmin(admin.ModelAdmin):
    list_display=["companyname_id","companyname",]

admin.site.register(CompanyName, CompanyNameAdmin)

class CompanyAdmin(admin.ModelAdmin):
    list_display=["company_name","company_sku","Product_SKU" ]

admin.site.register(Company, CompanyAdmin)

class ShopifyProductDetailsAdmin(admin.ModelAdmin):
    list_display = ['product_sku','product_id','variants_id','options_id','images_id']
admin.site.register(ShopifyProductDetails, ShopifyProductDetailsAdmin)

# class ManufacturerAdmin(admin.ModelAdmin):
#     list_display=["manufacturer_name","manufacturer_sku"]

# admin.site.register(Manufacturer, ManufacturerAdmin)
