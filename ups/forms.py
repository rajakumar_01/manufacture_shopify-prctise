from django import forms
from django.forms import MultiWidget
from .models import UpsLoginModel, ShipperModel, ShipFromModel

class UpsAddAcountForm(forms.ModelForm):

    class Meta:
        model = UpsLoginModel
        fields = ["username","password"]

class UpsShipperForm(forms.ModelForm):
    class Meta:
        model = ShipperModel
        fields = ['username',
                    'name',
                    'attentionname',
                    'tax_identification_number',
                    'phon_number','extension',
                    'shipper_account_number',
                    'fax_number',
                    'address_line',
                    'city',
                    'state_province_code',
                    'postal_code',
                    'country_code'
                ]
class ShipFromModelForm(forms.ModelForm):

    class Meta:
        model = ShipFromModel
        fields = '__all__'