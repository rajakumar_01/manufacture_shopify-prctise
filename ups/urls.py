from django.conf.urls import url
from django.conf.urls.static import static
from . import views

urlpatterns = [
    # url(r'^$', views.index, name="index")
    url(r'^$', views.UpsCreateShipment, name="ship"),
    url(r'^recovery/$', views.LabelRecoveryRequest, name="recovery"),
    url(r'^void/$', views.UpsVoidShipment, name="void"),
    url(r'^track/$', views.GetTrackingNumber, name="track"),
    url(r'^track_signature/$', views.GetTrackingNumber, name="track_signature"),
    url(r'^image_converter/$', views.image_converter, name="image_converter"),
    url(r'^address_validater/$', views.UpsAddressValidation, name="address_validater"),
    url(r'^add_upsaccount/$', views.AddUpsAccount, name="add_upsaccount"),
    url(r'^add_shipper/$', views.AddUpsShipper, name="add_shipper"),
    url(r'^add_shipfrom/$', views.AddShipFrom, name="add_shipfrom"),
    url(r'^sipper_list/$', views.ShipperListDisplay, name="sipper_list"),
    url(r'^shipfrom_list/$', views.ShipfromListDisplay, name="shipfrom_list"),
    url(r'^csv/$', views.ReadFromCsv, name="csv"),
    url(r'^exls/$', views.ReadFromExls, name="exls"),
    url(r'^shipment/$', views.CreateShipment, name="shipment")

]
 