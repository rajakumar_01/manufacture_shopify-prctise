# -*- coding: utf-8 -*-
from __future__ import unicode_literals 

from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse
from .models import *
import json, requests, csv, base64, os

from ordermanage.models import *
from shop.models import *
import datetime
from .forms import UpsAddAcountForm, UpsShipperForm, ShipFromModelForm
from django.conf import settings
from . import variables

'''===========================================================
       Project Name: Ups Api
       Function Name: Add Account details
       URL: http://127.0.0.1:8000/ups/add_upsaccount
       Author: eKnous Technology Solutions
       License: https://eknous.com/license/ 
============================================================'''
def AddUpsAccount(request):
       
    if request.method == 'POST':
        form = UpsAddAcountForm(request.POST or None)              
        if form.is_valid():
            instance = form.save(commit = False)
            instance.save()
    # else:
    form = UpsAddAcountForm()

    return render (request, 'ups_account.html', {"form":form})


'''===========================================================
       Project Name: Ups Api
       Function Name: Add Shipper Details
       URL: http://127.0.0.1:8000/ups/add_ups_shipper
       Author: eKnous Technology Solutions
       License: https://eknous.com/license/ 
============================================================'''


def AddUpsShipper(request):
       
    if request.method == 'POST':
        form = UpsShipperForm(request.POST or None)              
        if form.is_valid():
            instance = form.save(commit = False)
            instance.save()
            return HttpResponseRedirect(reverse('sipper_list'))
    # else:
    form = UpsShipperForm()

    return render (request, 'ups_shipper.html', {"form":form})

'''===========================================================
       Project Name: Ups Api
       Function Name: Add Shipper Details
       URL: http://127.0.0.1:8000/ups/add_ups_shipper
       Author: eKnous Technology Solutions
       License: https://eknous.com/license/ 
============================================================'''


def AddShipFrom(request):
       
    if request.method == 'POST':
        form = ShipFromModelForm(request.POST or None)              
        if form.is_valid():
            instance = form.save(commit = False)
            instance.save()
            return HttpResponseRedirect(reverse('shipfrom_list'))
    # else:
    form = ShipFromModelForm()

    return render (request, 'ups_shipfrom.html', {"form":form})

'''===========================================================
       Project Name: Ups Api
       Function Name: Add Shipper Details
       URL: http://127.0.0.1:8000/ups/list_display
       Author: eKnous Technology Solutions
       License: https://eknous.com/license/ 
============================================================'''

def ShipperListDisplay(request):
    queryset = ShipperModel.objects.all()
    return render(request, 'ups_list.html', {'queryset':queryset, 'title': "Shipper Account"})


def ShipfromListDisplay(request):
    queryset = ShipFromModel.objects.all()
    return render(request, 'ups_list.html', {'queryset':queryset, 'title': "ShipFrom List"})




# Create your views here.
def index(request):
    return HttpResponse('finally ups done')

'''============================================
       Project Name: Ups Api
       Name: Address UPSApi
       URL: http://127.0.0.1:8000/ups
       Author: eKnous Technology Solutions
       License: https://eknous.com/license/ 
   ============================================'''

def UpsAddressValidation(request):
    url = 'https://wwwcie.ups.com/rest/AV'
    header = variables.header
    
    data ={  
            "AccessRequest":{  
                "AccessLicenseNumber":variables.AccessLicenseNumber,
                "UserId":"eknousllc",
                "Password":"eknous@2018"
            },
            "AddressValidationRequest":{  
                "Request":{  
                    "TransactionReference":{  
                        "CustomerContext":"eKnous OrdermanageUpsApi",

                    },
                    "RequestAction":"AV"
                },
                "Address":{  
                    "City":"PRINCETON",
                    "StateProvinceCode":"NJ",
                    "PostalCode":"08540"
                }
            }
        }
    response=requests.post(url, headers= header, data= json.dumps(data))
    # print response
    # return HttpResponse(dict)
    dict = json.dumps(response.json(), indent=4)    
    return render(request, "json.json" , {'dict':dict}, content_type='application/json')
'''============================================
    Project Name: Ups Api
    Name: Create ups Shipment Labal
    URL: http://127.0.0.1:8000/ups
    Author: eKnous Technology Solutions
    License: https://eknous.com/license/ 
============================================'''

def UpsCreateShipment(request): 
    shipping_Address= Address.objects.all()
    now = datetime.datetime.now().strftime("%m%d%Y%S")
    k= len(shipping_Address)
    final_file = open("UPSshippingLbl/final"+now+".html", 'wb+')
    for i in shipping_Address:
        print ("==============Address po")
        order= i.po_number
        print("order:",order.po)
        order_id = Order.objects.get(po = order.po)
        print ("==============order_id")
        print (type(order_id))
        print (order_id)
        print (order_id.status )
        print ("==============")

        shipment_service_code = order_id.courier_method.valid_code
        product_dimensions =  order_id.company_name.Product_SKU.Product_Dimensions
        dimensions = product_dimensions.split('x')
        length = dimensions[0]
        width = dimensions[1]
        height = dimensions[2]
        label_response= ""
        order_qty1 = LineTable.objects.filter(purchase_order = i.po_number)
        order_qty = 0
        for j in order_qty1:
            order_qty += int(j.order_quantity)
            print (order_qty)
            print (j.purchase_order)
        qty = 1
        print (i.po_number.status)
        for qty in range(order_qty):
            if 'OPEN' in i.po_number.status:
                print ("in loop:",len(shipping_Address))
                url = 'https://wwwcie.ups.com/rest/Ship'
                # url ='https://onlinetools.ups.com/rest/Ship'
                header =  variables.header                
                name = (i.address).split(',')                
                data = {  
                            "UPSSecurity":{  
                            "UsernameToken":variables.ups_login(request),
                            "ServiceAccessToken":{  
                                "AccessLicenseNumber": variables.AccessLicenseNumber
                            }
                            },
                            "ShipmentRequest":{  
                            "Request":{  
                                "RequestOption":"validate",
                                "TransactionReference":{  
                                    "CustomerContext":name[0]
                                }
                            },
                            "Shipment":{  
                                "Description":str(i.po_number),
                                "Shipper":variables.shipper(request),
                                "ShipTo":{  
                                    "Name":name[0],
                                    "AttentionName":name[0],
                                    "Phone":{  
                                    "Number":i.phone_number
                                    },
                                    "Address":{  
                                    "AddressLine":"100 Overlook Center",
                                    "City":"PRINCETON",
                                    "StateProvinceCode":"NJ",
                                    "PostalCode":"08540",
                                    "CountryCode":"US"
                                    }
                                },
                                "ShipFrom":variables.ShipFrom(request),
                                "PaymentInformation":{  
                                    "ShipmentCharge":{  
                                    "Type":"01",
                                    "BillShipper":{  
                                        "AccountNumber":"5238y9"
                                    }
                                    }
                                },
                                "Service":{ 
                                    "Code":shipment_service_code,
                                    "Description":"Express2"
                                },
                                "Package":{
                                    "Description":name[0],
                                    "Packaging":{  
                                    "Code":"02",
                                    "Description":str(i.po_number)
                                    },
                                    "Dimensions":{  
                                    "UnitOfMeasurement":{
                                        "Code":"IN",
                                        "Description":"Inches"
                                    },
                                    "Length":length,
                                    "Width":width,
                                    "Height":height
                                    },
                                    "PackageWeight":{  
                                    "UnitOfMeasurement":{  
                                        "Code":"LBS",
                                        "Description":"Pounds3"
                                    },
                                    "Weight":"10"
                                    }
                                }
                            },
                            "LabelSpecification":{  
                                "LabelImageFormat":{  
                                    "Code":"GIF",
                                    "Description":"GIF"
                                },
                            
                            }
                        }
                    }                
                print(data)
                response = requests.post(url, headers= header, data= json.dumps(data))       
                label_response=  json.loads(response.content)
                print (label_response)      
                obj_ups = UpsModel                
                if label_response:
                    print ("lbl is working")
                    tracking_number =label_response["ShipmentResponse"]["ShipmentResults"]['ShipmentIdentificationNumber']
                    print (tracking_number)
                    # tracking_number =1
                    if tracking_number:
                        ups_trackingnumber = obj_ups(po=i, tracking_number =tracking_number )
                        ups_trackingnumber.save()
                    else:
                        print ("no shipment")
                    str_image1= label_response["ShipmentResponse"]["ShipmentResults"]["PackageResults"]["ShippingLabel"]["HTMLImage"]
                    str_image= label_response["ShipmentResponse"]["ShipmentResults"]["PackageResults"]["ShippingLabel"]["GraphicImage"]            
                    a = str_image1.decode('base64')            
                    obj = SaveImage
                    obj1 = obj(image=str_image)
                    obj1.save()
                    fh1 = open("var/www/ordermanage/UPSshippingLbl/ShipingLabel_UPS-PO_"+str(i)+".html", "wb+")
                    fh1.write(base64.b64decode(str_image1))
                    fh1.close()
                    fh = open("var/www/ordermanage/UPSshippingLbl/label"+str(tracking_number)+".gif", "wb+")
                    fh.write(base64.b64decode(str_image))
                    fh.close()
                    final_file.write(base64.b64decode(str_image1))
                    final_file.write("<br><br><hr><br><br><br>")
    final_file.close()        
    dict = json.dumps(label_response, indent=4)       
    # return render(request, "json.json" , {'dict':dict}, content_type='application/json')
    return HttpResponse("Success")
    # f = open("UPSshippingLbl/final"+now+".html", "r")
    # response = HttpResponse(f)
    # # # response['Content-Disposition'] = 'attachment; filename=final.html'
    # f.close()
    # return response

    # return response
    # return HttpResponse(open("UPSshippingLbl/final"+now+".html", 'r').read())

    
'''==================================================
       Project Name: Ups Api
       Function Name: Recovery ups Shipment Labal
       URL: http://127.0.0.1:8000/ups/recovery
       Author: eKnous Technology Solutions
       License: https://eknous.com/license/ 
==================================================='''


def LabelRecoveryRequest(request):
    # url = 'https://onlinetools.ups.com/rest/LBRecovery'
    url = "https://wwwcie.ups.com/rest/LBRecovery"
    header =  variables.header
    
    data ={
            "UPSSecurity": {
                "UsernameToken":variables.ups_login(request),
            "ServiceAccessToken": {
                "AccessLicenseNumber": variables.AccessLicenseNumber
                }
            },
            "LabelRecoveryRequest": {
                "LabelSpecification": {
                    "LabelImageFormat": {
                    "Code": "ZPL"
                    },
                    "HTTPUserAgent": "Mozilla/4.5"
                    },
                    "Translate": {
                        "LanguageCode": "eng",
                        "DialectCode": "GB",
                        "Code": "01"
                    },
                    "TrackingNumber": "1Z5238Y90305121444" #ups provide a fix ShipmentIdentificationNumber for ech function of api
            }
        }
    response=requests.post(url, headers= header, data= json.dumps(data))
    label_response=  json.loads(response.content)
    str_image= label_response["LabelRecoveryResponse"]["LabelResults"]["LabelImage"]["GraphicImage"]
    # print type(str_image)
    # a = str_image.decode('base64')
    # print type(a)
    fh = open("recovery_shipingLabelpo1.jpg", "wb+")
    fh.write(base64.b64decode(str_image))
    fh.close()
    # return HttpResponse(response)
    dict = json.dumps(response.json(), indent=4)    
    return render(request, "json.json" , {'dict':dict}, content_type='application/json')

'''==================================================
       Project Name: Ups Api
       Function Name: Void Labal
       URL: http://127.0.0.1:8000/ups/void
       Author: eKnous Technology Solutions
       License: https://eknous.com/license/ 
==================================================='''

def UpsVoidShipment(request): #UpsVoidShipment(request, orderid)
    # url = 'https://wwwcie.ups.com/rest/Void'
    url = 'https://onlinetools.ups.com/rest/Void'
    header = variables.header
    for i in UpsModel.objects.all():
        track = i.tracking_number
        # print track
        data ={
                "UPSSecurity": {
                    "UsernameToken":variables.ups_login(request),
                "ServiceAccessToken": {
                    "AccessLicenseNumber": variables.AccessLicenseNumber
                    }
                },
                "VoidShipmentRequest": {
                    "Request": {
                        "TransactionReference": {
                            "CustomerContext": "Your Customer Context"
                        }
                    },
                    "VoidShipment": {
                        "ShipmentIdentificationNumber": track,  #ups provide a fix ShipmentIdentificationNumber for ech function of api
                        # "ShipmentIdentificationNumber": "1Z5238Y90300122041"
                    }
                }

            }
        # print "void done"
        response=requests.post(url, headers= header, data= json.dumps(data))
    # return HttpResponse(response)
    dict = json.dumps(response.json(), indent=4)    
    return render(request, "json.json" , {'dict':dict}, content_type='application/json')


'''==================================================
       Project Name: Ups Api
       Function Name: Track Labal
       URL: http://127.0.0.1:8000/ups/track
       Author: eKnous Technology Solutions
       License: https://eknous.com/license/ 
==================================================='''


def GetTrackingNumber(request):
    url = 'https://wwwcie.ups.com/rest/Track' #test
    # url = 'https://onlinetools.ups.com/rest/Track' # online
    header = variables.header
    
    data ={
            "UPSSecurity": {
                "UsernameToken":variables.ups_login(request),
            "ServiceAccessToken": {
                    "AccessLicenseNumber": variables.AccessLicenseNumber
                }
            },
            "TrackRequest": {
                "Request": {
                    "RequestOption": "1",
                    "TransactionReference": {
                        "CustomerContext": "eKnous Home Depo"
                    }
                },
                "InquiryNumber": "1Z12345E6605272234" #ups provide a fix ShipmentIdentificationNumber for ech function of api 
            }
        }
    response=requests.post(url, headers= header, data= json.dumps(data))
    # print response

    # return HttpResponse(response)
    dict = json.dumps(response.json(), indent=4)    
    return render(request, "json.json" , {'dict':dict}, content_type='application/json')


'''===========================================================
       Project Name: Ups Api
       Function Name: TrackRequest with Signature Tracking
       URL: http://127.0.0.1:8000/ups/track
       Author: eKnous Technology Solutions
       License: https://eknous.com/license/ 
============================================================'''

def TrackRequestwithSignature(request):
    url = 'https://wwwcie.ups.com/rest/Track'
    header = variables.header
    
    data ={
            "UPSSecurity": {
                "UsernameToken":variables.ups_login(request),
            "ServiceAccessToken": {
                    "AccessLicenseNumber": variables.AccessLicenseNumber
                }
            },
            "TrackRequest": {
                "Request": {
                    "RequestOption": "15",
                    "TransactionReference": {
                        "CustomerContext": "eKnous Home Depo"
                    }
                },
                "InquiryNumber": "1Z5238Y90316417211", #ups provide a fix ShipmentIdentificationNumber for ech function of api
                "TrackingOption": "02" 
            }
        }
    response=requests.post(url, headers= header, data= json.dumps(data))
    # print response

    # return HttpResponse(response)
    dict = json.dumps(response.json(), indent=4)    
    return render(request, "json.json" , {'dict':dict}, content_type='application/json')


def image_converter(request):
    str_image=SaveImage.objects.get(id=2)
    # print (type(str_image))
    # a = str_image.decode('base64')
    fh = open("shipingLabelUpsPo1.pdf", "wb+")
    fh.write(base64.decodestring(str(str_image)))
    fh.close()
    
    # print fh
    return HttpResponse("Success")


    # else:
        #     print "shipment is already created"

    #     import io as BytesIO
    # #     # import base64
    # #     # from django.http import HttpResponse

    #     buffer = BytesIO.BytesIO()
    #     content = base64.b64decode(a)
    #     buffer.write(content)
    #     response = HttpResponse(buffer.getvalue(), content_type="application/pdf",)
    # #     print response
    #     response['Content-Disposition'] = 'inline;filename=ShipingLabel_UPS-PO.pds'
    # return response

    # return HttpResponse(response)
'''===========================================================
    Project Name: Ups Api
    Function Name: Uplode excel sheet 
    URL: http://127.0.0.1:8000/ups/exls
    Author: eKnous Technology Solutions
    License: https://eknous.com/license/ 
============================================================'''

def ReadFromCsv(request):    
    with open('ordermanage.csv', mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        # print csv_reader
        now = datetime.datetime.now().strftime("%m%d%Y")
        # print now     
        # for row in csv_reader:
        #     # a= row[1]
        #     print row        
    return HttpResponse('row')


from django import forms
class UploadFileForm(forms.Form):
    file = forms.FileField() 

def ReadFromExls(request):
    import xlrd
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        # loc = ("ordermanage.xls")
        loc = request.FILES['file']

        wb = xlrd.open_workbook(str(loc)) 
        sheet = wb.sheet_by_index(0)  
        excl_uplode = Order
        i=0
        
        for i in range(sheet.nrows):
            if not "COMPANY NAME" in sheet.cell_value(i , 0):        
                # print(sheet.cell_value(i , 0))
                company_name= sheet.cell_value(i , 0)
                # order_date= sheet.cell_value(i , 1)
                order_date= "2018-10-30"
                d= sheet.cell_value(i , 2)
                po= sheet.cell_value(i , 3)
                f= sheet.cell_value(i , 4)
                g= sheet.cell_value(i , 5)                
                print ("============================")
                exls_save = excl_uplode(po=po ,user = User.objects.get(id=2), company_name=Company.objects.get(id=2), courier_method= CourierMethod.objects.get(id=1), order_date= order_date)
                exls_save.save()
    else:
        form = UploadFileForm()
    return render( request, 'upload_form.html', {'form': form})
    # return HttpResponse("success")

def CreateShipment(request):
    queryset = Address.objects.all()
    shipper_name = ShipperModel.objects.all()
    packaging_type = PackagingType.objects.all()
    
    return render( request, 'create_shipment.html', {'queryset':queryset, 'shipper_name':shipper_name, 'packaging_type':packaging_type})
        
 
    


    
    

            
    