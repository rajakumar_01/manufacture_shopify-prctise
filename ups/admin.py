from __future__ import unicode_literals
from django.contrib import admin
from .models import UpsModel, UpsLoginModel, ShipperModel, ShipFromModel,SaveImage, PackagingType
class UpsModelAdmin(admin.ModelAdmin):
    model = UpsModel
    list_display =['po', 'tracking_number']

admin.site.register(UpsModel, UpsModelAdmin)

class UpsLoginModelAdmin(admin.ModelAdmin):
    model = UpsLoginModel
    list_display =['username', 'password']
admin.site.register(UpsLoginModel, UpsLoginModelAdmin)

class ShipperModelAdmin(admin.ModelAdmin):
    model = ShipperModel
    list_display =['name', 'shipper_account_number','attentionname','phon_number','address_line','city','state_province_code','postal_code','country_code']

admin.site.register(ShipperModel, ShipperModelAdmin)

class ShipFromModelAdmin(admin.ModelAdmin):
    model = ShipFromModel
    list_display =['name','attentionname','phon_number','address_line','city','state_province_code','postal_code','country_code']

admin.site.register(ShipFromModel, ShipFromModelAdmin)

admin.site.register(SaveImage)

class PackagingTypeAdmin(admin.ModelAdmin):
    model = PackagingType
    list_display = ['package_code', 'package_name']
admin.site.register(PackagingType, PackagingTypeAdmin )