# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from ordermanage.models import Address, Order
from shop.models import Company

from django.db import models

class UpsCompanyAccount(models.Model):    
    company_name = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='orderfrom_company_name')
    account_number = models.CharField(max_length = 20, null = False)
    account_zip = models.CharField(max_length = 12 , null= False)

# Create your models here.
class UpsModel(models.Model):
    po = models.ForeignKey(Address, default = 1, on_delete=models.CASCADE)
    tracking_number =  models.CharField(max_length =50 , null=True )
    
    def __unicode__(self): 
        return self.tracking_number

# ups login details
class UpsLoginModel(models.Model):
    # id  =models.AutoField(primary_key=True, default =1)
    username = models.CharField(max_length=20, null=False, unique=True)
    password = models.CharField(max_length =50 , null=False )
    
    def __unicode__(self):
        return (self.username) 


class ShipperModel(models.Model):
    username = models.ForeignKey(UpsLoginModel, related_name="upsusername",on_delete=models.CASCADE)
    name = models.CharField(max_length=20, null=False)
    attentionname= models.CharField(max_length=20, null=False)
    tax_identification_number = models.IntegerField()
    phon_number = models.IntegerField()
    extension = models.IntegerField()
    shipper_account_number= models.CharField(max_length=20, null=False)
    fax_number = models.IntegerField()
    address_line = models.CharField(max_length=200, null=False)
    city = models.CharField(max_length=20, null=False)
    state_province_code= models.CharField(max_length=20, null=False)
    postal_code= models.IntegerField()
    country_code = models.CharField(max_length=20, null=False)

    def __unicode__(self):
        return (self.name)

class ShipFromModel(models.Model):
    shipper_name = models.ForeignKey(ShipperModel, related_name="ups_shipper_name",on_delete=models.CASCADE)
    name = models.CharField(max_length=20, null=False)
    attentionname= models.CharField(max_length=20, null=False)   
    phon_number = models.IntegerField()
    fax_number = models.IntegerField()
    address_line = models.CharField(max_length=200, null=False)
    city = models.CharField(max_length=20, null=False)
    state_province_code= models.CharField(max_length=20, null=False)
    postal_code= models.IntegerField()
    country_code = models.CharField(max_length=20, null=False)

    def __unicode__(self):
        return (self.name)

class SaveImage(models.Model):
    image = models.TextField()
    def __unicode__(self):
        return (self.image)


class PackagingType(models.Model):
    package_code = models.CharField(max_length =4, null=False)
    package_name = models.CharField(max_length =25, null=False)
    def __unicode__(self):
        return (self.package_name)




    









